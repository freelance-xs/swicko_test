#  PerchPeek Test By Shane Daniel

---

##  Installation

* Decided to use php v 7.4
* From the root of the project - Run the `docker-compose up -d`.
```shell
docker-compose up -d
```
* Run the following command to monitor composer installation `docker logs -f peek-composer`.
```shell
docker logs -f peek-composer
```
* When that is done, enter the container and run this command to migrate and seed.

To enter the container: 
```shell
docker exec -it peek-www bash
```

To migrate and seed: 
```shell
php artisan start:app
```

Once that's done, start the queue with this command

To start the queue: 
```shell
php artisan queue:work &
```

That's all. Everything should be taken care of. Now read the instructions to use.

---

##  Instructions

Create Tickets - Bash into the container and run the command

```shell
php artisan ticket:generate
```

Process Tickets - Bash into the container and run the command

```shell
php artisan ticket:process
```

To access the end points

---

Get open tickets
```shell
http://localhost:8001/perch-peek/open-tickets
```
or
```shell
http://localhost:8001/perch-peek/open-tickets/{from}/{limit}
```

---

Get closed tickets
```shell
http://localhost:8001/perch-peek/closed-tickets
```
or
```shell
http://localhost:8001/perch-peek/closed-tickets/{from}/{limit}
```

---

Get user's tickets
```shell
http://localhost:8001/perch-peek/users/{email}/tickets
```
or
```shell
http://localhost:8001/perch-peek/{email}/tickets/{from}/{limit}
```

---

Get stats on tickets
```shell
http://localhost:8001/perch-peek/stats
```


---
