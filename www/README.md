# SWICO

## Setup

Copy the .env-example to .env

`cp .env-example .env`

and add your local DB credentials.

Next

`npm install && npm run dev`

Then

`composer install`

Then

`php artisan migrate --seed`

To refesh the database

`php artisan migrate:refresh --seed`

Followed by

`php artisan key:generate`

The urls that will be created for the multi tenant are swico.test, swissmade.test and crm.test.

If you are using valet you just need to run the following commands on the same directory

`valet link swico`

`valet link swissmade`



## Larvel Nova
`crm.test/nova`

username: dev@wearethunderbolt.com

password: password
