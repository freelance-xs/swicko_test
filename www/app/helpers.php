<?php

use App\Icons\Svg;
use App\Icons\Factory;
use Illuminate\Support\Carbon;

if(!function_exists('asset_url')){
    function asset_url(string $asset): string
    {
        return '/images/frontend/' . site_name() . '/' . $asset;
    }
}

if(!function_exists('site_name')){
    function site_name(): ?string
    {
        if (!app()->runningInConsole()) {
            return app('currentTenant')->name;
        }
    
        return null;
    }  
}


/*
* Returns the tenant id of the current user
*/
if(!function_exists('tenant_id')){
    function tenant_id(): ? int
    {
        if (!app()->runningInConsole()) {
            return app('currentTenant')->id;
        }

        return null;
    }
}

if(!function_exists('svg')){
    function svg(string $name, $class = '', array $attributes = []): Svg
    {
        return app(Factory::class)->svg($name, $class, $attributes);
    }
}

if(!function_exists('currentRouteIs')){
    function currentRouteIs($route): bool
    {
        if (request()->hasHeader('x-livewire') && url()->previous() === url()->route($route)) {
            return true;
        }

        if (request()->is($route)) {
            return true;
        }

        return false;
    }
}
/**
 * Readable date for frontend
 *
 * @param string $date
 * @return string
 */
if(!function_exists('formatDate')){
    function formatDate(string $date): string
    {
        return Carbon::parse($date)->format('d/m/Y');
    }
}

/**
 * Display a greeting for the authenticated user
 *
 * @return string
 */
if(!function_exists('greet')){
    function greet(): string
    {
        if(!auth()->check()) {
            return '';
        }

        $hour = now()->format('H');
        $name = auth()->user()->name;
        $message = '<span class="text-primary"> ' . $name . ' </span>';

        if ($hour < 12) {
            return 'Good morning' . $message;
        }
        if ($hour < 17) {
            return 'Good afternoon' . $message;
        }
        return 'Good evening' . $message;
    }
}
