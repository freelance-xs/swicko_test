<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'customer_id',
        'business_tel',
        'mobile_tel'
    ];

    protected $casts = [
        'customer_id' => 'integer'
    ];
}
