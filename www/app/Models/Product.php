<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'quantity',
        'price',
        'guarantee_terms',
        'tax_status_id'
    ];

    protected $casts = [
        'tax_status_id' => 'integer',
        'price' => 'integer'
    ];

    public function setPriceAttribute($value)
	{
		$this->attributes['price'] = $value*100;
	}

    public function getPriceAttribute($value)
	{
        return number_format($value/100, 2);
	}

    public function repaircase()
    {
        return $this->HasMany(RepairCase::class);
    }

    public function quotes()
    {
        return $this->belongsToMany(Quote::class);
    }
}
