<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepairJobType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'total_days',
        'watch_maker_days'
    ];

    protected $casts = [
        'name' => 'string',
        'total_days' => 'integer',
        'watch_maker_days' => 'integer'
    ];

    public function repaircase()
    {
        return $this->HasMany(RepairCase::class);
    }
}
