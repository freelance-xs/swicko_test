<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Tenant extends Model
{
    use HasFactory;

    /**
     * @param string $tenantDatabaseName
     * @return mixed
     */
    public function byDatabaseName(string $tenantDatabaseName)
    {
        return $this->where('database', $tenantDatabaseName);
    }

    /**
     * @return HasManyThrough
     */
    public function brands(): HasManyThrough
    {
        return $this->hasManyThrough(Brand::class, TenantBrand::class,
            'tenant_id', 'id', 'id', 'brand_id');
    }

    /**
     * @param string $tenantDatabaseName
     * @return mixed
     */
    public function brandsByDatabaseName(string $tenantDatabaseName)
    {
        $tenant = $this->byDatabaseName($tenantDatabaseName)->first();
        if ($tenant) {
            return $tenant->brands();
        }
        return null;
    }
}
