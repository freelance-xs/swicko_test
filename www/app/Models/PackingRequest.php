<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackingRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'repair_case_id',
        'customer_id',
        'with',
        'estimate_required',
        'model_ref'
    ];

    protected $casts = [
        'repair_case_id' => 'integer',
        'customer_id' => 'integer',
        'estimate_required' => 'boolean'
    ];
}
