<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'telephone',
        'email',
        'company_name',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'country',
        'offers'
    ];

    protected $casts = [
        'offers' => 'boolean',
    ];

    public function getFullNameAttribute(): string
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function repaircase()
    {
        return $this->HasMany(RepairCase::class);
    }
}
