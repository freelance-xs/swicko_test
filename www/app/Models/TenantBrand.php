<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantBrand extends Model
{
    use HasFactory;

    protected $fillable = [
        'tenant_id',
        'brand_id'
    ];
}
