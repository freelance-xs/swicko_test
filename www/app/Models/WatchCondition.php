<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WatchCondition extends Model
{
    use HasFactory;

    protected $fillable = [
        'repair_case_id',
        'description',
        'work_requested',
        'guarantee_end_date'
    ];

    protected $casts = [
        'repair_case_id' => 'integer',
        'description' => 'string',
        'work_requested' => 'string',
        'guarantee_end_date' => 'datetime'
    ];

    
    public function repair_case()
    {
        return $this->belongsTo(RepairCase::class);
    }
}
