<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    use HasFactory;

    protected $fillable = [
        'repair_case_id',
        'images',
        'glass',
        'strap_bracelet',
        'case',
        'bezel'
    ];

    protected $casts = [
        'repair_case_id' => 'integer',
        'images' => 'array'
    ];
}
