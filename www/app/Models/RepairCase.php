<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class RepairCase extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'case_number',
        'product_id',
        'serial_no',
        'status_id',
        'type_id',
        'department_id',
        'location_id',
        'advanced_assessment',
        'estimate_required',
        'assessment_condition',
        'admin_postage',
        'repair_delayed',
        'return_job',
        'return_undone',
        'guarantee_enc',
        'jobtype_id',
        'estimate_repair_date',
        'proceed_date',
        'misc',
        'trade_repair_ref_1',
        'trade_repair_ref_2',
        'royal_mail_tracking_no',
        'return_job_no',
        'date_received',
        'brand',
        'source_of_business',
        'despatched_date',
        'fast_track_battery_type',
        'proceeded_by',
        'final_watchmaker',
        'model_ref',
        'rud_reason',
        'guarantee_enclosed_date',
        'holdstatus_id'

    ];

    protected $casts = [
        'customer_id' => 'integer',
        'product_id' => 'integer',
        'status_id' => 'integer',
        'type_id' => 'integer',
        'department_id' => 'integer',
        'location_id' => 'integer',
        'advanced_assessment' => 'boolean',
        'assessment_condition' => 'array',
        'estimate_required' => 'boolean',
        'admin_postage' => 'boolean',
        'repair_delayed' => 'boolean',
        'return_job' => 'boolean',
        'return_undone' => 'boolean',
        'guarantee_enc' => 'boolean',
        'jobtype_id' => 'integer',
        'estimate_repair_date' => 'datetime',
        'proceed_date' => 'datetime',
        'date_received' => 'datetime',
        'source_of_business' => 'integer',
        'despatched_date' => 'datetime',
        'guarantee_enclosed_date' => 'datetime',
        'holdstatus_id' => 'integer'

    ];


    public function getCaseNumberAttribute($value)
    {
        return Str::upper($value);
    }

    public function product()
    {
        return $this->BelongsTo(Product::class);
    }

    public function customer()
    {
        return $this->BelongsTo(Customer::class);
    }

    public function status()
    {
        return $this->BelongsTo(Status::class);
    }

    public function type()
    {
        return $this->BelongsTo(Type::class);
    }

    public function department()
    {
        return $this->BelongsTo(Department::class);
    }
    
    public function location()
    {
        return $this->BelongsTo(Location::class);
    }

    public function jobtype()
    {
        return $this->BelongsTo(JobType::class);
    }

    public function holdstatus()
    {
        return $this->BelongsTo(HoldStatus::class);
    }

    public function assessmentcondtion()
    {
        return $this->BelongsTo(AssessmentCondition::class);
    }

    public function assessmentquotes()
    {
        return $this->hasMany(AssessmentQuoteNote::class);
    }

    public function quotes()
    {
        return $this->HasMany(Quote::class);
    }

    public function brand()
    {
        return $this->BelongsTo(Brand::class);
    }

    public function watchcondition()
    {
        return $this->HasOne(WatchCondition::class);
    }

    public function repair_job_type()
    {
        return $this->BelongsTo(RepairJobType::class);
    }

    public function fast_track_battery()
    {
        return $this->BelongsTo(FastTrackBattery::class);
    }

    public function case_type()
    {
        return $this->BelongsTo(CaseType::class);
    }

    public function serial_number()
    {
        return $this->BelongsTo(SerialNumber::class);
    }

    public function source_of_business()
    {
        return $this->BelongsTo(SourceOfBusiness::class);
    }

}
