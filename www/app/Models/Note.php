<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    protected $fillable = [
        'repair_case_id',
        'note'
    ];

    protected $casts = [
        'repair_case_id' => 'integer'
    ];
}
