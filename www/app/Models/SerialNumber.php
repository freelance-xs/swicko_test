<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SerialNumber extends Model
{
    use HasFactory;

    protected $fillable = [
        'brand_name',
        'serial_number'
    ];

    public function repaircase()
    {
        return $this->HasMany(RepairCase::class);
    }
}
