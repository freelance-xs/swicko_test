<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    use HasFactory;

    protected $fillable = [
        'repair_case_id',
        'active',
        'cost',
        'customer_id',
        'effective_from',
        'effective_to',
        'payment_type_id',
        'tax_status_id',
        'customer_facing_comment'
    ];

    protected $casts = [
        'repair_case_id' => 'integer',
        'active' => 'boolean',
        'cost' => 'integer',
        'customer_id' => 'integer',
        'effective_from' => 'datetime',
        'effective_to' => 'datetime',
        'payment_type_id' => 'integer',
        'tax_status_id' => 'integer'

    ];

    public function setCostAttribute($value)
	{
		$this->attributes['cost'] = $value*100;
	}

    public function getCostAttribute($value)
	{
        return number_format($value/100, 2);
	}

    public function products()
    {
        return $this->HasMany(Product::class);
    }

    public function repaircase()
    {
        return $this->belongsTo(RepairCase::class);
    }

    public function paymentType()
    {
        return $this->belongsTo(PaymentType::class);
    }

    public function taxStatus()
    {
        return $this->belongsTo(TaxStatus::class);
    }
}
