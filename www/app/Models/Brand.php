<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Brand extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'brand_name',
        'brand_short_name',
        'royal_mail_service_name',
        'title',
        'description',
        'repair_section_title'
    ];

    /**
     * @param string $brandShortName
     * @return mixed
     */
    public function byShortName(string $brandShortName)
    {
        return $this->where('brand_short_name', $brandShortName);
    }

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->HasMany(BrandImage::class);
    }

    /**
     * @return HasMany
     */
    public function repaircase(): HasMany
    {
        return $this->HasMany(RepairCase::class);
    }

}
