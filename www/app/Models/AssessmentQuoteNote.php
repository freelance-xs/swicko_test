<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessmentQuoteNote extends Model
{
    use HasFactory;

    protected $fillable = [
        'repair_cases_id',
        'timing',
        'estimates',
        'guarantee',
        'other',
        'dp_other'
    ];

    protected $casts = [
        'repair_cases_id' => 'integer',
        'timing' => 'array',
        'estimates' => 'array',
        'guarantee' => 'array',
        'other' => 'array'
    ];

    public function repaircases()
    {
        return $this->belongsTo(RepairCase::class);
    }

}
