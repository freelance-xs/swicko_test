<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use NovaButton\Button;

class Customer extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Customer::class;
    

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'FullName';


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'company_name','first_name','last_name','email',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable()
                ->hideFromIndex(),

            Text::make('Name', function () {
                return "<a href='/nova/resources/customers/{$this->id}' class='no-underline dim text-primary font-bold'>".sprintf('%s %s', $this->first_name, $this->last_name)."</a>";
            })->asHtml()->hideFromDetail(),

            Text::make('Name', function () {
                return "<a href='/nova/resources/customers/{$this->id}/edit' class='no-underline dim text-primary font-bold'>".sprintf('%s %s', $this->first_name, $this->last_name)."</a>";
            })->asHtml()->hideFromIndex(),

            Text::make('First name', 'first_name')
                ->hideFromIndex()
                ->hideFromDetail()
                ->rules('required','min:2'),

            Text::make('Last name', 'last_name')
                ->hideFromIndex()
                ->hideFromDetail()
                ->rules('required','min:2'),

            Text::make('Contact No', 'telephone')
                ->sortable()
                ->hideFromIndex()
                ->rules('required','numeric'),
            
            Text::make('Email', 'email')
                ->sortable()
                ->rules('required', 'email')
                ->creationRules('unique:customers,email')
                ->updateRules('unique:customers,email,{{resourceId}}'),

            Text::make('Company Name', 'company_name')
                ->sortable(),

            Text::make('Address 1', 'address_1')
                ->hideFromDetail()
                ->rules('required', 'min:2', 'string'),

            Text::make('Address 2', 'address_2')
                ->hideFromIndex()
                ->hideFromDetail(),

            Text::make('City', 'city')
                ->hideFromDetail()
                ->hideFromIndex()
                ->rules('required', 'min:2', 'string'),

            Text::make('Postcode', 'postcode')
                ->hideFromDetail()
                ->hideFromIndex()
                ->rules('required', 'min:2', 'string'),

            Text::make('Country', 'country')
                ->hideFromDetail()
                ->hideFromIndex()
                ->rules('required', 'min:2'),

            Button::make('New repair case')
                ->link("/nova/resources/repair-cases/new?viaResource=customers&viaResourceId={$this->id}&viaRelationship=repaircase", '_self')
                ->style('primary')
                ->hideFromDetail(),

            Boolean::make('offers')
                ->trueValue('On')
                ->falseValue('Off')
                ->hideFromIndex()
                ->hideFromDetail(),

            HasMany::make('Repair cases', 'repaircase')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
