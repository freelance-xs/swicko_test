<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\TEXT;
use Laravel\Nova\Fields\BOOLEAN;
use Laravel\Nova\Fields\BELONGSTO;
use Laravel\Nova\Fields\DATETIME;
use Laravel\Nova\Http\Requests\NovaRequest;

class Quote extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Quote::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            TEXT::make('Repair Case', 'repair_case_id'),

            BOOLEAN::make('Active', 'active'),

            TEXT::make('Cost', 'cost'),

            TEXT::make('Customer', 'customer_id'),

            DATETIME::make('Effective From', 'effective_from')->format('DD MM YYYY'),

            DATETIME::make('Effective To', 'effective_to')->format('DD MM YYYY'),

            BelongsTo::make('Payment Type', 'PaymentType')->hideFromIndex(),

            BelongsTo::make('Tax Status', 'TaxStatus')->hideFromIndex(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
