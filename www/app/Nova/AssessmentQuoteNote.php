<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\TEXT;
use Laravel\Nova\Fields\TEXTAREA;
use Laravel\Nova\Http\Requests\NovaRequest;
use OptimistDigital\MultiselectField\Multiselect;

class AssessmentQuoteNote extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\AssessmentQuoteNote::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            TEXT::make('Case Number', 'case_number'),

            Multiselect::make('Timing')
                ->options([
                    '0' => '-50 to +20 Sec Per Day within Tolerence',
                    '1' => 'Automatic Watch is Within Accepted Tolerence',
                ])
                ->placeholder('Timing'), 
                
            Multiselect::make('Estimates')
                ->options([
                    '0' => 'We don’t provide estimates under £20',
                    '1' => 'We don’t provide estimates under £40',
                    '2' => 'Will try to repair watch,but may may need to re-estimate',
                ])
                ->placeholder('Estimates'), 

            Multiselect::make('Guarantee')
                ->options([
                    '0' => 'Part job, not covered under Guarantee',
                    '1' => 'Guarantee for this type of procedure only applies to the work we undertake',
                    '2' => 'Battery is not under guarantee',
                ])
                ->placeholder('Guarantee'), 

            Multiselect::make('Other')
                ->options([
                    '0' => 'Full service required',
                    '1' => 'we will clean the dial/hands the best we can',
                ])
                ->placeholder('Other'),

            TEXTAREA::make('DP Other', 'dp_other')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
