<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BooleanGroup;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Select;
use OptimistDigital\MultiselectField\Multiselect;
use Illuminate\Support\Facades\Log;
use App\Nova\Metrics\RepairCaseCount;
use App\Nova\Metrics\RepairCaseByBrand;
use Laravel\Nova\Panel;
use Swico\RepaircaseHeader\RepaircaseHeader;
use Str;

class RepairCase extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\RepairCase::class;
    public static $globalSearchLink = 'edit';
    
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */

    
    public static $title = 'case_number';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'case_number', 'trade_repair_ref_1', 'trade_repair_ref_2', 'customer_id',
    ];

    public function title()
    {
        return $this->case_number;
    }

    public function subtitle()
    {
        return 'Customer: ' . $this->customer->first_name . ' ' . $this->customer->last_name . ' ' . $this->brand;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
       
       
        
        return [

            // Case Number
            Text::make('Case Number', 'case_number')
                ->hideFromIndex(),
        
            // Case Type
            BelongsTo::make('Case Type', 'case_type')
                ->hideFromIndex(),

            // Advanced Assessment
            Boolean::make('Advanced Assessment', 'advanced_assessment')
                ->trueValue('1')
                ->falseValue('0')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Date Created
            dateTime::make('Created On','created_at')
                ->format('DD MMM YYYY H:m:s')
                ->hideWhenUpdating()
                ->hideWhenCreating(),

            Text::make('Case Number', function(){
                $casenumber = $this->case_number;
                return "<a href='/nova/resources/repair-cases/{$this->id}/edit' class='no-underline dim text-primary font-bold'>{$casenumber}</a>";
            })->asHtml()->sortable(),

            // Brand
            BelongsTo::make('Brand')
                ->searchable()
                ->hideFromDetail()
                ->hideWhenUpdating()
                ->hideWhenCreating(),

            new Panel('Estimate Required', $this->estimateRequired()),

            new Panel('Case Details', $this->caseInformation()),

           

        ];
    }

    protected function estimateRequired()
    {
        return [
            // Estimate Required
            Boolean::make(__('Estimate Required (Trade)'), 'estimate_required')
                ->trueValue('1')
                ->falseValue('0')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Add Postage
            Boolean::make(__('Admin / Postage'), 'admin_postage')
                ->trueValue('1')
                ->falseValue('0')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Repair Job Type
            BelongsTo::make('Repair Job Type', 'repair_job_type')
                ->hideFromIndex()
                ->hideFromDetail(),

            //Fast Track Battery Type
            BelongsTo::make('Fast Track Battery','fast_track_battery')
                ->hideFromIndex(),

            // Date recieved
            Date::make(__('Date Received'), 'date_received')
                ->format('DD MMM YYYY')
                ->pickerDisplayFormat('d M Y')->hideFromIndex(),

            // Return Job
            Boolean::make(__('Repair Delayed'), 'repair_delayed')
                ->trueValue('1')
                ->falseValue('0')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Make Return Job
            Boolean::make(__('Return Job'), 'return_job')
                ->trueValue('1')
                ->falseValue('0')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Return Job Number
            Text::make(__('Return Job No'), 'return_job_no')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Estimate Repair date
            Date::make(__('Estimate Repair Date'), 'estimate_repair_date')
                ->format('DD MMM YYYY')
                ->pickerDisplayFormat('d M Y')
                ->hideFromIndex(),

            // Estimate Proceeded By
            Text::make(__('Proceeded By'), 'proceeded_by')->hideFromIndex()->hideFromDetail(),

            // Proceed Date
            Date::make(__('Proceed Date'), 'proceed_date')
                ->format('DD MMM YYYY')
                ->pickerDisplayFormat('d M Y')
                ->hideFromIndex()
                ->hideFromDetail(),
        ];
    }

    protected function caseInformation()
    {
        $cond = \App\Models\AssessmentCondition::all()->pluck('name','id');
        return [

            // ID
            ID::make(__('ID'), 'id')
                ->sortable()
                ->hideFromIndex()
                ->hideFromDetail(),

            // Customer
            BelongsTo::make('Customer')
                ->searchable()
                ->hideFromDetail()
                ->hideFromIndex(),  
                
            // Advanced Assessment Required
            Boolean::make('Advanced Assessment', 'advanced_assessment')
                ->trueValue('1')
                ->falseValue('0')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Case Type
            BelongsTo::make('Case Type', 'case_type')
                ->hideFromIndex(),

            // Brand
            BelongsTo::make('Brand')
                ->searchable()
                ->hideFromIndex()
                ->hideFromDetail(),

            // Serial Number 
            BelongsTo::make('Serial Number', 'serial_number')
                ->searchable()
                ->hideFromIndex()
                ->hideFromDetail(),

            // Model Reference
            Text::make('Model Ref', 'model_ref')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Trade Ref Repair #1
            Text::make(__('Trade Repair Ref 1'), 'trade_repair_ref_1')
                ->hideFromIndex(),

            // Trade Ref Repair #2
            Text::make(__('Trade Repair Ref 2'), 'trade_repair_ref_2')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Royal Mail tracking Number
            Text::make(__('Royal Mail Tracking No'), 'royal_mail_tracking_no')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Despatched Date
            Date::make(__('Despatched Date'), 'despatched_date')
                ->format('DD MMM YYYY')
                ->pickerDisplayFormat('d M Y')
                ->hideFromIndex(),

            // Guarantee Enclosed
            Boolean::make(__('Guarantee Enclosed'), 'guarantee_enc')
                ->trueValue('1')
                ->falseValue('0')
                ->hideFromIndex()
                ->hideFromDetail(),
        
            // Products
            BelongsTo::make('Product')->hideFromIndex(),

           // Misc Product
           Text::make(__('Misc'), 'misc')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Customer
            BelongsTo::make('Customer')
                ->searchable()
                ->hideFromDetail()
                ->hideWhenUpdating()
                ->hideWhenCreating(),
         
            // Final Watchmaker
            Text::make('Final Watchmaker', 'final_watchmaker')
                ->hideFromIndex()
                ->hideFromDetail(),

            // Source of Business
            BelongsTo::make('Source of Business', 'source_of_business')
                ->hideFromIndex()
                ->hideFromDetail(),

            

            BelongsTo::make('Status','Status')->hideFromIndex()->hideFromDetail(),

            

            BelongsTo::make('Department', 'Department')->hideFromIndex()->hideFromDetail(), 

            BelongsTo::make('Location', 'Location')->hideFromIndex()->hideFromDetail(),

           
     
            Multiselect::make('Assessment Condition', 'assessment_condition')
                ->options($cond)
                ->placeholder('Assessment Condition')
                ->hideFromIndex()
                ->hideFromDetail(), 
  
            

            

           

            Boolean::make(__('Return Undone'), 'return_undone')
                ->trueValue('1')
                ->falseValue('0')
                ->hideFromIndex()
                ->hideFromDetail(),
            
           
           

            
            
           

            

     

            

            

            

            ID::make(__('Source of Business'), 'source_of_business')
                ->hideFromIndex()->hideFromDetail(),

            

           

            

            

            

            Textarea::make(__('RUD Reason'), 'rud_reason')->hideFromIndex()->hideFromDetail(),

            Date::make(__('Guarantee Enclosed Date'), 'guarantee_enclosed_date')
                ->format('DD MMM YYYY')
                ->pickerDisplayFormat('d M Y')->hideFromIndex()->hideFromDetail(),

            BelongsTo::make('Hold Status', 'HoldStatus'),

            HasOne::make('Watch Condition', 'watchcondition'),
            
        ];
    }


    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new RepairCaseCount,
            new RepairCaseByBrand,
            new RepaircaseHeader
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
