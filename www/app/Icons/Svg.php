<?php

namespace App\Icons;

use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\Str;

class Svg implements Htmlable
{
    private string $name;

    private string $contents;

    private array $attributes;

    public function __construct(string $name, string $contents, array $attributes = [])
    {
        $this->name = $name;
        $this->contents = $contents;
        $this->attributes = $attributes;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function contents(): string
    {
        return $this->contents;
    }

    public function attributes(): array
    {
        return $this->attributes;
    }

    public function renderAttributes(): string
    {
        if (count($this->attributes) == 0) {
            return '';
        }

        return ' '.collect($this->attributes)->map(function (string $value, $attribute) {
                if (is_int($attribute)) {
                    return $value;
                }

                return sprintf('%s="%s"', $attribute, $value);
            })->implode(' ');
    }

    public function toHtml(): string
    {
        return str_replace(
            '<svg',
            sprintf('<svg%s', $this->renderAttributes()),
            $this->contents,
        );
    }

    public function __call(string $method, array $arguments): self
    {
        if (count($arguments) === 0) {
            $this->attributes[] = Str::snake($method, '-');
        } else {
            $this->attributes[Str::snake($method, '-')] = $arguments[0];
        }

        return $this;
    }
}
