<?php

namespace App\Icons\Components;

use Illuminate\View\Component;

class Svg extends Component
{
    public function render(): \Closure
    {
        return function (array $data) {
            $attributes = $data['attributes']->getIterator()->getArrayCopy();

            $class = $attributes['class'] ?? '';

            unset($attributes['class']);

            return svg($this->componentName, $class, $attributes)->toHtml();
        };
    }
}
