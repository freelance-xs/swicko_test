<?php

namespace App\Icons\Exceptions;

class SvgNotFound extends \Exception
{
    public static function missing(string $name): self
    {
        return new static("Svg by name \"$name\" not found.");
    }
}
