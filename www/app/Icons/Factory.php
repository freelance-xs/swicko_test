<?php

namespace App\Icons;

use App\Icons\Components\Svg as SvgComponent;
use App\Icons\Exceptions\SvgNotFound;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Str;

class Factory
{
    private Filesystem $filesystem;

    private string $defaultClass;

    private string $path;

    public function __construct(Filesystem $filesystem, string $defaultClass = '')
    {
        $this->filesystem = $filesystem;
        $this->defaultClass = $defaultClass;
        $this->path = base_path('resources/svg');
    }

    public function registerComponents(): void
    {
        foreach ($this->filesystem->allFiles($this->path) as $file) {
            if ($file->getExtension() !== 'svg') {
                continue;
            }

            $path = array_filter(explode('/', Str::after($file->getPath(), $this->path)));

            Blade::component(
                SvgComponent::class,
                implode('.', array_filter($path + [$file->getFilenameWithoutExtension()])),
                'icon',
            );
        }
    }

    public function svg(string $name, $class = '', array $attributes = []): Svg
    {
        return new Svg($name, $this->contents($name), $this->formatAttributes($class, $attributes));
    }

    private function contents(string $name): string
    {
        try {
            return trim($this->filesystem->get(sprintf(
                '%s/%s.svg',
                rtrim($this->path),
                str_replace('.', '/', $name),
            )));
        } catch (FileNotFoundException $exception) {
            //
        }

        throw SvgNotFound::missing($name);
    }

    private function formatAttributes(string $class = '', array $attributes = [])
    {
        if (is_string($class)) {
            $attributes['class'] = $attributes['class'] ?? $class;
        } elseif (is_array($class)) {
            $attributes = $class;

            if (! isset($attributes['class'])) {
                $attributes['class'] = $class;
            }
        }

        return $attributes;
    }

//    private function buildClass(string $class): string
//    {
//        return trim(sprintf(
//            '%s %s',
//            trim(sprintf('%s %s', $this->defaultClass, $this->sets[$set]['class'] ?? '')),
//            $class,
//        ));
//    }
}
