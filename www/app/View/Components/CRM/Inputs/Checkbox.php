<?php

namespace App\View\Components\CRM\Inputs;

use Illuminate\Contracts\View\View;

class Checkbox extends Input
{
    public bool $checked;

    public function __construct(string $name, string $id = null, bool $checked = false)
    {
        parent::__construct($name, $id, 'checkbox');

        $this->checked = (bool) old($name, $checked);
    }

    public function render(): View
    {
        return view('components.crm.inputs.checkbox');
    }
}
