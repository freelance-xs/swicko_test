<?php

namespace App\View\Components\CRM;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Arr;
use Illuminate\View\Component;

class Alert extends Component
{
    public string $type;

    public function __construct(string $type = 'alert')
    {
        $this->type = $type;
    }

    public function message(): string
    {
        return (string) Arr::first($this->messages());
    }

    public function messages(): array
    {
        return (array) session()->get($this->type);
    }

    public function exists(): bool
    {
        return session()->has($this->type) && ! empty($this->messages());
    }

    public function render(): View
    {
        return view('components.crm.alert');
    }
}
