<?php

namespace App\Providers;

use App\Icons\Factory;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSvgFactory();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function registerSvgFactory()
    {
        $this->app->singleton(Factory::class, function () {
            return new Factory(new Filesystem(), '');
        });

        $this->callAfterResolving(\Illuminate\Contracts\View\Factory::class, function ($view, Application $app) {
            $app->make(Factory::class)->registerComponents();
        });
    }
}
