<?php

namespace App\Scopes;

Use Illuminate\Database\Eloquent\Scope;
Use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\Builder;
Use Spatie\Multitenancy\Models\Tenant;


Class TenantScope implements Scope 
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('tenant_id', tenant_id());
    }
}