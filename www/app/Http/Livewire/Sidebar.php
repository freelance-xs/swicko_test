<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Sidebar extends Component
{
    public bool $minimised;

    public function mount()
    {
        $this->minimised = session('sidebar', false);
    }

    public function toggleMinimised()
    {
        session(['sidebar' => !session('sidebar')]);

        $this->minimised = !$this->minimised;
    }

    public function render()
    {
        return view('livewire.sidebar');
    }
}
