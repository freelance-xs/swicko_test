<?php

namespace App\Http\Livewire;

use Illuminate\Support\Str;
use Livewire\Component;

class PriceCalculator extends Component
{
    public string $selectedBrand = '';
    public string $selectedService = '';
    public string $selectedSubService = '';
    public string $price = '';
    public string $time = '';
    public string $other = '';

    protected $listeners = ['reset' => 'resetForm'];

    public function resetForm()
    {
        $this->reset();
    }

    public function updatedSelectedBrand()
    {
        $this->reset('selectedService', 'selectedSubService', 'price', 'time', 'other');
    }

    public function updatedSelectedService($value)
    {
        $this->price = isset($this->servicesDetails()[$value]['price']) ? $this->servicesDetails()[$value]['price'] : '';
        $this->time = isset($this->servicesDetails()[$value]['time']) ? $this->servicesDetails()[$value]['time'] : '';
    }

    public function updatedSelectedSubService($value)
    {
        $this->price = isset($this->servicesDetails()[$value]['price']) ? $this->servicesDetails()[$value]['price'] : '';
        $this->time = isset($this->servicesDetails()[$value]['time']) ? $this->servicesDetails()[$value]['time'] : '';
        $this->other = isset($this->servicesDetails()[$value]['other']) ? $this->servicesDetails()[$value]['other'] : '';
    }

    public function watchBrands(): array
    {
        if (site_name() === 'swissmade') {
            return [
                '' => 'PLEASE SELECT',
                'baume' => 'BAUME & MERCIER',
                'ebel' => 'EBEL',
                'rolex' => 'ROLEX',
                'omega' => 'OMEGA',
                'longines' => 'LONGINES'
            ];
        }

        return [
            '' => 'Select your brand',
            'raymond-weil' => 'Raymond Weil',
            'rue' => '88 Rue Du Rhone',
            'tw-steel' => 'TW Steel',
            'mgs' => 'Paul Smith Watches',
            'mgs1' => 'BOSS Watches',
            'mgs2' => 'BOSS Orange Watches',
            'hugo' => 'HUGO Watches',
            'mgs3' => 'Coach Watches',
            'mgs4' => 'Lacoste Watches',
            'others' => 'Movado Watches',
            'olivia-burton' => 'Olivia Burton',
            'mgs5' => 'Scuderia Ferrari Watches',
            'tommy' => 'Tommy Hilfiger Watches',
        ];
    }

    public function brandServices(): array
    {
        $selectedBrand = ucfirst(str_replace('-', '', $this->selectedBrand));

        if (Str::of($this->selectedBrand)->startsWith('mgs')) {
            $selectedBrand = 'Mgs';
        }

        $serviceMethod = 'brandServices'.$selectedBrand;

        if (method_exists($this, $serviceMethod)) {
            return $this->{$serviceMethod}();
        }
    }

    public function servicesDetails(): array
    {
        return [
            'mgs-quartz-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£40.00',
            ],
            'mgs-quartz-chronograph-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£50.00',
            ],
            'mgs-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£76.00',
            ],
            'mgs-quartz-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£112.00',
            ],
            'mgs-automatic-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'rw-quartz-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£69.00',
            ],
            'rw-quartz-othello-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£84.00',
            ],
            'rw-quartz-parsifal-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£84.00',
            ],
            'rw-quartz-chronograph-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£95.00',
            ],
            'rw-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£143.00',
            ],
            'rw-quartz-othello-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£272.00',
            ],
            'rw-quartz-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£221.00',
            ],
            'rw-automatic-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£185.00',
            ],
            'rw-automatic-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£272.00',
            ],
            'hugo-quartz-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£40.00',
            ],
            'hugo-quartz-chronograph-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£50.00',
            ],
            'hugo-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£76.00',
            ],
            'hugo-quartz-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£112.00',
            ],
            'hugo-automatic-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'rue-quartz-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£60.00',
            ],
            'rue-quartz-chronograph-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£85.00',
            ],
            'rue-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£130.00',
            ],
            'rue-quartz-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£195.00',
            ],
            'rue-automatic-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£165.00',
            ],
            'rue-automatic-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£250.00',
            ],
            'tw-quartz-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£54.00',
            ],
            'tw-quartz-chronograph-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£64.00',
            ],
            'tw-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£118.00',
            ],
            'tw-quartz-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£160.00',
            ],
            'tw-automatic-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'others-quartz-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£72.00',
            ],
            'others-quartz-chronograph-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£100.00',
            ],
            'others-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'others-quartz-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'others-automatic-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'olivia-burton-battery' => [
                'time' => 'Approx. 5 days',
                'price' => '£12.00',
            ],
            'olivia-burton-service' => [
                'time' => '&asymp; 14 days',
                'price' => '£40.00 to £60.00',
            ],
            'olivia-burton-dial' => [
                'time' => '&asymp; 14 days',
                'price' => '£40.00 to £60.00',
            ],
            'olivia-burton-case' => [
                'time' => '&asymp; 14 days',
                'price' => '£40.00 to £60.00',
            ],
            'olivia-burton-glass' => [
                'time' => '&asymp; 14 days',
                'price' => '£40.00 to £60.00',
            ],
            'olivia-burton-crown-stem' => [
                'time' => '&asymp; 14 days',
                'price' => '£40.00 to £60.00',
            ],
            'olivia-burton-strap' => [
                'time' => '',
                'price' => '',
                'other' => 'shop online at oliviaburton.com',
            ],
            'olivia-burton-bracelet' => [
                'time' => '',
                'price' => '',
                'other' => 'shop online at oliviaburton.com',
            ],
            'olivia-burton-links' => [
                'time' => '&asymp; 14 days',
                'price' => '£5.00 to £7.00',
            ],
            'tommy-quartz-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£40.00',
            ],
            'tommy-quartz-chronograph-battery' => [
                'time' => 'Approx. 10 days',
                'price' => '£50.00',
            ],
            'tommy-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£74.00',
            ],
            'tommy-quartz-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£108.00',
            ],
            'tommy-automatic-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'ebel-quartz' => [
                'time' => 'Approx. 14 days',
                'price' => '£153.00',
            ],
            'ebel-quartz-chronograph-battery' => [
                'time' => 'Approx. 14 days',
                'price' => '£153.00',
            ],
            'ebel-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£453.00',
            ],
            'ebel-quartz-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£522.00',
            ],
            'ebel-automatic-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£534.00',
            ],
            'ebel-automatic-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£969.00',
            ],
            'rolex-yachtmasterII' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'rolex-cosmograph' => [
                'time' => 'Approx. 28 days',
                'price' => '£590.00',
            ],
            'rolex-daydate' => [
                'time' => 'Approx. 28 days',
                'price' => '£520.00',
            ],
            'rolex-professional' => [
                'time' => 'Approx. 28 days',
                'price' => '£537.00',
            ],
            'rolex-datejust' => [
                'time' => 'Approx. 28 days',
                'price' => '£480.00',
            ],
            'rolex-date' => [
                'time' => 'Approx. 28 days',
                'price' => '£480.00',
            ],
            'rolex-perpetualairking' => [
                'time' => 'Approx. 28 days',
                'price' => '£390.00',
            ],
            'rolex-oyster' => [
                'time' => 'Approx. 28 days',
                'price' => '£430.00',
            ],
            'rolex-cellini-battery' => [
                'time' => 'Approx. 14 days',
                'price' => '£60.00',
            ],
            'rolex-cellini-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£390.00',
            ],
            'rolex-tudor-battery' => [
                'time' => 'Approx. 28 days',
                'price' => '£60.00',
            ],
            'rolex-tudor-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£285.00',
            ],
            'rolex-tudor-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£355.00',
            ],
            'longines-quartz' => [
                'time' => 'Approx. 14 days',
                'price' => '£114.50',
            ],
            'longines-quartz-chronograph-battery' => [
                'time' => 'Approx. 14 days',
                'price' => '£114.50',
            ],
            'longines-quartz-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£296.40',
            ],
            'longines-quartz-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£349.60',
            ],
            'longines-automatic-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£337.00',
            ],
            'longines-automatic-chronograph-service' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'omega-calibers-1985-present' => [
                'sub_services' => [
                    '' => 'PLEASE SELECT',
                    'omega-calibers-1985-present-quartz-service-strap' => 'Quartz Full Service - STRAP',
                    'omega-calibers-1985-present-quartz-service-bracelet' => 'Quartz Full Service - BRACELET',
                    'omega-calibers-1985-present-automatic-mechanical-service-strap' => 'Automatic / Mechanical Full Service - STRAP',
                    'omega-calibers-1985-present-automatic-mechanical-service-bracelet' => 'Automatic / Mechanical Full Service - BRACELET',
                ],
            ],
            'omega-calibers-1975-1984' => [
                'sub_services' => [
                    '' => 'PLEASE SELECT',
                    'omega-calibers-1975-1984-quartz-service-strap' => 'Quartz Full Service - STRAP',
                    'omega-calibers-1975-1984-quartz-service-bracelet' => 'Quartz Full Service - BRACELET',
                    'omega-calibers-1975-1984-automatic-mechanical-service-strap' => 'Automatic / Mechanical Full Service - STRAP',
                    'omega-calibers-1975-1984-automatic-mechanical-service-bracelet' => 'Automatic / Mechanical Full Service - BRACELET',
                ],
            ],
            'omega-calibers-1965-1974' => [
                'sub_services' => [
                    '' => 'PLEASE SELECT',
                    'omega-calibers-1965-1974-quartz-service-strap' => 'Quartz Full Service - STRAP',
                    'omega-calibers-1965-1974-quartz-service-bracelet' => 'Quartz Full Service - BRACELET',
                    'omega-calibers-1965-1974-automatic-mechanical-service-strap' => 'Automatic / Mechanical Full Service - STRAP',
                    'omega-calibers-1965-1974-automatic-mechanical-service-bracelet' => 'Automatic / Mechanical Full Service - BRACELET',
                ],
            ],
            'omega-calibers-pre-1965' => [
                'sub_services' => [
                    '' => 'PLEASE SELECT',
                    'omega-calibers-pre-1965-quartz-service-strap' => 'Quartz Full Service - STRAP',
                    'omega-calibers-pre-1965-quartz-service-bracelet' => 'Quartz Full Service - BRACELET',
                    'omega-calibers-pre-1965-automatic-mechanical-service-strap' => 'Automatic / Mechanical Full Service - STRAP',
                    'omega-calibers-pre-1965-automatic-mechanical-service-bracelet' => 'Automatic / Mechanical Full Service - BRACELET',
                ],
            ],
            'omega-all-ages' => [
                'sub_services' => [
                    '' => 'PLEASE SELECT',
                    'omega-all-ages-quartz-chronograph-service-strap' => 'Quartz Chronograph Full Service - STRAP',
                    'omega-all-ages-quartz-chronograph-service-bracelet' => 'Quartz Chronograph Full Service - BRACELET',
                    'omega-all-ages-automatic-mechanical-chronograph-service-strap' => 'Automatic / Mechanical Chronograph Full Service - STRAP',
                    'omega-all-ages-automatic-mechanical-chronograph-service-bracelet' => 'Automatic / Mechanical Chronograph Full Service - BRACELET',
                ],
            ],
            'baume-contemporary' => [
                'sub_services' => [
                    '' => 'PLEASE SELECT',
                    'baume-contemporary-maintenance-quartz-standard' => 'Basic Maintenance, Quartz - Standard',
                    'baume-contemporary-maintenance-quartz-chronograph' => 'Basic Maintenance, Quartz - Chronograph',
                    'baume-contemporary-maintenance-quartz-mechanical-standard' => 'Basic Maintenance, Mechanical/Automatic - Standard',
                    'baume-contemporary-maintenance-quartz-mechanical-chronograph' => 'Basic Maintenance, Mechanical/Automatic - Chronograph',
                    'baume-contemporary-service-quartz-standard' => 'Complete Service, Quartz - Standard',
                    'baume-contemporary-service-quartz-complication' => 'Complete Service, Quartz - Small complication',
                    'baume-contemporary-service-quartz-chronograph' => 'Complete Service, Quartz - Chronograph',
                    'baume-contemporary-service-mechanical-standard' => 'Complete Service, Mechanical/Automatic - Standard',
                    'baume-contemporary-service-mechanical-complication' => 'Complete Service, Mechanical/Automatic - Small complication',
                    'baume-contemporary-service-mechanical-chronograph' => 'Complete Service, Mechanical/Automatic - Chronograph',
                    'baume-contemporary-service-mechanical-chronograph-complication' => 'Complete Service, Mechanical/Automatic - Chronograph with small complication(s)',
                ]
            ],
            'baume-vintage' => [
                'sub_services' => [
                    '' => 'PLEASE SELECT',
                    'baume-vintage-maintenance-quartz-standard' => 'Basic Maintenance, Quartz - Standard',
                    'baume-vintage-maintenance-quartz-chronograph' => 'Basic Maintenance, Quartz - Chronograph',
                    'baume-vintage-maintenance-quartz-mechanical-standard' => 'Basic Maintenance, Mechanical/Automatic - Standard',
                    'baume-vintage-maintenance-quartz-mechanical-chronograph' => 'Basic Maintenance, Mechanical/Automatic - Chronograph',
                    'baume-vintage-service-quartz-standard' => 'Complete Service, Quartz - Standard',
                    'baume-vintage-service-quartz-complication' => 'Complete Service, Quartz - Small complication',
                    'baume-vintage-service-quartz-chronograph' => 'Complete Service, Quartz - Chronograph',
                    'baume-vintage-service-mechanical-standard' => 'Complete Service, Mechanical/Automatic - Standard',
                    'baume-vintage-service-mechanical-complication' => 'Complete Service, Mechanical/Automatic - Small complication',
                    'baume-vintage-service-mechanical-chronograph' => 'Complete Service, Mechanical/Automatic - Chronograph',
                ]
            ],
            'baume-contemporary-maintenance-quartz-standard' => [
                'time' => 'Approx. 14 days',
                'price' => '£85.00',
            ],
            'baume-contemporary-maintenance-quartz-chronograph' => [
                'time' => 'Approx. 14 days',
                'price' => '£110.00',
            ],
            'baume-contemporary-maintenance-quartz-mechanical-standard' => [
                'time' => 'Approx. 14 days',
                'price' => '£85.00',
            ],
            'baume-contemporary-maintenance-quartz-mechanical-chronograph' => [
                'time' => 'Approx. 14 days',
                'price' => '£110.00',
            ],
            'baume-contemporary-service-quartz-standard' => [
                'time' => 'Approx. 28 days',
                'price' => '£250.00',
            ],
            'baume-contemporary-service-quartz-complication' => [
                'time' => 'Approx. 28 days',
                'price' => '£295.00',
            ],
            'baume-contemporary-service-quartz-chronograph' => [
                'time' => 'Approx. 28 days',
                'price' => '£320.00',
            ],
            'baume-contemporary-service-mechanical-standard' => [
                'time' => 'Approx. 28 days',
                'price' => '£325.00',
            ],
            'baume-contemporary-service-mechanical-complication' => [
                'time' => 'Approx. 28 days',
                'price' => '£410.00',
            ],
            'baume-contemporary-service-mechanical-chronograph' => [
                'time' => 'Approx. 28 days',
                'price' => '£460.00',
            ],
            'baume-contemporary-service-mechanical-chronograph-complication' => [
                'time' => 'Approx. 28 days',
                'price' => '£460.00',
            ],
            'baume-vintage-maintenance-quartz-standard' => [
                'time' => '',
                'price' => '',
                'other' => 'Not available',
            ],
            'baume-vintage-maintenance-quartz-chronograph' => [
                 'time' => '',
                'price' => '',
                'other' => 'Not available',
            ],
            'baume-vintage-maintenance-quartz-mechanical-standard' => [
                 'time' => '',
                'price' => '',
                'other' => 'Not available',
            ],
            'baume-vintage-maintenance-quartz-mechanical-chronograph' => [
                 'time' => '',
                'price' => '',
                'other' => 'Not available',
            ],
            'baume-vintage-service-quartz-standard' => [
                'time' => 'Approx. 28 days',
                'price' => '£405.00',
            ],
            'baume-vintage-service-quartz-complication' => [
                'time' => 'Approx. 28 days',
                'price' => '£540.00',
            ],
            'baume-vintage-service-quartz-chronograph' => [
                'time' => 'Approx. 28 days',
                'price' => '£675.00',
            ],
            'baume-vintage-service-mechanical-standard' => [
                'time' => 'Approx. 28 days',
                'price' => '£730.00',
            ],
            'baume-vintage-service-mechanical-complication' => [
                'time' => 'Approx. 28 days',
                'price' => '£820.00',
            ],
            'baume-vintage-service-mechanical-chronograph' => [
                'time' => 'Approx. 28 days',
                'price' => '£890.00',
            ],
            'omega-calibers-1985-present-quartz-service-strap' => [
                'time' => 'Approx. 14 days',
                'price' => '£260.00',
            ],
            'omega-calibers-1985-present-quartz-service-bracelet' => [
                'time' => 'Approx. 14 days',
                'price' => '£290.00',
            ],
            'omega-calibers-1985-present-automatic-mechanical-service-strap' => [
                'time' => 'Approx. 28 days',
                'price' => '£310.00',
            ],
            'omega-calibers-1985-present-automatic-mechanical-service-bracelet' => [
                'time' => 'Approx. 28 days',
                'price' => '£340.00',
            ],
            'omega-calibers-1975-1984-quartz-service-strap' => [
                'time' => 'Approx. 14 days',
                'price' => '£330.00',
            ],
            'omega-calibers-1975-1984-quartz-service-bracelet' => [
                'time' => 'Approx. 14 days',
                'price' => '£360.00',
            ],
            'omega-calibers-1975-1984-automatic-mechanical-service-strap' => [
                'time' => 'Approx. 28 days',
                'price' => '£380.00',
            ],
            'omega-calibers-1975-1984-automatic-mechanical-service-bracelet' => [
                'time' => 'Approx. 28 days',
                'price' => '£410.00',
            ],
            'omega-calibers-1965-1974-quartz-service-strap' => [
                'time' => 'Approx. 14 days',
                'price' => '£380.00',
            ],
            'omega-calibers-1965-1974-quartz-service-bracelet' => [
                'time' => 'Approx. 14 days',
                'price' => '£410.00',
            ],
            'omega-calibers-1965-1974-automatic-mechanical-service-strap' => [
                'time' => 'Approx. 28 days',
                'price' => '£440.00',
            ],
            'omega-calibers-1965-1974-automatic-mechanical-service-bracelet' => [
                'time' => 'Approx. 28 days',
                'price' => '£470.00',
            ],
            'omega-calibers-pre-1965-quartz-service-strap' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'omega-calibers-pre-1965-quartz-service-bracelet' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'omega-calibers-pre-1965-automatic-mechanical-service-strap' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'omega-calibers-pre-1965-automatic-mechanical-service-bracelet' => [
                'time' => 'Approx. 28 days',
                'price' => '£POA',
            ],
            'omega-all-ages-quartz-chronograph-service-strap' => [
                'time' => 'Approx. 14 days',
                'price' => '£380.00',
            ],
            'omega-all-ages-quartz-chronograph-service-bracelet' => [
                'time' => 'Approx. 14 days',
                'price' => '£410.00',
            ],
            'omega-all-ages-automatic-mechanical-chronograph-service-strap' => [
                'time' => 'Approx. 28 days',
                'price' => '£440.00',
            ],
            'omega-all-ages-automatic-mechanical-chronograph-service-bracelet' => [
                'time' => 'Approx. 28 days',
                'price' => '£470.00',
            ],
        ];
    }

    public function brandServicesMgs(): array
    {
        return [
            '' => 'Select required service',
            'mgs-quartz-battery' => 'Quartz Battery',
            'mgs-quartz-chronograph-battery' => 'Quartz Chronograph Battery',
            'mgs-quartz-service' => 'Quartz Service',
            'mgs-quartz-chronograph-service' => 'Quartz Chronograph Service',
            'mgs-automatic-service' => 'Automatic Service',
        ];
    }

    public function brandServicesRaymondweil(): array
    {
        return [
            '' => 'Select required service',
            'rw-quartz-battery' => 'Quartz Battery',
            'rw-quartz-othello-battery' => 'Quartz Othello Battery',
            'rw-quartz-parsifal-battery' => 'Quartz Parsifal Battery',
            'rw-quartz-chronograph-battery' => 'Quartz Chronograph Battery',
            'rw-quartz-service' => 'Quartz Service',
            'rw-quartz-othello-service' => 'Quartz Othello 1.2mm Service',
            'rw-quartz-chronograph-service' => 'Quartz Chronograph Service',
            'rw-automatic-service' => 'Automatic Service',
            'rw-automatic-chronograph-service' => 'Automatic Chronograph Service',
        ];
    }

    public function brandServicesRue(): array
    {
        return [
            '' => 'Select required service',
            'rue-quartz-battery' => 'Quartz Battery',
            'rue-quartz-chronograph-battery' => 'Quartz Chronograph Battery',
            'rue-quartz-service' => 'Quartz Service',
            'rue-quartz-chronograph-service' => 'Quartz Chronograph Service',
            'rue-automatic-service' => 'Automatic Service',
            'rue-automatic-chronograph-service' => 'Automatic Chronograph Service',
        ];
    }

    public function brandServicesTwsteel(): array
    {
        return [
            '' => 'Select required service',
            'tw-quartz-battery' => 'Quartz Battery',
            'tw-quartz-chronograph-battery' => 'Quartz Chronograph Battery',
            'tw-quartz-service' => 'Quartz Service',
            'tw-quartz-chronograph-service' => 'Quartz Chronograph Service',
            'tw-automatic-service' => 'Automatic Service',
        ];
    }

    public function brandServicesHugo(): array
    {
        return [
            '' => 'Select required service',
            'hugo-quartz-battery' => 'Quartz Battery',
            'hugo-quartz-chronograph-battery' => 'Quartz Chronograph Battery',
            'hugo-quartz-service' => 'Quartz Service',
            'hugo-quartz-chronograph-service' => 'Quartz Chronograph Service',
            'hugo-automatic-service' => 'Automatic Service',
        ];
    }

    public function brandServicesOthers(): array
    {
        return [
            '' => 'Select required service',
            'others-quartz-battery' => 'Quartz Battery',
            'others-quartz-chronograph-battery' => 'Quartz Chronograph Battery',
            'others-quartz-service' => 'Quartz Service',
            'others-quartz-chronograph-service' => 'Quartz Chronograph Service',
            'others-automatic-service' => 'Automatic Service',
        ];
    }

    public function brandServicesOliviaburton(): array
    {
        return [
            '' => 'Select required service',
            'olivia-burton-battery' => 'Battery Replacement',
            'olivia-burton-service' => 'Mechanism Fault',
            'olivia-burton-dial' => 'Dial / Watch Face',
            'olivia-burton-case' => 'Case',
            'olivia-burton-glass' => 'Glass Replacement',
            'olivia-burton-crown-stem' => 'Crown & Stem',
            'olivia-burton-strap' => 'Leather Strap',
            'olivia-burton-bracelet' => 'Bracelet / Mesh',
            'olivia-burton-links' => 'Links',
        ];
    }

    public function brandServicesTommy(): array
    {
        return [
            '' => 'Select required service',
            'tommy-quartz-battery' => 'Quartz Battery',
            'tommy-quartz-chronograph-battery' => 'Quartz Chronograph Battery',
            'tommy-quartz-service' => 'Quartz Service',
            'tommy-quartz-chronograph-service' => 'Quartz Chronograph Service',
            'tommy-automatic-service' => 'Automatic Service',
        ];
    }

    public function brandServicesEbel(): array
    {
        return [
            '' => 'PLEASE SELECT',
            'ebel-quartz' => 'Quartz Battery',
            'ebel-quartz-chronograph-battery' => 'Quartz Chronograph Battery',
            'ebel-quartz-service' => 'Quartz Service',
            'ebel-quartz-chronograph-service' => 'Quartz Chronograph Service',
            'ebel-automatic-service' => 'Automatic Service',
            'ebel-automatic-chronograph-service' => 'Automatic Chronograph Service',
        ];
    }

    public function brandServicesRolex(): array
    {
        return [
            '' => 'WATCH MODEL / SERVICE',
            'rolex-yachtmasterII' => 'Yacht-Master II',
            'rolex-cosmograph' => 'Cosmograph',
            'rolex-daydate' => 'Day Date',
            'rolex-professional' => 'Professional',
            'rolex-datejust' => 'Datejust',
            'rolex-date' => 'Date',
            'rolex-perpetualairking' => 'Perpetual and AirKing',
            'rolex-oyster' => 'Oyster',
            'rolex-cellini-battery' => 'Cellini Battery',
            'rolex-cellini-quartz-service' => 'Cellini Quartz Service',
            'rolex-tudor-battery' => 'Tudor Battery',
            'rolex-tudor-quartz-service' => 'Tudor Quartz Service',
            'rolex-tudor-chronograph-service' => 'Tudor Chronograph Service',
        ];
    }

    public function brandServicesOmega(): array
    {
        return [
            '' => 'AGE OF WATCH',
            'omega-calibers-1985-present' => 'Calibers 1985 - Present Date',
            'omega-calibers-1975-1984' => 'Old Calibers 1975 - 1984',
            'omega-calibers-1965-1974' => 'Old Calibers 1965 - 1974',
            'omega-calibers-pre-1965' => 'Pre 1965',
            'omega-all-ages' => 'All ages',
        ];
    }

    public function brandServicesBaume(): array
    {
        return [
            '' => 'AGE OF WATCH',
            'baume-contemporary' => 'Contemporary - less than 20 years old',
            'baume-vintage' => 'Vintage - more than 20 years old',
        ];
    }

    public function brandServicesLongines(): array
    {
        return [
            '' => 'PLEASE SELECT',
            'longines-quartz' => 'Quartz Battery',
            'longines-quartz-chronograph-battery' => 'Quartz Chronograph Battery',
            'longines-quartz-service' => 'Quartz Service',
            'longines-quartz-chronograph-service' => 'Quartz Chronograph Service',
            'longines-automatic-service' => 'Automatic Service',
            'longines-automatic-chronograph-service' => 'Automatic Chronograph Service',
        ];
    }

    public function render()
    {
        return view('livewire.price-calculator');
    }
}
