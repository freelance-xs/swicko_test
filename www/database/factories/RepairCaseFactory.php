<?php

namespace Database\Factories;

use App\Models\RepairCase;
use Illuminate\Database\Eloquent\Factories\Factory;

class RepairCaseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RepairCase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => $this->faker->numberBetween($min = 1, $max = 150),
            'case_number' => 'SSC-'.rand(277564,277664).'-R'.$this->faker->randomDigit.$this->faker->randomLetter.$this->faker->randomDigit.$this->faker->randomLetter,
            'product_id' => rand(1,25),
            'serial_no' => $this->faker->numberBetween($min = 2526431, $max = 9526431),
            'status_id' => rand(1,6),
            'type_id' => rand(1,6),
            'department_id' => rand(1,5),
            'location_id' => rand(1,2),
            'advanced_assessment' => $this->faker->boolean($chanceOfGettingTrue = 10),
            'estimate_required' => $this->faker->boolean($chanceOfGettingTrue = 30),
            'admin_postage' => $this->faker->boolean($chanceOfGettingTrue = 30),
            'repair_delayed' => $this->faker->boolean($chanceOfGettingTrue = 30),
            'return_job' => $this->faker->boolean($chanceOfGettingTrue = 30),
            'return_undone' => $this->faker->boolean($chanceOfGettingTrue = 30),
            'guarantee_enc' => $this->faker->boolean($chanceOfGettingTrue = 30),
            'jobtype_id' => rand(1,4),
            'estimate_repair_date' => $this->faker->dateTimeInInterval($startDate = 'now', $interval = '+ '.rand(1,7).' months', $timezone = null),
            // 'proceed_date' => '00-00-0000 00:00:00',
            'misc' => $this->faker->sentence,
            // 'trade_repair_ref_1' => '',
            // 'trade_repair_ref_2' => '',
            // 'royal_mail_tracking_no' => '',
            // 'return_job_no' => '',
            // 'date_received' => '0000-00-00 00:00:00',
            // 'brand' => '',
            'source_of_business' => rand(1,3),
            // 'despatched_date' => '0000-00-00 00:00:00',
            // 'fast_track_battery_type' => '',
            // 'proceeded_by' => '',
            // 'final_watchmaker' => '',
            // 'model_ref' => '',
            // 'rud_reason' => '',
            // 'guarantee_enclosed_date' => '0000-00-00 00:00:00',
            'holdstatus_id' => rand(1,7),
            'brand_id' => rand(1,28),
            

        ];
    }
}
