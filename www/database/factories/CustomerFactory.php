<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName($gender=NULL),
            'last_name' => $this->faker->lastname,
            'telephone' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'company_name' => $this->faker->company,
            'address_1' => $this->faker->buildingNumber.' '.$this->faker->streetName,
            'address_2' => '',
            'city' => $this->faker->city,
            'postcode' => $this->faker->postcode,
            'country' => $this->faker->country,
            'offers' => rand(0,1)

        ];
    }
}
