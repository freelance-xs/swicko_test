<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence($nbWords = 3, $variableNbWords = true),
            'quantity' => $this->faker->numberBetween($min=10, $max=5000),
            'price' => rand(7500,500000),
            'guarantee_terms' => $this->faker->sentence($nbwords = 3),
            'tax_status_id' => $this->faker->numberBetween($min=1, $max=3)
        ];
    }
}
