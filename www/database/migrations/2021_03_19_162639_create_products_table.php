<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_family');
            $table->string('brand_name');
            $table->string('name');
            $table->integer('product_type');
            $table->bigInteger('price');
            $table->string('product_number');
            $table->bigInteger('current_cost');
            $table->string('vendor_part_name');
            $table->bigInteger('current_cost_base');
            $table->bigInteger('price_base');
            $table->text('product_description');
            $table->integer('quantity');
            $table->timestamps();
            $table->string('guarantee_terms');
            $table->bigInteger('tax_status_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
