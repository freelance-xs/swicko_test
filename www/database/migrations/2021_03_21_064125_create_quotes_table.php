<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->id();
            $table->biginteger('repair_case_id')->unsigned()->nullable();
            $table->boolean('active');
            $table->bigInteger('cost');
            $table->bigInteger('customer_id');
            $table->dateTime('effective_from');
            $table->dateTime('effective_to');
            $table->bigInteger('payment_type_id')->unsigned();
            $table->bigInteger('tax_status_id')->unsigned();
            $table->text('customer_facing_comments');
            $table->timestamps();

            $table->foreign('payment_type_id')->references('id')->on('payment_types')->onDelete('cascade');
            $table->foreign('tax_status_id')->references('id')->on('tax_statuses')->onDelete('cascade');
            $table->foreign('repair_case_id')->references('id')->on('repair_cases')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
