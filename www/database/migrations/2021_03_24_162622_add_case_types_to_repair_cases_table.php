<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCaseTypesToRepairCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_cases', function (Blueprint $table) {
            $table->bigInteger('case_type_id')->unsigned()->nullable();
            $table->foreign('case_type_id')->references('id')->on('case_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repair_cases', function (Blueprint $table) {
            $table->dropForeign('repair_cases_case_type_id_foreign');
            $table->dropColumn('case_type_id');
        });
    }
}
