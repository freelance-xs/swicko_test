<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentQuoteNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_quote_notes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('repair_cases_id');
            $table->json('timing')->nullable();
            $table->json('estimates')->nullable();
            $table->json('guarantee')->nullable();
            $table->json('other')->nullable();
            $table->text('dp_other')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_quote_notes');
    }
}
