<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packing_requests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('repair_case_id');
            $table->bigInteger('customer_id');
            $table->string('with');
            $table->boolean('estimate_required');
            $table->string('model_ref');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packing_requests');
    }
}
