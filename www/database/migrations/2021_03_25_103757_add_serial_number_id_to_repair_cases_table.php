<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSerialNumberIdToRepairCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_cases', function (Blueprint $table) {
            $table->bigInteger('serial_number_id')->unsigned()->nullable();
            $table->foreign('serial_number_id')->references('id')->on('serial_numbers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repair_cases', function (Blueprint $table) {
            $table->dropForeign('repair_cases_serial_number_id_foreign');
            $table->dropColumn('serial_number_id');
        });
    }
}
