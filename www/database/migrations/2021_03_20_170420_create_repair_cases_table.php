<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepairCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repair_cases', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id')->unsigned()->nullable();
            $table->string('case_number');
            $table->bigInteger('product_id')->unsigned()->nullable();
            $table->string('serial_no')->nullable();
            $table->bigInteger('status_id')->unsigned()->nullable();
            $table->bigInteger('type_id')->unsigned()->nullable();
            $table->bigInteger('department_id')->unsigned()->nullable();
            $table->bigInteger('location_id')->unsigned()->nullable();
            $table->boolean('advanced_assessment')->nullable();
            $table->text('assessment_condition')->nullable();
            $table->boolean('estimate_required');
            $table->boolean('admin_postage');
            $table->boolean('repair_delayed');
            $table->boolean('return_job');
            $table->boolean('return_undone');
            $table->boolean('guarantee_enc');
            $table->bigInteger('jobtype_id')->unsigned()->nullable();
            $table->dateTime('estimate_repair_date')->nullable();
            $table->dateTime('proceed_date')->nullable();
            $table->text('misc')->nullable();
            $table->text('trade_repair_ref_1')->nullable();
            $table->text('trade_repair_ref_2')->nullable();
            $table->text('royal_mail_tracking_no')->nullable();
            $table->string('return_job_no')->nullable();
            $table->dateTime('date_received')->nullable();
            $table->string('brand')->nullable();
            $table->bigInteger('source_of_business')->nullable();
            $table->dateTime('despatched_date')->nullable();
            $table->string('fast_track_battery_type')->nullable();
            $table->string('proceeded_by')->nullable();
            $table->text('final_watchmaker')->nullable();
            $table->string('model_ref')->nullable();
            $table->text('rud_reason')->nullable();
            $table->dateTime('guarantee_enclosed_date')->nullable();
            $table->bigInteger('holdstatus_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repair_cases');
    }
}
