<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFastTrackBatteryIdToRepairCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_cases', function (Blueprint $table) {
            $table->bigInteger('fast_track_battery_id')->unsigned()->nullable();
            $table->foreign('fast_track_battery_id')->references('id')->on('fast_track_batteries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repair_cases', function (Blueprint $table) {
            $table->dropForeign('repair_cases_fast_track_battery_id_foreign');
            $table->dropColumn('fast_track_battery_id');
        });
    }
}
