<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSourceOfBusinessIdToRepairCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_cases', function (Blueprint $table) {
            $table->bigInteger('source_of_business_id')->unsigned()->nullable();
            $table->foreign('source_of_business_id')->references('id')->on('source_of_businesses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repair_cases', function (Blueprint $table) {
            $table->dropForeign('repair_cases_source_of_business_id_foreign');
            $table->dropColumn('source_of_business_id');
        });
    }
}
