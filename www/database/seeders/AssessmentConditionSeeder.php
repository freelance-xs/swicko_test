<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AssessmentConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assessment_conditions')->insert([
            [
                'name' => 'Glass is Scratched',
                'slug' => Str::slug('Glass is Scratched'),
            ],
            [
                'name' => 'Glass is Lightly Scratched',
                'slug' => Str::slug('Glass is Lightly Scratched')
            ],
            [
                'name' => 'Glass is Badly Scratched',
                'slug' => Str::slug('Glass is Badly Scratched')
            ],
            [
                'name' => 'Glass is Chipped',
                'slug' => Str::slug('Glass is Chipped')
            ],
            [
                'name' => 'Glass is Cracked',
                'slug' => Str::slug('Glass is Cracked')
            ],
            [
                'name' => 'Glass is Smashed',
                'slug' => Str::slug('Glass is Smashed')
            ],
            [
                'name' => 'Glass is Chipped around Bezel',
                'slug' => Str::slug('Glass is Chipped around Bezel')
            ],
            [
                'name' => 'Case/Bezel is Chipped around the Glass',
                'slug' => Str::slug('Case/Bezel is Chipped around the Glass')
            ],
            [
                'name' => 'Bezel is Dented',
                'slug' => Str::slug('Bezel is Dented')
            ],
            [
                'name' => 'Strap/Bracelet Fitted is not OEM',
                'slug' => Str::slug('Strap/Bracelet Fitted is not OEM')
            ],
            [
                'name' => 'Bracelet is Scratched',
                'slug' => Str::slug('Bracelet is Scratched')
            ],
            [
                'name' => 'Bracelet is Lightly Scratched',
                'slug' => Str::slug('Bracelet is Lightly Scratched')
            ],
            [
                'name' => 'Bracelet is Dented',
                'slug' => Str::slug('Bracelet is Dented')
            ],
            [
                'name' => 'Dial is Damaged by Broken Glass',
                'slug' => Str::slug('Dial is Damaged by Broken Glass')
            ],
            [
                'name' => 'Dial and Hands are Watermarked',
                'slug' => Str::slug('Dial and Hands are Watermarked')
            ],
            [
                'name' => 'Dial/Hands are Marked/Discoloured',
                'slug' => Str::slug('Dial/Hands are Marked/Discoloured')
            ],
            [
                'name' => 'Case is Scratched',
                'slug' => Str::slug('Case is Scratched')
            ],
            [
                'name' => 'Case is Lightly Scratched',
                'slug' => Str::slug('Case is Lightly Scratched')
            ],
            [
                'name' => 'Case is Dented',
                'slug' => Str::slug('Case is Dented')
            ],
            [
                'name' => 'Case is Distorted',
                'slug' => Str::slug('Case is Distorted')
            ],
            [
                'name' => 'Case is Damaged at Opening Point',
                'slug' => Str::slug('Case is Damaged at Opening Point')
            ],
            [
                'name' => 'Case Lugs are Bent',
                'slug' => Str::slug('Case Lugs are Bent')
            ],
            [
                'name' => 'Button is Worn',
                'slug' => Str::slug('Button is Worn')
            ],
            [
                'name' => 'Button/Crown is Worn',
                'slug' => Str::slug('Button/Crown is Worn')
            ],
            [
                'name' => 'Back is Scratched',
                'slug' => Str::slug('Back is Scratched')
            ],
            [
                'name' => 'Back is dented',
                'slug' => Str::slug('Back is dented')
            ],
            [
                'name' => 'Case/Dial has Water Damage',
                'slug' => Str::slug('Case/Dial has Water Damage')
            ],
            
        ]);
    }
}
