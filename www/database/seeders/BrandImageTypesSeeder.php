<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandImageTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brand_image_types')->insert(
            [
                ['name' => 'banner'],
                ['name' => 'main'],
                ['name' => 'logo']
            ]
        );
    }
}
