<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            [
                'brand_name' => 'Ebel',
                'brand_short_name' => 'EB',
                'royal_mail_service_name' => 'SD1'
            ],
            [
                'brand_name' => 'TW Steel',
                'brand_short_name' => 'TW',
                'royal_mail_service_name' => 'TP201'
            ],
            [
                'brand_name' => 'Scuderia Ferrari Watches',
                'brand_short_name' => 'SF',
                'royal_mail_service_name' => 'SD101'
            ],
            [
                'brand_name' => 'MISC',
                'brand_short_name' => 'XX',
                'royal_mail_service_name' => 'TPS48'
            ],
            [
                'brand_name' => 'Eternal Silver',
                'brand_short_name' => 'ES',
                'royal_mail_service_name' => 'TP201'
            ],
            [
                'brand_name' => 'Paul Smith Watches',
                'brand_short_name' => 'PS',
                'royal_mail_service_name' => 'SD201'
            ],
            [
                'brand_name' => 'Swissmade (Other)',
                'brand_short_name' => 'SM',
                'royal_mail_service_name' => 'SD201'
            ],
            [
                'brand_name' => 'Rolex',
                'brand_short_name' => 'RX',
                'royal_mail_service_name' => 'SD101'
            ],
            [
                'brand_name' => 'Movado Watches',
                'brand_short_name' => 'MV',
                'royal_mail_service_name' => 'SD1'
            ],
            [
                'brand_name' => '88 Rue Du Rhone',
                'brand_short_name' => 'RD',
                'royal_mail_service_name' => 'TP201'
            ],
            [
                'brand_name' => 'Coach Watches',
                'brand_short_name' => 'CO',
                'royal_mail_service_name' => 'TPS48'
            ],
            [
                'brand_name' => 'BOSS Orange Watches',
                'brand_short_name' => 'BO',
                'royal_mail_service_name' => 'TPS48'
            ],
            [
                'brand_name' => 'Boss Watches',
                'brand_short_name' => 'BW',
                'royal_mail_service_name' => 'TPS48'
            ],
            [
                'brand_name' => 'Lacoste Watches',
                'brand_short_name' => 'LA',
                'royal_mail_service_name' => 'TP201'
            ],
            [
                'brand_name' => 'Raymond Weil',
                'brand_short_name' => 'RW',
                'royal_mail_service_name' => 'SD1'
            ],
            [
                'brand_name' => 'Tommy Hilfiger Watches',
                'brand_short_name' => 'TH',
                'royal_mail_service_name' => 'TPS48'
            ],
            [
                'brand_name' => 'Longines',
                'brand_short_name' => 'LG',
                'royal_mail_service_name' => 'SD301'
            ],
            [
                'brand_name' => 'Sekford',
                'brand_short_name' => 'SK',
                'royal_mail_service_name' => 'TP201'
            ],
            [
                'brand_name' => 'Olivia Burtom',
                'brand_short_name' => 'OB',
                'royal_mail_service_name' => 'TP201'
            ],
            [
                'brand_name' => 'HUGO Watches',
                'brand_short_name' => 'HU',
                'royal_mail_service_name' => 'TPS48'
            ],
            [
                'brand_name' => 'Daniel Wellington',
                'brand_short_name' => 'DW',
                'royal_mail_service_name' => 'TP201'
            ],
            [
                'brand_name' => 'William L',
                'brand_short_name' => 'WL',
                'royal_mail_service_name' => 'TP201'
            ],
            [
                'brand_name' => 'Rebecca Minkoff',
                'brand_short_name' => 'MK',
                'royal_mail_service_name' => 'TPS48'
            ],
            [
                'brand_name' => 'Maserati',
                'brand_short_name' => 'MS',
                'royal_mail_service_name' => 'TPN24'
            ],
            [
                'brand_name' => 'Louis Vuitton',
                'brand_short_name' => 'LV',
                'royal_mail_service_name' => 'SD3'
            ],
            [
                'brand_name' => 'MVMT',
                'brand_short_name' => 'MD',
                'royal_mail_service_name' => 'TPS48'
            ],
            [
                'brand_name' => 'Baume & Mercier',
                'brand_short_name' => 'BM',
                'royal_mail_service_name' => 'SD3'
            ],
            [
                'brand_name' => 'Omega',
                'brand_short_name' => 'OM',
                'royal_mail_service_name' => 'SD301'
            ],
            [
                'brand_name' => 'Juicy Courture',
                'brand_short_name' => 'JC',
                'royal_mail_service_name' => 'SD301'
            ],
        ]);
    }
}
