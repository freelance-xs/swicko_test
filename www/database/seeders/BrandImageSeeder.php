<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\BrandImage;
use App\Models\BrandImageType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class BrandImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mainImagePass = ['rx', 'bm', 'om'];
        $brands = Brand::all();
        $brands_types = BrandImageType::all();
        foreach ($brands as $key => $brand) {
            foreach ($brands_types as $value) {
                $brandAcronym = Str::lower($brand->brand_short_name);
                $image = new BrandImage();
                $image->brand_id = $brand->id;
                $image->brand_image_type_id = $value->id;
                if ($value->name == 'main') {
                    if (!in_array($brandAcronym, $mainImagePass)) {
                        $image->url = 'banners/brands/' . $brandAcronym . '.' . 'jpg';
                    }else{
                        $image->url = "";
                    }
                } elseif ($value->name == 'banner') {
                    $image->url = 'banners/brands/banner-' . $brandAcronym . '.' . 'jpg';
                } elseif ($value->name == 'logo') {
                    if ($brandAcronym == "dw") {
                        $image->url = 'banners/brands/logo-' . $brandAcronym . '.' . 'svg';
                    } elseif ($brandAcronym == "eb") {
                        $image->url = 'banners/brands/logo-' . $brandAcronym . '.' . 'jpg';
                    } else {
                        $image->url = 'banners/brands/logo-' . $brandAcronym . '.' . 'png';
                    }
                }
                $image->save();
            }
        }
    }
}
