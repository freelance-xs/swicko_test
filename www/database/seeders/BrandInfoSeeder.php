<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class BrandInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brandShortNames = [
            'EB',
            'TW',
            'SF',
            'XX',
            'ES',
            'PS',
            'SM',
            'RX',
            'MV',
            'RD',
            'CO',
            'BO',
            'BW',
            'LA',
            'RW',
            'TH',
            'LG',
            'SK',
            'OB',
            'HU',
            'DW',
            'WL',
            'MK',
            'MS',
            'LV',
            'MD',
            'BM',
            'OM'
        ];


        $brandInfo = [
            'EB' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR EBEL WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWISSMADE are proud to be EBEL’S Official UK Service Centre, providing warranty and non-warranty repair and maintenance to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with EBEL gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

Sending your EBEL watch to us is easy and safe thanks to our secure postal service. Our online service provides the fastest solution to care for your watch direct with the brands Official UK Service centre.</p>',
                'repair_section_title' => 'EBEL WATCHES'
            ],
            'TW' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'SF' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR SCUDERIA FERRARI WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be SCUDERIA FERRARI watches Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with SCUDERIA FERRARI watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

                Sending your SCUDERIA FERRARI watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'SCUDERIA FERRARI WATCHES'
            ],
            'XX' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'ES' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'PS' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'SM' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'RX' => [
                'title' => 'ROLEX ACCREDITED SERVICE CENTRE',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWISSMADE are proud to employ a ROLEX accredited Watchmaker, meaning our Service Centre SWISSMADE is authorised and equipped with the specialist tools, equipment and genuine spare parts by ROLEX to service and maintain ROLEX watches to the highest standard. This is vital to ensure your watch is cared for correctly and in accordance to ROLEX’S procedures and regulations.

                </h3><p>Sending your ROLEX watch to us is easy and safe thanks to our secure postal service. Our online service provides the fastest solution to care for your watch direct with the brands Official UK Service centre.',
                'repair_section_title' => 'ROLEX'
            ],
            'LA' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR LACOSTE WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be LACOSTE watches Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with LACOSTE watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

                Sending your LACOSTE watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'LACOSTE WATCHES'
            ],
            'MV' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR MOVADO WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be MOVADO’S Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with MOVADO watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

                Sending your MOVADO watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'MOVADO WATCHES'
            ],
            'RD' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'CO' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR COACH WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be COACH watches Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with COACH watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

                Sending your COACH watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'COACH WATCHES'
            ],
            'BO' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR BOSS WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be BOSS watches Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with BOSS watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

                Sending your BOSS watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'BOSS WATCHES'
            ],
            'BW' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR LACOSTE WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be LACOSTE watches Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with LACOSTE watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

                Sending your LACOSTE watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'LACOSTE WATCHES'
            ],
            'RW' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR RAYMOND WEIL WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be RAYMOND WEIL’S Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with RAYMOND WEIL gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>
                Sending your RAYMOND WEIL watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'RAYMOND WEIL WATCHES'
            ],
            'TH' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR TOMMY HILFIGER WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be TOMMY HILFIGER watches Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with TOMMY HILFIGER watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

                Sending your TOMMY HILFIGER watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'TOMMY HILFIGER WATCHES'
            ],
            'LG' => [
                'title' => 'LONGINES CERTIFIED SERVICE & REPAIR CENTRE.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWISSMADE are proud to be a LONGINES CERTIFIED Level 3 UK Service & Repair Centre. As a certified LONGINES service partner we have fulfilled LONGINES strict quality requirements and can provide services and repairs to the manufacturers high standards for retailers and customers direct.</h3>
                <p>Operating from our purpose-built secure service centre in Hatton Garden, London, our workshop only ever uses the manufacturers’ genuine and approved spare parts with the specialist tools and equipment. This is vital to ensure your watch is cared for correctly and in accordance to LONGINES procedures and regulations. <br><br>

                Sending your LONGINES watch to us is easy and safe thanks to our secure postal service. Our online service provides the fastest solution to care for your watch direct with the brands Official UK Service centre.</p>',
                'repair_section_title' => 'LONGINES WATCHES'
            ],
            'SK' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'OB' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR OLIVIA BURTON WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be OLIVIA BURTON watches Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3>
                <p>Our partnership working with OLIVIA BURTON watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.

                Sending your OLIVIA BURTON watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'OLIVIA BURTON WATCHES'
            ],
            'HU' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR HUGO WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be HUGO watches Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with HUGO watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

                Sending your HUGO watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'HUGO WATCHES'
            ],
            'DW' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR DANIEL WELLINGTON.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWICO are proud to be DANIEL WELLINGTON watches Official UK Service Centre, providing warranty or non-warranty services and repairs to the highest standards for retailers and customers direct.</h3><p>Our long and successful history working with DANIEL WELLINGTON watches gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.<br><br>

                Sending your DANIEL WELLINGTON watch to us is easy and safe thanks to our free secure postal service. Our online service provides the easiest and fastest solution to care for your watch direct with the watch brands Official UK Service Centre.</p>',
                'repair_section_title' => 'DANIEL WELLINGTON'
            ],
            'WL' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'MK' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'MS' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'LV' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'MD' => [
                'title' => '',
                'description' => '',
                'repair_section_title' => ''
            ],
            'BM' => [
                'title' => 'OFFICIAL UK SERVICE AND REPAIR CENTRE FOR BAUME & MERCIER WATCHES.',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWISSMADE are proud to be BAUME & MERCIER’S Official UK Service Centre, providing warranty and non-warranty repair and maintenance to the highest standards for retailers and customers direct.
                </h3><p>Our successful working partnership working with BAUME & MERCIER gives us the unique understanding and expertise with all the specialist equipment and knowledge to maintain and repair your watch using genuine manufactures parts.

                <br><br>
                Sending your BAUME & MERCIER watch to us is easy and safe thanks to our secure postal service. Our online service provides the fastest solution to care for your watch direct with the brands Official UK Service centre.</p>',
                'repair_section_title' => 'BAUME & MERCIER WATCHES'
            ],
            'OM' => [
                'title' => 'OMEGA CERTIFIED SERVICE & REPAIR CENTRE',
                'description' => '<h3 class="text-grey-500 text-lg md:text-xl mb-3">SWISSMADE are proud to be an OMEGA CERTIFIED Level 3 UK Service & Repair Centre. As a certified OMEGA service partner we have fulfilled OMEGA’S strict quality requirements and can provide services and repairs to the manufacturers high standards for retailers and customers direct.</h3>

                <p>
                <br><br>
                Operating from our purpose-built secure service centre in Hatton Garden, London, our workshop only ever uses the manufacturers’ genuine and approved spare parts with the specialist tools and equipment. This is vital to ensure your watch is cared for correctly and in accordance to Omega’s procedures and regulations.
                <br><br>
                Sending your OMEGA watch to us is easy and safe thanks to our secure postal service. Our online service provides the fastest solution to care for your watch direct with the brands Official UK Service centre.
                </p>',
                'repair_section_title' => 'OMEGA'
            ],

        ];

        $brands = Brand::whereIn('brand_short_name', $brandShortNames)->get();
        foreach ($brands as $brand) {
            $brandShortName = $brand->brand_short_name ?? null;

            if ($brandShortName && isset($brandInfo[$brandShortName])) {
                $data = [
                    'description' => $brandInfo[$brandShortName]['description'] ?? "",
                    'title' => $brandInfo[$brandShortName]['title'] ?? "",
                    'repair_section_title' => $brandInfo[$brandShortName]['repair_section_title'] ?? "",
                ];
                $brand->update($data);
            }
        }
    }
}
