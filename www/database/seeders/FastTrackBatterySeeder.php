<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FastTrackBatterySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fast_track_batteries')->insert([
            [
                'name' => 'Under Guarantee - Quartz Battery'
            ],
            [
                'name' => 'Under Guarantee - Quartz Chronograph Battery'
            ],
            [
                'name' => 'Not Under Guarantee - Quartz Battery'
            ],
            [
                'name' => 'Not Under Guarantee - Quartz Chronograph Battery'
            ]
        ]);
    }
}
