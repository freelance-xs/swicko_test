<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            [
                'name' => 'status 1',
                
            ],
            [
                'name' => 'status 2',
               
            ],
            [
                'name' => 'status 3',
                
            ],
            [
                'name' => 'status 4',
                
            ],
            [
                'name' => 'status 5',
               
            ],
            [
                'name' => 'status 6',
                
            ],
        ]);
    }
}
