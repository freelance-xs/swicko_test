<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TenantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tenants')->insert([
            [
                'name' => 'crm',
                'domain' => 'crm.test',
                'database' => 'crm',
            ],
            [
                'name' => 'swico',
                'domain' => 'swico.test',
                'database' => 'swico',
            ],
            [
                'name' => 'swissmade',
                'domain' => 'swissmade.test',
                'database' => 'swissmade',
            ],
        ]);
    }
}
