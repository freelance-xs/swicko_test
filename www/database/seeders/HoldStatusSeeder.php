<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HoldStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hold_statuses')->insert([
            [
                'name' => 'Dial feet broken'
            ],
            [
                'name' => 'Dial cracked'
            ],
            [
                'name' => 'Tampered with'
            ],
            [
                'name' => 'Clasp Damaged'
            ],
            [
                'name' => 'Movement rusty'
            ],
            [
                'name' => 'Awaiting parts'
            ],
            [
                'name' => 'On Hold - Estimate Needed'
            ],

        ]);
    }
}
