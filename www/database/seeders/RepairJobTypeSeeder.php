<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RepairJobTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('repair_job_types')->insert([
            
            [
                'name' => 'Battery',
                'total_days' => '10',
                'watch_maker_days' => '6'
            ],
            [
                'name' => 'Mech/Auto Service',
                'total_days' => '28',
                'watch_maker_days' => '14'
            ],
            [
                'name' => 'Part Job',
                'total_days' => '14',
                'watch_maker_days' => '7'
            ],
            [
                'name' => 'Quartz Service',
                'total_days' => '28',
                'watch_maker_days' => '14'
            ],
            [
                'name' => 'Refurbishment',
                'total_days' => '32',
                'watch_maker_days' => '20'
            ],
            [
                'name' => 'Replacement Watch',
                'total_days' => '20',
                'watch_maker_days' => '10'
            ],
            [
                'name' => 'X-Cancelled',
                'total_days' => '0',
                'watch_maker_days' => '0'
            ],
            [
                'name' => 'Z-Battery',
                'total_days' => '20',
                'watch_maker_days' => '10'
            ],
            [
                'name' => 'Z-Case Part Job',
                'total_days' => '20',
                'watch_maker_days' => '10'
            ],
            [
                'name' => 'Z-Dial Part Job',
                'total_days' => '20',
                'watch_maker_days' => '10'
            ],
            [
                'name' => 'Z-Movement',
                'total_days' => '20',
                'watch_maker_days' => '10'
            ],
            [
                'name' => 'Z-Refurbishment',
                'total_days' => '20',
                'watch_maker_days' => '10'
            ],
            [
                'name' => 'Z-Strap Part Job',
                'total_days' => '20',
                'watch_maker_days' => '10'
         
                ]
        ]
             
        );
    }
}
