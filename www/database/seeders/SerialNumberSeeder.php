<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SerialNumberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('serial_numbers')->insert([
            [
                'brand_name' => 'Rolex',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'Omega',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'Louis Vuitton',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'Raymond Weil',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'HUGO',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'BOSS Orange',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'Scuderia Ferrari Watches',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'MISC',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'Lacoste Watches',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'TW STEEL',
                'serial_number' => rand(100001,999999)
            ],
            [
                'brand_name' => 'Juicy Courture',
                'serial_number' => rand(100001,999999)
            ]

        ]);
    }
}
