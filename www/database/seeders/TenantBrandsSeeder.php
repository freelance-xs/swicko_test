<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\TenantBrand;
use Illuminate\Database\Seeder;
use App\Models\Tenant;

/**
 * Class TenantBrandsSeeder
 * @package Database\Seeders
 */
class TenantBrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $swicoBrands = [
            'RW',
            'TW',
            'HU',
            'BO',
            'CO',
            'LA',
            'MV',
            'OB',
            'SF',
            'TH',
            'MK',
            'MS',
            'PS',
            'DW',
        ];

        $swissMadeBrands = [
            'EB',
            'BM',
            'LG',
            'OM',
            'RX',
        ];

        $tenantBrands = [
            'swico' => $swicoBrands,
            'swissmade' => $swissMadeBrands
        ];

        $tenant = new Tenant();
        $brand = new Brand();
        foreach($tenantBrands as $tenantDatabaseName => $tenantBrandArray){
            $tenant = $tenant->byDatabaseName($tenantDatabaseName)->first();
            if($tenant){
                foreach($tenantBrandArray as $siteBrandValue){
                    $brand = $brand->byShortName($siteBrandValue)->first();
                    if($brand){
                        (new TenantBrand([
                            'tenant_id' => $tenant->id,
                            'brand_id' => $brand->id
                        ]))->save();
                    }
                }
            }
        }
    }
}
