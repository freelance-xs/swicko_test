<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            [
                'name' => 'Type 1',
                
            ],
            [
                'name' => 'Type 2',
               
            ],
            [
                'name' => 'Type 3',
                
            ],
            [
                'name' => 'Type 4',
                
            ],
            [
                'name' => 'Type 5',
               
            ],
            [
                'name' => 'Type 6',
                
            ],
        ]);
    }
}
