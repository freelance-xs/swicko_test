<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CaseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('case_types')->insert([
            [
                'name' => 'Watch Registration'
            ],
            [
                'name' => 'Repair'
            ],
            [
                'name' => 'Stock Maintenance'
            ]
        ]);
    }
}
