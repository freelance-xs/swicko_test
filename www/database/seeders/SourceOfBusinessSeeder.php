<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SourceOfBusinessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('source_of_businesses')->insert([
            [
                'name' => 'Post'
            ],
            [
                'name' => 'Counter'
            ],
            [
                'name' => 'Imported'
            ],
            [
                'name' => 'BOSS Watches UK Website'
            ],
            [
                'name' => 'Eternal Silver London Website'
            ],
            [
                'name' => 'Juicy Courture Watches UK Website'
            ],
            [
                'name' => 'RAYMOND WEIL Aftersales Website'
            ],
            [
                'name' => 'SWICO Website'
            ],
            [
                'name' => 'SWICO WSC Website'
            ],
            [
                'name' => 'SWISSMADE WSC Website'
            ],
            [
                'name' => 'Tommy Hilfiger Watches UK Website'
            ],
            [
                'name' => 'TW STEEL UK Website'
            ],
            [
                'name' => 'In House'
            ],
        ]);
    }
}
