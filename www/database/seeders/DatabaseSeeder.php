<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TenantSeeder::class,
            UserSeeder::class,
            ProductSeeder::class,
            CustomerSeeder::class,
            StatusSeeder::class,
            TypesSeeder::class,
            DepartmentSeeder::class,
            LocationSeeder::class,
            JobTypeSeeder::class,
            HoldStatusSeeder::class,
            AssessmentConditionSeeder::class,
            BrandSeeder::class,
            RepairCaseSeeder::class,
            BrandImageTypesSeeder::class,
            BrandInfoSeeder::class,
            TenantBrandsSeeder::class,
            RepairJobTypeSeeder::class,
            FastTrackBatterySeeder::class,
            CaseTypeSeeder::class,
            SerialNumberSeeder::class,
            SourceOfBusinessSeeder::class,
            BrandImageSeeder::class
        ]);

    }
}
