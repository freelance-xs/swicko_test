const defaultTheme = require("tailwindcss/defaultTheme");
const plugin = require("tailwindcss/plugin");

module.exports = {
    purge: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        colors: {
            transparent: "transparent",
            white: "#fff",
            black: "#000",
            primary: "var(--primary)",
            "dark-grey": "#2b2e34",
            grey: {
                100: "#f2f2f2",
                200: "#d1d1d1",
                300: "#a2a2a2",
                400: "#484848",
                500: "#adb6bb",
            },
            red: "#e04022",
            yellow: "#e6ae13",
            green: "#29c97f",
        },
        fontFamily: {
            sans: ["Montserrat", ...defaultTheme.fontFamily.sans],
        },
        extend: {
            typography: (theme) => ({
                DEFAULT: {
                    css: {
                        p: {
                            marginTop: 0,
                            marginBottom: "10px",
                        },
                    },
                },
            }),
            height: {
                banner: "450px",
            },
            fontSize: {
                lg: [
                    "20px",
                    {
                        letterSpacing: "-0.01em",
                        lineHeight: "1.1",
                    },
                ],
                xl: [
                    "24px",
                    {
                        letterSpacing: "-0.01em",
                        lineHeight: "1.1",
                    },
                ],
                "2xl": [
                    "28px",
                    {
                        letterSpacing: "-0.01em",
                        lineHeight: "1.1",
                    },
                ],
                "3xl": [
                    "32px",
                    {
                        letterSpacing: "-0.02em",
                        lineHeight: "1.1",
                    },
                ],
                "4xl": [
                    "32px",
                    {
                        letterSpacing: "-0.02em",
                        lineHeight: "1.1",
                    },
                ],
            },
            container: {
                padding: "20px",
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require("@tailwindcss/forms"),
        require("@tailwindcss/typography"),
        plugin(function ({ addBase, theme }) {
            addBase({
                h1: { fontSize: theme("fontSize.3xl") },
                h2: { fontSize: theme("fontSize.2xl") },
                h3: { fontSize: theme("fontSize.xl") },
                h4: { fontSize: theme("fontSize.lg") },
                p: { marginTop: 0, marginBottom: "10px" },
                br: {
                    marginBottom: 0,
                    content: "''",
                    display: "block",
                },
            });
        }),
    ],
};
