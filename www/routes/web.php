<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Guest
 */
Route::get('/', fn () => view('frontend.home'))->name('home');
Route::get('service-repair', fn () => view('frontend.service-repair'))->name('service-repair');
Route::get('services-explained', fn () => view('frontend.services-explained'))->name('services-explained');
Route::get('repairs-by-post', fn () => view('frontend.repairs-by-post'))->name('repairs-by-post');
Route::get('estimate-by-post', fn () => view('frontend.estimate-by-post'))->name('estimate-by-post');
Route::get('about', fn () => view('frontend.about'))->name('about');
Route::get('watch-care', fn () => view('frontend.watch-care'))->name('watch-care');
Route::get('warranty-information', fn () => view('frontend.warranty-information'))->name('warranty-information');
Route::get('warranty-claim', fn () => view('frontend.warranty-claim'))->name('warranty-claim');
Route::get('maintenance-care', fn () => view('frontend.maintenance-care'))->name('maintenance-care');
Route::get('testimonials', fn () => view('frontend.testimonials'))->name('testimonials');
Route::get('faqs', fn () => view('frontend.faqs'))->name('faqs');
Route::get('contact', fn () => view('frontend.contact'))->name('contact');
Route::get('watch-brand/{brandName}', fn () => view('frontend.watch-brand'))->name('watch-brand');

/**
 * Terms
 */
Route::get('terms', fn () => view('frontend.policy.terms'))->name('terms');
Route::get('privacy', fn () => view('frontend.policy.privacy'))->name('privacy');
Route::get('cookies', fn () => view('frontend.policy.cookies'))->name('cookies');


/**
 * Authorized
 */
Route::middleware(['auth'])->group(function () {
    /**
     * Cart
     */
    Route::get('cart', fn () => view('frontend.cart', ['items' => collect()]))->name('cart');

    /**
     * Account
     */
    Route::prefix('account')->group(function () {
        /**
         * Overview
         */
        Route::get('overview', fn () => view('frontend.account.account-overview'))->name('account-overview');

        /**
         * Settings
         */
        Route::get('contact-preferences', fn () => redirect(route('account-details', ['#contact-preferences'])))->name('account-contact-preferences');
        Route::get('reset-password', fn () => redirect(route('account-details', ['#reset-password'])))->name('account-reset-password');
        Route::get('address', fn () => redirect(route('account-details', ['#address'])))->name('account-address');
        Route::get('details', fn () => view('frontend.account.account-details'))->name('account-details');

        /**
         * Registered Watches
         */
        Route::get('my-watches', fn () => view('frontend.account.account-my-watches'))->name('account-my-watches');
        Route::get('register-your-watch', fn () => view('frontend.account.account-register-your-watch'))->name('account-register-your-watch');

        /**
         * My Services & Repairs
         */
        Route::get('my-repairs', fn () => view('frontend.account.account-my-repairs'))->name('account-my-repairs');
        Route::get('send-my-watch', fn () => view('frontend.account.account-send-my-watch'))->name('account-send-my-watch');

    });
});
