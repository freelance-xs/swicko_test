<?php

Route::redirect('/', 'dashboard');
Route::middleware('auth')->group(function () {
    Route::get('dashboard', fn() => view('dashboard'))->name('dashboard');
    Route::get('contacts', fn() => view('contacts'))->name('contacts');
    Route::get('cases', fn() => view('cases'))->name('cases');
    Route::get('work', fn() => view('work'))->name('work');
    Route::get('packing-requests', fn() => view('packing-requests'))->name('packing-requests');
    Route::get('reports', fn() => view('reports'))->name('reports');
    Route::get('accounts', fn() => view('accounts'))->name('accounts');
});
