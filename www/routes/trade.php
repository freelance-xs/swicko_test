<?php

Route::middleware('auth')->group(function () {
    Route::get('/', fn() => view('welcome'));
});
