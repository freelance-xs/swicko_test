window.modals = function() {
    return {
        modalOpen: false,
        modalId: null,
        open(id) {
            this.modalOpen = true
            this.modalId = id
            document.getElementById(id).classList.add()
        },
        close(id) {
            this.modalOpen = false
            this.modalId = null
            document.getElementById(id).classList.remove()
        },
        isModalOpen() {
            return this.modalOpen === true
        },
        isOpen(id) {
            return this.modalId === id && this.modalOpen
        }
    }
}
