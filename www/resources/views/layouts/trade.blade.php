<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/frontend/'.site_name().'.css') }}">

@livewireStyles

<!-- Scripts -->
    <script src="{{ mix('js/frontend/'.site_name().'.js') }}" defer></script>
</head>
<body>
<div class="font-sans text-dark-grey text-base antialiased" x-data="modals()">
{{--    <x-navigation/>--}}

    <div class="pt-36">
        {{ $slot }}
    </div>

    <div
        class="fixed bottom-0 z-40 inset-0 sm:px-4 sm:pb-6 sm:inset-0 sm:p-0 sm:flex sm:items-center sm:justify-center"
        x-show="isModalOpen()" x-cloak>
        <div class="fixed inset-0 transition-opacity" x-show="isModalOpen()" x-transition:enter="ease-out duration-300"
             x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
             x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100"
             x-transition:leave-end="opacity-0" x-cloak>
            <div class="absolute inset-0 bg-black bg-opacity-50"></div>
        </div>

        @stack('modals')
    </div>

{{--    <x-footer/>--}}
</div>

@livewireScripts
</body>
</html>
