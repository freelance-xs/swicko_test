<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

@livewireStyles

<!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
</head>
<body>
<div class="font-sans text-dark-grey text-base antialiased h-screen flex overflow-hidden bg-gray-100" x-data="{ sidebarOpen: false, ...modals() }" @keydown.window.escape="sidebarOpen = false">
    <livewire:sidebar/>

    <div class="flex flex-col w-0 flex-1 overflow-hidden">
        <div class="md:hidden pl-1 pt-1 sm:pl-3 sm:pt-3">
            <button @click.stop="sidebarOpen = true" class="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                <span class="sr-only">Open sidebar</span>
                <svg class="h-6 w-6" x-description="Heroicon name: outline/menu" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path>
                </svg>
            </button>
        </div>

        <main class="flex-1 relative overflow-y-auto focus:outline-none" tabindex="0">
            <x-alerts/>

            <div class="py-6">
                <div class="container-fluid mx-auto px-4 sm:px-6 md:px-8">
                    {{ $slot }}
                </div>
            </div>
        </main>

        <div
            class="fixed bottom-0 z-40 inset-0 sm:px-4 sm:pb-6 sm:inset-0 sm:p-0 sm:flex sm:items-center sm:justify-center"
            x-show="isModalOpen()" x-cloak>
            <div class="fixed inset-0 transition-opacity" x-show="isModalOpen()" x-transition:enter="ease-out duration-300"
                 x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
                 x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100"
                 x-transition:leave-end="opacity-0" x-cloak>
                <div class="absolute inset-0 bg-black bg-opacity-50"></div>
            </div>

            @stack('modals')
        </div>
    </div>
</div>

@livewireScripts
</body>
</html>
