@props([
'src' => '',
'showPannel' => false
])


<div class="relative">

    <div class="h-64 md:h-banner relative">
        <img src="{{ $src }}" class="w-full h-full object-cover" />
    </div>

    @if ($showPannel)
        <div class="md:absolute md:-bottom-20 transform md:-translate-x-1/2 w-full" style="left: 50%;">
            <div class="container mx-auto">
                <div class="w-full bg-white p-10 shadow-sm">
                    {{ $slot }}
                </div>
            </div>
        </div>
    @endif

</div>
