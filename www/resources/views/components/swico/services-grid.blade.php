<div class="py-10 md:py-24 px-10 bg-dark-grey">
    <div class="container mx-auto">
        <x-cta.box-wrapper class="md:flex">
            <x-cta.box class="md:w-1/3 my-3" background="{{ site_name() === 'swico' ? asset_url('image/shutterstock_1058967827.jpg') : asset_url('image/dial.jpg') }}">
                <x-slot name="icon">
                    <img src="{{ asset_url('icon/tick_shield.png') }}" alt="shield">
                </x-slot>
                <h3 class="text-white text-xl mt-5 mb-8 uppercase">Warranty <br> Claim</h3>
                <x-slot name="button">
                    <a class="btn btn-black text-primary uppercase w-full text-center" href="{{ route('warranty-claim') }}">Start Your Warranty Claim</a>
                </x-slot>
            </x-cta.box>
            <x-cta.box class="md:w-1/3 my-3" bg-color="bg-white" background="{{ site_name() === 'swico' ? asset_url('image/shutterstock_796998178.jpg') : asset_url('image/Ebelmovement.jpeg') }}">
                <x-slot name="icon">
                    <img src="{{ asset_url('icon/settings.png') }}" alt="settings">
                </x-slot>
                <h3 class="text-black text-xl mt-5 mb-8 uppercase">SERVICE & </br> REPAIR</h3>
                <x-slot name="button">
                    <a class="btn btn-grey uppercase w-full text-center" href="{{ route('service-repair') }}">start
                        service & repair</a>
                </x-slot>
            </x-cta.box>
            <x-cta.box class="md:w-1/3 my-3" bg-color="bg-grey-500" background="{{ asset_url('image/shutterstock_140419135.jpg') }}">
                <x-slot name="icon">
                    <img src="{{ asset_url('icon/watch.png') }}" alt="watch">
                </x-slot>
                <h3 class="text-white text-xl mt-5 mb-8 uppercase">register with {{site_name()}} watchcare</h3>
                <x-slot name="button">
                    <a class="btn btn-primary uppercase w-full text-center" href="{{ route('watch-care') }}">
                        Register A Watch
                    </a>
                </x-slot>
            </x-cta.box>
        </x-cta.box-wrapper>
    </div>
</div>