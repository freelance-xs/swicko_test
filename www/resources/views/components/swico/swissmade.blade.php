@props([
'mode' => 'light',
])

<div class="{{ $mode == 'dark' ? 'bg-dark-grey' : 'bg-grey-100' }} py-24">
    <div class="container mx-auto px-4 text-center">
        <h3 class="uppercase text-3xl font-bold  mb-2 {{ $mode == 'dark' ? 'text-white' : 'text-black' }}">SWISSMADE LUXURY WATCH REPAIR & SERVICING</h3>
        <h4 class="text-xl text-grey-500">From Hatton Garden in the heart of London’s jewellery quarter, Swissmade <br>
            specialise in the repair and maintenance of fine Swissmade watches.</h4>
        <div class="flex flex-wrap my-24 space-y-12 md:space-y-0">
            <div class="w-1/12"></div>
            <div class="w-full flex justify-center sm:w-2/12 px-3">
                <img src="{{ asset_url('image/watch_ebel_ftc_logo' . ($mode == 'dark' ? '_white' : '') . '.png') }}" alt="ebel">
            </div>
            <div class="w-full flex justify-center sm:w-2/12 px-3">
                <img src="{{ asset_url('image/watch_baume_ftc_logo' . ($mode == 'dark' ? '_white' : '') . '.png') }}" alt="ebel">
            </div>
            <div class="w-full flex justify-center sm:w-2/12 px-3">
                <img src="{{ asset_url('image/watch_longines_ftc_logo' . ($mode == 'dark' ? '_white' : '') . '.png') }}" alt="ebel">
            </div>
            <div class="w-full flex justify-center sm:w-2/12 px-3">
                <img src="{{ asset_url('image/watch_omega_ftc_logo' . ($mode == 'dark' ? '_white' : '') . '.png') }}" alt="ebel">
            </div>
            <div class="w-full flex justify-center sm:w-2/12 px-3">
                <img src="{{ asset_url('image/watch_rolex_ftc_logo' . ($mode == 'dark' ? '_white' : '') . '.png') }}" alt="ebel">
            </div>
            <div class="w-1/12"></div>
        </div>
        <div class="flex justify-center">
            <a class="btn btn-primary uppercase w-full md:w-auto" href="https://www.swissmadewatchservicecentre.co.uk/">Visit Swissmade</a>
        </div>
    </div>
</div>
