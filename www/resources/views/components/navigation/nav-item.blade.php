@php $classes = currentRouteIs($route) ? 'text-white bg-white bg-opacity-10' : 'text-grey-300' @endphp

<a href="{{ route($route) }}"
   class="hover:text-white group flex items-center px-3 py-3 text-sm font-medium rounded-md {{ $classes }} {{ $minimised ? 'justify-center' : '' }}">
    {{ svg('icon-'.$icon, "group-hover:text-white h-4 w-4 $classes") }}
    @unless ($minimised)
        <span class="ml-3">{{ $slot }}</span>
    @endif
</a>
