@props(['route'])

<a {!! $attributes->merge(['class' => 'text-white px-6 py-2 hover:bg-dark-grey hover:text-white']) !!} href="{{ $route }}">{{ $slot }}</a>
