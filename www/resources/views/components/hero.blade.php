@props([
'backgroundImage' => '',
'backgroundOverlay' => '',
'preTitle' => '',
'bannerTitle' => ''
])

<div class="bg-no-repeat bg-cover" style="background-image: url('{{ $backgroundImage }}')">
    <div class="bg-no-repeat bg-right-bottom" style="background-image: url('{{ $backgroundOverlay }}')">
        <div class="container mx-auto px-6 @unless(isset($extraContent)) h-96 @endif">
            <div class="md:flex items-end">
                <div class="text-white flex-1">
                    <p class="text-primary pt-10 md:pt-32 uppercase pb-3">{{ $preTitle ?? '' }}</p>
                    <h1 class="text-white text-3xl md:text-5xl uppercase">{{ $mainTitle }}</h1>
                </div>
                @if (isset($rightColumn))
                    <div class="flex-1">
                        {{ $rightColumn }}
                    </div>
                @endif
            </div>
            <div class="pb-6">
                {{ $slot }}
            </div>
            @if (isset($extraContent))
                <div class="mt-12">
                    {{ $extraContent }}
                </div>
            @endif
        </div>
    </div>
</div>
