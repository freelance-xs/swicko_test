@props([
    'image',
    'left' => false,
    'bgColor' => 'white'
])

<div class="bg-{{ $bgColor }} flex flex-wrap">
    <div class="w-full md:w-1/2 flex items-center {{ $left ? 'md:order-2' : 'md:order-1 justify-end' }}">
        <div class="max-w-3xl px-6 py-6 {{ $left ? 'md:pr-12 md:pl-24' : 'md:pl-12 md:pr-24' }}">
            {{ $slot }}
        </div>
    </div>
    <div class="w-full md:w-1/2 {{ $left ? 'order-2 md:order-1' : 'order-1 md:order-2' }}">
        <img src="{{ asset_url($image) }}" class="w-full h-full object-cover" alt="...">
    </div>
</div>
