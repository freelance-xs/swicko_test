@props([
    'label' => 'note',
    'bgColor' => 'grey-100'
])

<div {{$attributes}}>

<div class="bg-dark-grey p-5 flex items-center mb-5">
    <img src="{{asset_url('icon/information.png')}}" alt="..." class="mr-5">
    <span class="text-white uppercase">{{$label ?? 'label'}}</span>
</div>

<div class="bg-{{ $bgColor }} p-5">
    {{$slot}}
</div>

</div>