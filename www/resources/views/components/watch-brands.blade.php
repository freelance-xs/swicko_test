@props([
    'columns' => 5,
    'watches' => [],
    'mode' => 'light'
])

<div class="{{$mode == 'light' ? 'bg-grey-100' : 'bg-dark-grey'}} py-20">
    <div class="container mx-auto px-4">
        <div class="text-center">
            <h2 class="{{$mode == 'light' ? 'text-black' : 'text-white'}} text-3xl uppercase font-bold tracking-tight mb-3">Watch brands we look after</h2>
            <h4 class="text-grey-500 mb-3">Our in-house specialist Watchmakers are trained experts in
                maintaining, repairing & servicing your watch, using genuine manufacturers parts and the specialist
                tools & equipment.</h4>
        </div>
        <div class="my-20 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-{{ $columns }} gap-12">
            @foreach ($watches as $watch)
                <div class="flex justify-center items-center">
                    <img class="object-contain" src="{{ $mode == 'light' ? asset_url('logo/'.$watch.'_grey.png') : asset_url('logo/'.$watch.'_white.png')  }}" alt="{{ $watch }}">
                </div>
            @endforeach
        </div>
        <div class="text-center">
            <a class="btn btn-primary uppercase" href="{{ route('about') }}">About {{ site_name() }}</a>
        </div>
    </div>
</div>
