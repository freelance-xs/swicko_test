@props([
    'src' => null
])

<iframe {{ $attributes->merge(['style' => 'border:0;']) }} src="{{ $src ?? 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d20049.33644236685!2d-0.7513501762885465!3d51.08692442946791!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4875cb509f331be7%3A0x932cbf9e0d213408!2sHaslemere%20GU27%201NN!5e0!3m2!1sen!2suk!4v1614564288416!5m2!1sen!2suk' }}" allowfullscreen="" loading="lazy"></iframe>
