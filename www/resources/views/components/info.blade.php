@props([
    'icon' => null,
])

<div class="p-5 flex bg-dark-grey text-white text-lg mt-10 w-full">
    <div>
        <img class="mr-3" src="{{ $icon ?? asset_url('icon/shield.png') }}" alt="...">
    </div>
    <div class="text-base">
        {{$slot}}
    </div class="text-base">
</div>