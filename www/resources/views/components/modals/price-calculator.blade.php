<x-modals.modal id="price-calculator" class="md:max-w-5xl">
    <div class="container mx-auto px-12 bg-black py-16"
         style="background-image: url({{ asset_url('logo/logo_swico_white.png') }}); background-position: -800% 90%;">
        <div class="w-full flex justify-end">
            <button type="button"
                    class="bg-white rounded-full text-3xl w-8 h-8 flex items-center justify-center font-bold focus:outline-none"
                    @click="close('price-calculator'); Livewire.emit('reset');">
                ×
            </button>
        </div>
        <div>
            <h2 class="text-2xl text-white flex items-center lg:text-3xl">
                <img src="{{ asset_url('icon/navbar/pound.png') }}" width="40" height="40">
                <span class="pl-3">QUICK QUOTE</span>
            </h2>
        </div>

        <livewire:price-calculator/>

        <div class="mb-12">
            <p class="text-white">PLEASE BE AWARE THIS IS A GUIDE ONLY, WE PROVIDE AN ACCURATE ESTIMATE &amp; TIMESCALE
                WHEN WE RECEIVE YOUR WATCH</p>
        </div>
    </div>
</x-modals.modal>
