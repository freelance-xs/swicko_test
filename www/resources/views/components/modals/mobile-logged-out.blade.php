<x-modals.modal id="navbar-mobile-logged-out">
    <div class="w-full h-screen bg-primary text-lg">
        <div class="container mx-auto px-6">
            <div class="flex items-center py-6">
                <div class="w-1/3">
                    <button @click="close('navbar-mobile-logged-out')" class="self-end">
                        <img src="{{ asset_url('icon/navbar/close-blank.png') }}">
                    </button>
                </div>

                <div class="w-1/3 flex justify-center">
                    <img class="w-11 h-11"  src="{{ asset_url('logo/logo_swico_white_small.png') }}">
                </div>

                <div class="w-1/3"></div>
            </div>

            <div class="grid gap-4 px-4 py-12">
                <p class="uppercase text-2xl font-bold text-black">Welcome
                    to {{ strtoupper(site_name()) }}</p>
                <p class="text-white text-lg">Create an account to register, service or
                    repair your watch.</p>
                <a href="{{ route('login') }}"
                   class="btn btn-black w-full text-center text-base font-normal">LOGIN TO SWICO</a>
                <a href="{{ route('watch-care') }}"
                   class="btn btn-primary w-full underline text-base font-normal pl-0">CREATE
                    MY ACCOUNT</a>
            </div>
        </div>
    </div>
</x-modals.modal>
