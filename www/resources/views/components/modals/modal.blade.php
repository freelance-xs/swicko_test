<div id="{{ $id }}" x-show="isOpen('{{ $id }}')" @click.away="close('{{ $id }}'); Livewire.emit('reset');"
     {!! $attributes->merge(['class' => 'overflow-hidden shadow-xl transform transition-all sm:max-w-3xl sm:w-full']) !!}
     role="dialog" aria-modal="true" aria-labelledby="modal-headline"
     x-transition:enter="ease-out duration-300"
     x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
     x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100" x-transition:leave="ease-in duration-200"
     x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
     x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
     x-cloak>
    <!-- Modal content-->
    {{ $slot }}
</div>
