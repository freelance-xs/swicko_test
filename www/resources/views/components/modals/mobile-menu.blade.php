<x-modals.modal id="navbar-mobile">
    <div class="w-full h-screen bg-dark-grey text-lg">
        <div class="container mx-auto px-6">
            <div class="flex items-center py-6">
                <div class="w-1/3"></div>

                <div class="w-1/3 flex justify-center">
                    @if (site_name() === 'swico')

                        <img src="{{ asset_url('/logo/logo_swico_mobile_modal.png') }}">
                    @else
                        <img src="{{ asset_url('/logo/logo_swissmade_white.png') }}" style="width: 44px;">

                    @endif
                </div>

                <div class="w-1/3 flex justify-end">
                    <button @click="close('navbar-mobile')" class="self-end">
                        <img src="{{ asset_url('/icon/navbar/close-blank.png') }}">
                    </button>
                </div>
            </div>

            <div>
                <x-accordion.accordion class="grid gap-3">
                    <x-accordion.accordion-item index="1" id="item1">
                        <x-slot name="trigger">
                            <div class="flex items-center text-white focus:outline-none">
                                <span class="pr-2">Watch Brands</span>
                                <img src="{{ asset_url('/icon/navbar/plus_green.png') }}">
                            </div>
                        </x-slot>
                        <div class="grid py-4">
                            @if (site_name() === 'swico')
                                <a class="text-primary pb-1" href="">Raymond Weil</a>
                                <a class="text-primary pb-1" href="">TW Steel</a>
                                <a class="text-primary pb-1" href="">HUGO BOSS</a>
                                <a class="text-primary pb-1" href="">BOSS Watches</a>
                                <a class="text-primary pb-1" href="">Coach</a>
                                <a class="text-primary pb-1" href="">Lacoste</a>
                                <a class="text-primary pb-1" href="">Movado</a>
                                <a class="text-primary pb-1" href="">Olivia Burton</a>
                                <a class="text-primary pb-1" href="">Scuderia Ferrari</a>
                                <a class="text-primary pb-1" href="">Tommy Hilfiger</a>
                                <a class="text-primary pb-1" href="">Rebecca Minkoff</a>
                                <a class="text-primary pb-1" href="">Maserati</a>
                                <a class="text-primary pb-1" href="">Paul Smith Watches</a>
                            @else
                                <a class="text-primary pb-1" href="">Ebel</a>
                                <a class="text-primary pb-1" href="">Baume & Mercier</a>
                                <a class="text-primary pb-1" href="">Longines</a>
                                <a class="text-primary pb-1" href="">Omega</a>
                                <a class="text-primary pb-1" href="">Rolex</a>
                            @endif
                        </div>
                    </x-accordion.accordion-item>

                    <x-accordion.accordion-item index="2" id="item2">
                        <x-slot name="trigger">
                            <div class="flex items-center text-white focus:outline-none">
                                <span class="pr-2">Send Your Watch</span>
                                <img src="{{ asset_url('/icon/navbar/plus_green.png') }}">
                            </div>
                        </x-slot>
                        <div class="grid py-4">
                            <a class="text-primary pb-1" href="{{ route('warranty-claim') }}">Start Warranty
                                Claim</a>
                            <a class="text-primary pb-1" href="{{ route('service-repair') }}">Start Service &
                                Repair</a>
                            <a class="text-primary pb-1" href="{{ route('repairs-by-post') }}">How it works</a>
                        </div>
                    </x-accordion.accordion-item>

                    <div class="p-3 my-3 shadow">
                        <a class="text-white" href="{{ route('watch-care') }}">{{ site_name() }}</a>
                    </div>

                    <x-accordion.accordion-item index="3" id="item3">
                        <x-slot name="trigger">
                            <div class="flex items-center text-white focus:outline-none">
                                <span class="pr-2">Customer Information</span>
                                <img src="{{ asset_url('/icon/navbar/plus_green.png') }}">
                            </div>
                        </x-slot>
                        <div class="grid py-4">
                            <a class="text-primary pb-1" href="{{ route('about') }}">About SWICO</a>
                            <a class="text-primary pb-1" href="{{ route('repairs-by-post') }}">Repairs By Post</a>
                            <a class="text-primary pb-1" href="{{ route('services-explained') }}">Service
                                Explained</a>
                            <a class="text-primary pb-1" href="{{ route('warranty-information') }}">Warranty
                                Information</a>
                            <a class="text-primary pb-1" href="{{ route('maintenance-care') }}">Maintenance &
                                Care</a>
                            <a class="text-primary pb-1" href="{{ route('testimonials') }}">Testimonials</a>
                            <a class="text-primary pb-1" href="{{ route('estimate-by-post') }}">Received Estimate By
                                Post</a>
                            <a class="text-primary pb-1" href="{{ route('faqs') }}">FAQs</a>
                        </div>
                    </x-accordion.accordion-item>

                    <div class="p-3 my-3 shadow">
                        <a class="text-white" href="{{ route('contact') }}">Contact</a>
                    </div>
                </x-accordion.accordion>

                <div class="mt-6">
                    <button type="button" @click="open('price-calculator')" class="btn btn-primary text-white">
                        QUICK QUOTE
                    </button>
                </div>
            </div>
        </div>
    </div>
</x-modals.modal>
