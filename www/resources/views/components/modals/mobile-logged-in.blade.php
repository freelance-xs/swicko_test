<x-modals.modal id="navbar-mobile-logged-in">
    <div class="w-full h-screen bg-primary text-lg">
        <div class="container mx-auto px-6">
            <div class="flex items-center py-6">
                <div class="w-1/3">
                    <button @click="close('navbar-mobile-logged-in')" class="self-end">
                        <img src="{{ asset_url('icon/navbar/close-blank.png') }}">
                    </button>
                </div>

                <div class="w-1/3 flex justify-center">
                    <img class="w-11 h-11" src="{{ asset_url('logo/logo_swico_white_small.png') }}">
                </div>

                <div class="w-1/3"></div>
            </div>

            <div>
                <div class="flex items-center py-4 border-b border-white mb-4">
                    <img class="h-5 w-5 mr-3 inline-block object-contain" src="{{ asset_url('icon/login.png') }}" alt="...">
                    <p class="text-white text-lg font-bold">{{ Auth::user()->fullName }}</p>
                </div>
                <x-accordion.accordion class="grid gap-3">
                    <div>
                        <img class="h-5 w-5 mr-3 inline-block object-contain"
                             src="{{ asset_url('icon/navbar/screen@2x.png') }}" alt="...">
                        <a class="text-black" href="{{ route('home') }}">Account Overview</a>
                    </div>

                    <x-accordion.accordion-item index="1" id="item1">
                        <x-slot name="trigger">
                            <div class="flex items-center text-black focus:outline-none">
                                <img class="h-5 w-5 mr-3 inline-block object-contain"
                                     src="{{ asset_url('icon/navbar/option_bar_settings@2x.png') }}" alt="...">
                                <span class="pr-2">Account Settings</span>
                                <img src="{{ asset_url('icon/navbar/plus.png') }}">
                            </div>
                        </x-slot>
                        <div class="grid py-4">
                            <p>
                                <img class="h-5 w-5 mr-3 inline-block"
                                     src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                <a class="text-white pb-1" href="">My Details</a>
                            </p>
                            <p>
                                <img class="h-5 w-5 mr-3 inline-block"
                                     src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                <a class="text-white pb-1" href="">My Addresses</a>
                            </p>
                            <p>
                                <img class="h-5 w-5 mr-3 inline-block"
                                     src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                <a class="text-white pb-1" href="">Change Password</a>
                            </p>
                            <p>
                                <img class="h-5 w-5 mr-3 inline-block"
                                     src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                <a class="text-white pb-1" href="">Contact Preferences</a>
                            </p>
                        </div>
                    </x-accordion.accordion-item>

                    <x-accordion.accordion-item index="2" id="item2">
                        <x-slot name="trigger">
                            <div class="flex items-center text-black focus:outline-none">
                                <img class="h-5 w-5 mr-3 inline-block object-contain"
                                     src="{{ asset_url('icon/navbar/watch@2x.png') }}" alt="...">
                                <span class="pr-2">Registered Watches</span>
                                <img src="{{ asset_url('icon/navbar/plus.png') }}">
                            </div>
                        </x-slot>
                        <div class="grid py-4">
                            <p>
                                <img class="h-5 w-5 mr-3 inline-block"
                                     src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                <a class="text-white pb-1" href="">My Watches</a>
                            </p>
                            <p>
                                <img class="h-5 w-5 mr-3 inline-block"
                                     src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                <a class="text-white pb-1" href="">Register your watch</a>
                            </p>
                        </div>
                    </x-accordion.accordion-item>

                    <x-accordion.accordion-item index="3" id="item3">
                        <x-slot name="trigger">
                            <div class="flex items-center text-black focus:outline-none">
                                <img class="h-5 w-5 mr-3 inline-block object-contain"
                                     src="{{ asset_url('icon/navbar/settings@2x_white.png') }}" alt="...">
                                <span class="pr-2">My Services & Repairs</span>
                                <img src="{{ asset_url('icon/navbar/plus.png') }}">
                            </div>
                        </x-slot>
                        <div class="grid py-4">
                            <p>
                                <img class="h-5 w-5 mr-3 inline-block"
                                     src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                <a class="text-white pb-1" href="">View my repairs</a>
                            </p>
                            <p>
                                <img class="h-5 w-5 mr-3 inline-block"
                                     src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                <a class="text-white pb-1" href="">Send my watch</a>
                            </p>
                        </div>
                    </x-accordion.accordion-item>

                    <div>
                        <form class="inline-block" method="post" action="{{ route('logout') }}">
                            @csrf
                            <button class="text-black py-2 hover:underline focus:outline-none">
                                <img class="h-5 w-5 mr-3 inline-block object-contain"
                                     src="{{ asset_url('icon/navbar/padlock_white@2x.png') }}" alt="...">Logout
                            </button>
                        </form>
                    </div>
                </x-accordion.accordion>
            </div>
        </div>
    </div>
</x-modals.modal>
