@props([
    'contain' => false
])

<div {{ $attributes->merge(['class' => 'md:py-20 py-10']) }}>
    @if ($contain)
        <div class="container mx-auto">
            {{ $slot }}
        </div>
    @else
        {{ $slot }}
    @endif
</div>
