<div class="fixed inset-0 flex flex-col items-end justify-center px-4 py-6 pointer-events-none sm:p-6 sm:justify-start space-y-4">
    @foreach (['success', 'warning', 'danger', 'alert'] as $type)
        @if ($type === 'warning')
            {{ svg('icon-warning', 'text-yellow h-5 w-5') }}
</div>
<div class="ml-3 w-0 flex-1 pt-0.5">
    <p class="text-sm font-medium text-yellow">
        {{ $component->message() }}
    </p>
    {{-- <p class="mt-1 text-sm text-gray-500"> --}}
    {{-- Anyone with a link can now view this file. --}}
    {{-- </p> --}}
</div>
@elseif ($type === 'danger')
<div class="flex-shrink-0">
    {{ svg('icon-warning', 'text-red h-5 w-5') }}
</div>
<div class="ml-3 w-0 flex-1 pt-0.5">
    <p class="text-sm font-medium text-red">
        {{ $component->message() }}
    </p>
    {{-- <p class="mt-1 text-sm text-gray-500"> --}}
    {{-- Anyone with a link can now view this file. --}}
    {{-- </p> --}}
</div>
@elseif ($type === 'success')
<div class="flex-shrink-0">
    {{ svg('icon-checkmark-circle', 'text-green h-5 w-5') }}
</div>
<div class="ml-3 w-0 flex-1 pt-0.5">
    <p class="text-sm font-medium text-green">
        {{ $component->message() }}
    </p>
    {{-- <p class="mt-1 text-sm text-gray-500"> --}}
    {{-- Anyone with a link can now view this file. --}}
    {{-- </p> --}}
</div>
@else
<div class="ml-3 w-0 flex-1 pt-0.5">
    <p class="text-sm font-medium">
        {{ $component->message() }}
    </p>
    {{-- <p class="mt-1 text-sm text-gray-500"> --}}
    {{-- Anyone with a link can now view this file. --}}
    {{-- </p> --}}
</div>
@endif
</x-crm.alert>
@endforeach
</div>
