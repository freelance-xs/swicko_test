<div class="container mx-auto px-6">
    <div class="py-12">
        <footer class="flex flex-wrap space-y-4 sm:space-y-0">
            <div class="w-full sm:w-6/12">
                <img src="{{ asset_url('/logo/footer-logo.jpg') }}" alt="..." style="max-width: 100px">
                <p class="mt-8">
                    @if (site_name() === 'swico')
                        <b>SWICO Watch Service Centre</b><br>
                        Meadway, Haslemere, Surrey, GU27 1NN<br>
                        <a class="text-dark-grey hover:text-dark-grey hover:underline" href="mailto:watchrepairs@swicogroup.com">watchrepairs@swicogroup.com</a><br>
                        +44 (0)1428 656822
                    @elseif (site_name() === 'swissmade')
                        <b>SWISSMADE Watch Service Centre</b><br>
                        Unit 21 - 22, Arundel House, 43 Kirby Street, London EC1N 8TE<br>
                        <a class="text-dark-grey hover:text-dark-grey hover:underline" href="mailto:info@swissmade.co.uk">info@swissmade.co.uk</a><br>
                        +44 (0)20 7405 8504
                    @endif
                </p>
            </div>
            <div class="w-full sm:w-6/12 sm:pb-4 lg:w-2/12 px-0 lg:px-5">
                <div class="flex flex-col space-y-2">
                    <p class="font-bold">CUSTOMER INFO</p>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('repairs-by-post') }}">
                        {{ site_name() === 'swico' ? 'Repairs By Post' : 'Online Watch Repairs' }}
                    </a>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('services-explained') }}">Service Explained</a>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('maintenance-care') }}">Maintenance & Care</a>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('warranty-information') }}">Warranty Information</a>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('testimonials') }}">Testimonials</a>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('faqs') }}">FAQs</a>
                </div>
            </div>
            <div class="w-full sm:w-6/12 lg:w-2/12 px-0 lg:px-5">
                <div class="flex flex-col space-y-2">
                    <p class="font-bold">PRIVACY & TERMS</p>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('privacy') }}">Privacy
                        Policy</a>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('terms') }}">Terms &
                        Conditions</a>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('cookies') }}">Cookies</a>
                </div>
            </div>
            <div class="w-full sm:w-6/12 lg:w-2/12 lg:pl-5 pl-0">
                <div class="flex flex-col space-y-2">
                    <p class="font-bold">COMPANY INFO</p>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('about') }}">About
                        Us</a>
                    <a class="text-dark-grey hover:text-dark-grey hover:underline" href="{{ route('contact') }}">Contact
                        Us</a>
                </div>
            </div>
            {{-- <div class="col-md-2 col-sm-6" hidden=""> --}}
            {{-- <p> --}}
            {{-- <b>FOLLOW US</b> --}}
            {{-- </p> --}}
            {{-- <p> --}}
            {{-- <span> --}}
            {{-- <a href="#"> --}}
            {{-- <img src="/themes/swicoV2/images/icon/Twitter.png" alt="..."> --}}
            {{-- </a> --}}
            {{-- &nbsp; --}}
            {{-- <a href="#"> --}}
            {{-- <img src="/themes/swicoV2/images/icon/LinkedIn.png" alt="..."> --}}
            {{-- </a> --}}
            {{-- &nbsp; --}}
            {{-- <a href="#"> --}}
            {{-- <img src="/themes/swicoV2/images/icon/Facebook.png" alt="..."> --}}
            {{-- </a> --}}
            {{-- &nbsp; --}}
            {{-- <a href="#"> --}}
            {{-- <img src="/themes/swicoV2/images/icon/Instagram.png" alt="..."> --}}
            {{-- </a> --}}
            {{-- </span> --}}
            {{-- </p> --}}
            {{-- </div> --}}
        </footer>
    </div>
</div>
