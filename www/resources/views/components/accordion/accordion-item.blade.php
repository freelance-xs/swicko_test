<div {{ $attributes->merge(['class' => 'p-3 my-3 shadow']) }}>
    <button class="focus:outline-none w-full" type="button" @click="selected !== {{ $index }} ? selected = {{ $index }} : selected = null">
        <div class="flex justify-between items-start">
            <div class="text-lg text-left hover:underline">
                {{ $trigger }}
            </div>
            <div class="px-3">
                <svg class="w-6 text-primary" aria-hidden="true" data-prefix="fa" data-icon="plus-square" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor" d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-32 252c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92H92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z" />
                </svg>
            </div>
        </div>
    </button>

    <div class="relative overflow-hidden transition-all max-h-0 duration-200" x-ref="{{ $id }}" x-bind:style="selected == {{ $index }} ? `max-height: ${$refs.{{ $id }}.scrollHeight}px` : ''">
        <hr class="border-grey-200 mt-5 mb-3">
        <div class="text-grey-400 text-sm">
            {{ $slot }}
        </div>
    </div>
</div>
