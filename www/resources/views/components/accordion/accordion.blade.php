<div x-data="{ selected: null }" {!! $attributes !!}>
    {{ $slot }}
</div>
