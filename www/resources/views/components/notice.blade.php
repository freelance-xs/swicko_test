<div class="text-center">
    <div class="text-black bg-white mt-5 p-5">
        <div class="w-full px-5 prose max-w-none">
            @if (site_name() === 'swico')

                <p>
                    In line with government advice, we will now be operating with a skeleton staff to deal with emergencies.
                </p>
                <p>
                    If you have repairs you can continue to send these to us, rest assured they will be stored in a safe
                    place and processed in due course.
                </p>
                <p>
                    There will inevitably be delays to our usual services for which we sincerely apologise, however as soon
                    as restrictions are lifted, we will be back to normal operations.
                </p>
                <p>
                    If you have an emergency, you can contact us by email on <a href="mailto:sales@swicogroup.com" target="_blank">sales@swicogroup.com</a>
                    or <a href="mailto:sales@afhdistribution.co.uk" target="_blank">sales@afhdistribution.co.uk</a>.
                </p>
            @elseif( site_name() === 'swissmade' )
                <p>
                    Swissmade has made the difficult decision to follow government advice and go into full lockdown as of close of business Friday 5th February.

                </p>
                <p>
                    Please do not send repairs to us until we return. As soon as restrictions are lifted, we will be back to normal operations.

                </p>
                <p class="font-bold">
                    Our office will be open for normal business from Monday 29th March 2021
                </p>
            @endif
        </div>
    </div>
</div>
