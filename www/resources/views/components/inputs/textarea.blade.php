@props(['disabled' => false])

<textarea {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'block mt-1 w-full text-sm border-grey-300 focus:border-primary focus:ring-primary']) !!}>
</textarea>
