<button {{ $attributes->merge(['type' => 'submit', 'class' => 'btn btn-black uppercase disabled:opacity-25 transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>
