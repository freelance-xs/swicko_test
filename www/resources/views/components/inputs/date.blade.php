@props([
    'id' => '',
    'label' => null
])

<label for="{{$id}}" class="mb-2 inline-block text-sm">{{$label}}</label>
<input type="date" {{$attributes->merge(['class' => 'block mt-1 w-full text-sm border-grey-300 focus:border-primary focus:ring-primary'])}} />