<div {{ $attributes->merge(['class' => 'my-5 relative', 'id' => $id ?? '']) }}>{{ $slot }}</div>
