<select {!! $attributes->merge(['class' => 'block mt-1 w-full text-sm focus:border-primary focus:ring-primary']) !!}>
    {{ $slot }}
</select>
