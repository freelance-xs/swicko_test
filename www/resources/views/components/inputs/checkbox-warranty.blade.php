<div {{$attributes}}>
    <label for="warranty_acceptance" class="text-lg">
        <x-inputs.checkbox id="warranty_acceptance" type="textarea" name="warranty_acceptance" placeholder="Your enquiry / message to us *"
        rows="8" value="{{ old('warranty_acceptance') }}" required />
        <span class="ml-2 text-sm">By checking this box you agree that you have read and accepted the <a class="underline" href="{{'warranty-claim'}}">Warranty Information</a></span>
    </label>
</div>