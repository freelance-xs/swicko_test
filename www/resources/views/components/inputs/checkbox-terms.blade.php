<div {{$attributes}}>
    <label for="terms_acceptance" class="text-lg">
        <x-inputs.checkbox id="terms_acceptance" type="textarea" name="terms_acceptance" placeholder="Your enquiry / message to us *"
            rows="8" value="{{ old('terms_acceptance') }}" required />
        <span class="ml-2 text-sm">By checking this box you agree to our <a class="underline" href="{{route('terms')}}">Terms & Conditions</a> and <a class="underline" href="{{route('privacy')}}">Privacy Policy</a></span>
    </label>
</div>