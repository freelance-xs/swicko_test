@props([
    'id' => null,
    'label' => null
])

<input type="radio" {{$attributes->merge(['class' => 'focus:border-primary focus:ring-primary'])}}>
<label for="{{$id}}">{{$label}}</label>