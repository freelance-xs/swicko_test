<div {{ $attributes }}>
    <div id="recaptcha"></div>
</div>

<script type="text/javascript">
    var onloadCallback = function() {
        grecaptcha.render('recaptcha', {
            'sitekey': '{{ config('google.reCAPTCHA.site_key')}}',
            'callback': verifyCallback
        });
    };

    var verifyCallback = function(response) {
        // alert(response);
    };

</script>

<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
