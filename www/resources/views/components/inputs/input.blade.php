@props(['disabled' => false, 'mode' => null, 'label' => null, 'id' => null])

@if(isset($label))
<label for="{{$id}}" class="inline-block mb-2 text-small">{{$label}}</label>
@endif

@if ($mode == 'light')
    <input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'block mt-1 w-full text-sm border-white bg-transparent placeholder-white focus:border-primary focus:ring-primary']) !!}>
@endif

@if (!$mode)
    <input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'block mt-1 w-full text-sm border-grey-300 focus:border-primary focus:ring-primary']) !!}>
@endif
