<div class="flex flex-col">
    <label class="text-xs leading-loose" for="{{ $name }}">{{ Str::title(str_replace(['_', '-'], ' ', $name)) }}</label>
    <input
        name="{{ $name }}"
        type="email"
        id="{{ $id }}"
        @if($value)value="{{ $value }}"@endif
        {{ $attributes->class(['text-sm border border-grey-200 rounded-md text-dark-grey focus:bg-grey-100 focus:ring-0 focus:border-grey-200', 'border-red focus:border-red' => $errors->has($name)]) }}
    />
    @error($name)
    <div class="-mt-3 mx-2">
        <span class="text-red bg-white px-2 py-1 text-xs">{{ $message }}</span>
    </div>
    @enderror
</div>
