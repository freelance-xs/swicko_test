@props([
    'minimised' => false
])

<div>
    <span class="h-px bg-grey-400 w-full mb-6 block"></span>
    <p class="text-white font-semibold mb-6 {{ $minimised ? 'text-xl' : 'text-2xl' }}">
        {{ $minimised ? Auth::user()->initials : Auth::user()->fullname }}
    </p>
    <x-navigation.nav-item icon="dashboard" route="dashboard" :minimised="$minimised">Dashboard</x-navigation.nav-item>
    <x-navigation.nav-item icon="contacts" route="contacts" :minimised="$minimised">Contacts</x-navigation.nav-item>
    <x-navigation.nav-item icon="cases" route="cases" :minimised="$minimised">Cases</x-navigation.nav-item>
    <x-navigation.nav-item icon="work" route="work" :minimised="$minimised">Work</x-navigation.nav-item>
    <x-navigation.nav-item icon="packing-requests" route="packing-requests" :minimised="$minimised">Packing Requests
    </x-navigation.nav-item>
    <x-navigation.nav-item icon="reports" route="reports" :minimised="$minimised">Reports</x-navigation.nav-item>
    <x-navigation.nav-item icon="accounts" route="accounts" :minimised="$minimised">Accounts</x-navigation.nav-item>

    <div class="border-t border-b border-grey-400 text-white mt-3">
        <form method="post" action="{{ route('logout') }}">
            @csrf
            <button
                class="text-grey-300 hover:text-white group flex items-center px-3 py-3 text-sm font-medium rounded-md w-full {{ !$minimised ?: 'justify-center' }}"
                type="submit">
                {{ svg('icon-logout', 'text-grey-300 group-hover:text-white h-4 w-4') }}
                @unless ($minimised)
                    <span class="ml-3">Logout</span>
                @endif
            </button>
        </form>
    </div>
</div>
