@props([])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6" />

    <form method="POST" action="{{ route('account-register-your-watch') }}">
        @csrf

        <p>Please enter the details of your watch using the form below.</p>

        <div class="md:flex md:-mx-4">
            <div class="flex-1 md:px-4">
                <x-inputs.input-wrapper>
                    <x-inputs.select id="watch_brand" name="watch_brand" required>
                        @if (site_name() === 'swissmade')
                            <option value="" selected>Watch Brand *</option>
                            <option value="baume_mercier">BAUME & MERCIES</option>
                            <option value="ebel">EBEL</option>
                            <option value="rolex">ROLEX</option>
                            <option value="longines">LONGINES</option>

                        @endif
                    </x-inputs.select>
                </x-inputs.input-wrapper>
                <x-inputs.input-wrapper>
                    <x-inputs.select id="WristbandType" name="WristbandType">
                        <option value="" selected="selected">Wristband Type</option>
                        <option value="Strap">Strap</option>
                        <option value="Bracelet">Bracelet</option>
                        <option value="Bangle">Bangle</option>
                        <option value="Other">Other</option>
                    </x-inputs.select>
                </x-inputs.input-wrapper>
            </div>
            <div class="flex-1 md:px-4">
                <x-inputs.input-wrapper>
                    <x-inputs.select id="DialColour" name="DialColour">
                        <option value="" selected="selected">Dial Colour</option>
                        <option value="Black">Black</option>
                        <option value="Blue">Blue</option>
                        <option value="Brown">Brown</option>
                        <option value="Burgundy">Burgundy</option>
                        <option value="Champagne/Gold">Champagne/Gold</option>
                        <option value="Copper">Copper</option>
                        <option value="Cream">Cream</option>
                        <option value="Green">Green</option>
                        <option value="Grey">Grey</option>
                        <option value="Mother of Pearl">Mother of Pearl</option>
                        <option value="Orange">Orange</option>
                        <option value="Pink">Pink</option>
                        <option value="Purple">Purple</option>
                        <option value="Red">Red</option>
                        <option value="Silver">Silver</option>
                        <option value="White">White</option>
                        <option value="Other">Other</option>
                    </x-inputs.select>
                </x-inputs.input-wrapper>
                <x-inputs.input-wrapper>
                    <x-inputs.input id="ModelRef" maxlength="50" name="ModelRef" placeholder="Reference (SKU or Model)" type="text" value="" />
                </x-inputs.input-wrapper>
            </div>
        </div>

        <div class="my-5">
            <h4 class="uppercase font-bold">WARRANTY INFORMATION</h4>
            <div class="md:flex md:-mx-4">
                <div class="md:w-1/2 md:px-4">
                    <x-inputs.input-wrapper>
                        <x-inputs.select id="WarrantyYesNo" name="WarrantyYesNo">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </x-inputs.select>
                    </x-inputs.input-wrapper>
                    <x-inputs.input-wrapper>
                        <x-inputs.date id="DateOfPurchase" name="DateOfPurchase" label="Date of Purchase"/>
                    </x-inputs.input-wrapper>
                </div>
            </div>
        </div>

        <div class="my-5">
            <h4 class="uppercase font-bold">place of Purchase</h4>
            <div class="md:flex md:-mx-4">
                <div class="flex-1 md:px-4">
                    <x-inputs.input-wrapper>
                        <x-inputs.input label="Name of Retailer" id="RetailerName" maxlength="50" name="RetailerName" placeholder="Name of Retailer" type="text" value="" />
                    </x-inputs.input-wrapper>
                </div>
                <div class="flex-1 md:px-4">
                    <x-inputs.input-wrapper>
                        <x-inputs.input label="Retailer Location"  id="RetailerLocation" maxlength="50" name="RetailerLocation" placeholder="i.e. London" type="text" value="" />
                    </x-inputs.input-wrapper>
                </div>
            </div>
        </div>

        <div class="my-5">
            <div class="md:flex">
                <x-inputs.button class="uppercase mr-3 btn-primary mb-3">register new watch</x-inputs.button>
                <a href="{{route('account-my-watches')}}" class="mb-3 btn btn-black uppercase inline-block">cancel</a>
            </div>
        </div>

    </form>
</div>
