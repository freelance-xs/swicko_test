@props([])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6" />

    <form method="POST" action="{{ route('account-contact-preferences') }}" onsubmit="return e.preventDefault()">
        @csrf
        <div class="py-5">
            <label for="opt_in_marketing" class="flex items-center">
                <x-inputs.checkbox id="opt_in_marketing" name="opt_in_marketing" class="mr-3" />
                <span>I would like to be the first to hear about our big savings and offers on new watches, Watch Care and straps.</span>
            </label>
        </div>
        <div class="mt-5">
            <x-inputs.button class="btn-primary" type="submit" id="submit_password">submit</x-inputs.button>
        </div>
    </form>
</div>
