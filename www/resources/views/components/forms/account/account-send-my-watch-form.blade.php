@props([])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6" />

    <form method="POST" action="{{ route('account-send-my-watch') }}">
        @csrf

        <div class="bg-grey-100 p-5">
            <div class="">
                <h4 class="uppercase font-bold mb-3">TELL US ABOUT YOUR WATCH</h4>
                <p>
                    Please complete the details below to request a service or repair request for your watch. When we receive your request we will send you our FREE postage label &amp; instructions to send your watch to us safely by a Secure Royal Mail Return Service.
                </p>
                <p>
                    <b class="text-red">Please note: Free packaging is only available to UK customers only.</b>
                </p>
                <p class="">
                    Please complete all fields marked with an *
                </p>
            </div>

            <div class="md:flex md:-mx-4 mb-10">
                <div class="flex-1 md:px-4">
                    <x-inputs.input-wrapper>
                        <x-inputs.select id="Brand" name="Brand">
                            <option value="" selected="selected">Watch Brand*</option>
                            <option value="Baume &amp; Mercier">BAUME &amp; MERCIER</option>
                            <option value="ebel">EBEL</option>
                            <option value="rolex">ROLEX</option>
                            <option value="omega">OMEGA</option>
                            <option value="longines">LONGINES</option>
                        </x-inputs.select>
                    </x-inputs.input-wrapper>
                    <x-inputs.input-wrapper>
                        <x-inputs.select id="DialColour" name="DialColour">
                            <option value="" selected="selected">Dial Colour</option>
                            <option value="Black">Black</option>
                            <option value="Blue">Blue</option>
                            <option value="Brown">Brown</option>
                            <option value="Burgundy">Burgundy</option>
                            <option value="Champagne/Gold">Champagne/Gold</option>
                            <option value="Copper">Copper</option>
                            <option value="Cream">Cream</option>
                            <option value="Green">Green</option>
                            <option value="Grey">Grey</option>
                            <option value="Mother of Pearl">Mother of Pearl</option>
                            <option value="Orange">Orange</option>
                            <option value="Pink">Pink</option>
                            <option value="Purple">Purple</option>
                            <option value="Red">Red</option>
                            <option value="Silver">Silver</option>
                            <option value="White">White</option>
                            <option value="Other">Other</option>
                        </x-inputs.select>
                    </x-inputs.input-wrapper>
                </div>
                <div class="flex-1 md:px-4">
                    <x-inputs.input-wrapper>
                        <x-inputs.select id="WristbandType" name="WristbandType">
                            <option value="" selected="selected">Wristband Type</option>
                            <option value="Strap">Strap</option>
                            <option value="Bracelet">Bracelet</option>
                            <option value="Bangle">Bangle</option>
                            <option value="Other">Other</option>
                        </x-inputs.select>
                    </x-inputs.input-wrapper>
                    <x-inputs.input-wrapper>
                        <x-inputs.input id="ModelRef" maxlength="50" name="ModelRef" placeholder="Reference (SKU or Model)" type="text" value="" />
                    </x-inputs.input-wrapper>
                </div>
            </div>

            <div class="mb-10">
                <h4 class="uppercase font-bold mb-3">
                    TELL US ABOUT YOUR REPAIR
                </h4>
                <x-inputs.textarea cols="20" id="SpecialInstructions" name="SpecialInstructions" placeholder="Please describe any problems and issues in as much detail as possible *" rows="2" style="height:100px;"></x-inputs.textarea>
            </div>

            <div class="mb-10">
                <h4 class="uppercase font-bold">
                    warranty information
                </h4>

                <div class="md:flex md:-mx-4">
                    <div class="flex-1 md:px-4">
                        <x-inputs.select id="WarrantyYesNo" name="WarrantyYesNo">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </x-inputs.select>
                    </div>
                    <div class="flex-1 md:px-4"></div>
                </div>


                <div class="bg-white p-5">
                    <div class="md:flex md:-mx-4 items-end">
                        <div class="w-full md:w-1/3 md:px-4">
                            <x-inputs.date label="Date Of Purchase" id="DateOfPurchase" maxlength="10" name="DateOfPurchase" placeholder="dd/mm/yyyy" type="text" value="" />
                        </div>
                        <div class="w-full md:w-1/3 md:px-4">
                            <x-inputs.input id="WarrantyRetailer" maxlength="50" name="WarrantyRetailer" placeholder="Retailer" type="text" value="" />
                        </div>
                        <div class="w-full md:w-1/3 md:px-4">
                            <x-inputs.input id="WarrantyLocation" maxlength="50" name="WarrantyLocation" placeholder="Location" type="text" value="" />
                        </div>
                    </div>
                    <p class="my-5 italic">If your watch is covered by a valid warranty and you have adhered to the manufacturing terms we will repair manufacturing defect free of charge. The watch must be accompanied with a proof of purchase or the original warranty card.</p>
                </div>
                <div class="bg-grey-500 p-5">
                    <x-inputs.checkbox-warranty class="text-white text-lg" />
                </div>

                <div class="mt-5">
                    <h4 class="font-bold mb-5 uppercase">
                        SWISSMADE WATCHCARE
                    </h4>
                    <p>
                        Get the best possible performance and wear from your watch! Sign up now to get free service reminders and exclusive watch care offers and discounts.
                    </p>

                    <div class="flex items-center">
                        <x-inputs.input-wrapper class="mr-4">
                            <x-inputs.radio label="Yes please" id="yes" name="yes/no" value="yes" />
                        </x-inputs.input-wrapper>
                        <x-inputs.input-wrapper>
                            <x-inputs.radio label="No Thank You" id="No" name="yes/no" value="No" />
                        </x-inputs.input-wrapper>
                    </div>
                </div>

                <div class="mt-5">
                    <x-inputs.button class="btn-primary uppercase mr-3">send request</x-inputs.button>
                    <a href="{{route('account-my-repairs')}}" class="btn btn-black uppercase">cancel</a>
                </div>

            </div>
        </div>

    </form>
</div>
