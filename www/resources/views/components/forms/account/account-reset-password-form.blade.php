@props([])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6" />

    <form method="POST" action="{{ route('account-reset-password') }}">
        @csrf

        <h4 class="font-bold uppercase my-5">CURRENT PASSWORD</h4>
        
        <div class="md:flex md:-mx-10">
            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.input type="text" id="current_password" name="current_password" placeholder="Current Password" value="{{ old('current_password') }}" required />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="new_password" name="new_password" placeholder="New Password" value="{{ old('new_password') }}" required />
                </div>
            </div>
            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.input type="text" id="confirm_new_password" name="confirm_new_password" placeholder="Confirm New Password" value="{{ old('confirm_new_password') }}" required />
                </div>
            </div>
        </div>

        <div class="mt-5">
            <x-inputs.button class="btn-primary" type="submit" id="submit_password">submit</x-inputs.button>
        </div>
    </form>
</div>
