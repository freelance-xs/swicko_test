@props([])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6" />

    <form method="POST" action="{{ route('account-address') }}">
        @csrf

        <div class="bg-dark-grey p-5 my-5">
            <h4 class="uppercase text-white">billing address</h4>
        </div>

        <div class="md:flex md:-mx-10">
            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.input type="text" id="billing_company_name" name="billing_company_name" placeholder="Company Name" value="{{ old('billing_company_name') }}" required />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="billing_address_one" name="billing_address_one" placeholder="Address 1" value="{{ old('billing_address_one') }}" required />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="billing_address_two" name="billing_address_two" placeholder="Address 2" value="{{ old('billing_address_two') }}" />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="billing_city" name="billing_city" placeholder="City" value="{{ old('billing_city') }}" required />
                </div>
            </div>
            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.input type="text" id="billing_county" name="billing_county" placeholder="County" value="{{ old('billing_county') }}" required />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="billing_country" name="billing_country" placeholder="Country" value="{{ old('billing_country') }}" required />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="billing_postcode" name="billing_postcode" placeholder="Postcode" value="{{ old('billing_postcode') }}" />
                </div>
            </div>
        </div>


        <div class="bg-dark-grey p-5 my-5">
            <h4 class="uppercase text-white">delivery address</h4>
        </div>

        <div class="md:flex md:-mx-10">
            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.input type="text" id="delivery_company_name" name="delivery_company_name" placeholder="Company Name" value="{{ old('delivery_company_name') }}" required />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="delivery_address_one" name="delivery_address_one" placeholder="Address 1" value="{{ old('delivery_address_one') }}" required />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="delivery_address_two" name="delivery_address_two" placeholder="Address 2" value="{{ old('delivery_address_two') }}" />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="delivery_city" name="delivery_city" placeholder="City" value="{{ old('delivery_city') }}" required />
                </div>
            </div>
            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.input type="text" id="delivery_county" name="delivery_county" placeholder="County" value="{{ old('delivery_county') }}" required />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="delivery_country" name="delivery_country" placeholder="Country" value="{{ old('delivery_country') }}" required />
                </div>
                <div class="mb-5">
                    <x-inputs.input type="text" id="delivery_postcode" name="delivery_postcode" placeholder="Postcode" value="{{ old('delivery_postcode') }}" />
                </div>
            </div>
        </div>

        <div class="mt-5">
            <x-inputs.button class="btn-primary" type="submit" id="submit_address">update info</x-inputs.button>
        </div>

    </form>
</div>
