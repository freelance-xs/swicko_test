@props([])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6" />

    <form method="POST" action="{{ route('account-details') }}">
        @csrf

        <div class="md:flex md:-mx-10">
            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.select name="title" id="title" value="{{ old('title') }}" required>
                        <option value="na" selected>Not specified</option>
                        <option value="mr">Mr</option>
                        <option value="mrs">Mrs</option>
                        <option value="ms">Ms</option>
                        <option value="miss">Miss</option>
                        <option value="dr">Dr</option>
                    </x-inputs.select>
                </div>

                <div class="mb-5">
                    <x-inputs.input type="text" id="first_name" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" required />
                </div>

                <div class="mb-5">
                    <x-inputs.input type="text" id="last_name" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" required />
                </div>
            </div>

            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.input type="text" id="telephone" name="telephone" placeholder="Telephone" value="{{ old('telephone') }}" required />
                </div>

                <div class="mb-5">
                    <x-inputs.input class="bg-grey-100" type="email" id="email" name="email" placeholder="Email Address" value="{{ auth()->user()->email }}" required disabled />
                </div>
            </div>
        </div>

        <h3 class="uppercase font-bold my-10">change email address</h3>


        <div class="md:flex md:-mx-10">
            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.input type="email" id="new_email" name="new_email" placeholder="New Email Address" value="{{ old('new_email') }}" />
                </div>

               
            </div>
            <div class="md:w-1/2 md:px-10">
                <div class="mb-5">
                    <x-inputs.input type="email" id="new_email_confirm" name="new_email_confirm" placeholder="Confirm New Email Address" value="{{ old('new_email_confirm') }}" />
                </div>
            </div>

        </div>
        
        <div class="mt-5">
            <x-inputs.button class="btn-primary" type="submit" id="submit">submit</x-inputs.button>
        </div>

    </form>
</div>
