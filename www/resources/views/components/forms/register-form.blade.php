@props(['warranty' => false])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6" />
    <form method="POST" action="{{ route('register') }}">
        @csrf

        <div class="mb-10">
            <div class="mb-5">
                <div class="md:flex md:items-center md:justify-between">
                    <div class="flex-1">
                        <x-inputs.select id="title" name="title">
                            <option value="not_specified">Not specified</option>
                            <option value="mr">Mr</option>
                            <option value="mrs">Mrs</option>
                            <option value="ms">Ms</option>
                            <option value="ms">Miss</option>
                            <option value="dr">Dr</option>
                        </x-inputs.select>
                    </div>

                    <div class="flex-1 text-sm text-right">
                        <label class="control-label">Please complete all fields marked with an *</label>
                    </div>
                </div>
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="First Name *" type="text" id="first_name" name="first_name" value="{{ old('first_name') }}" />
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="Last Name *" type="text" id="last_name" name="last_name" value="{{ old('last_name') }}" />
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="Primary Contact Number *" type="tel" id="primary_contact_number" name="primary_contact_number" />
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="Email Address *" type="email" id="warranty_email" name="warranty_email" />
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="Password *" type="password" id="warranty_password" name="warranty_password" />
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="Confirm Password *" type="password" id="password_confirm" name="password_confirm" />
            </div>
        </div>

        <div class="mb-10">
            <h3 class="uppercase font-bold mb-5">adddress details</h3>

            <div class="mb-5">
                <x-inputs.input placeholder="Company Name *" type="text" id="company_name" name="company_name" />
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="Address One *" type="text" id="address_one" name="address_one" />
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="Address Two *" type="text" id="address_two" name="address_two" />
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="City *" type="text" id="county" name="county" />
            </div>
            <div class="mb-5">
                <x-inputs.input placeholder="Postcode *" type="text" id="postcode" name="postcode" />
            </div>
            <div class="mb-5">
                <x-inputs.country-list />
            </div>

            @if ($warranty && (Route::has('warranty-information') ||Route::has('warranty-claim')  ))
                <div class="bg-dark-grey text-white p-5 mb-5">
                    <x-inputs.checkbox-warranty />
                </div>
            @endif

            <div class="mb-5">
                <x-inputs.checkbox-terms />
            </div>
            <div class="mb-5">
                <label for="offers_acceptance" class="text-lg">
                    <x-inputs.checkbox id="offers_acceptance" type="textarea" name="offers_acceptance" placeholder="Your enquiry / message to us *" rows="8" value="{{ old('offers_acceptance') }}" required />
                    <span class="ml-2 text-sm">I would like to be the first to hear about our big savings and offers on new watches, Watch Care and straps.</span>
                </label>
            </div>
            <div>
                <x-inputs.recaptcha />
            </div>
        </div>

        <div>
            <x-inputs.button class="btn-primary">create you account</x-inputs.button>
        </div>
    </form>
</div>
