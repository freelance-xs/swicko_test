@props([])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6" />

    <form method="POST" action="{{ route('contact') }}">
        @csrf

        <div class="mb-5">
            <p class="text-black text-right text-sm">Please complete all fields</p>
        </div>

        <div class="mb-5">
            <x-inputs.input id="name" type="text" name="name" placeholder="Your full name *" value="{{ old('name') }}"
                required />
        </div>

        <div class="mb-5">
            <x-inputs.input id="email" type="email" name="email" placeholder="Your email address *"
                value="{{ old('email') }}" required />
        </div>

        <div class="mb-5">
            <x-inputs.input id="phone" type="tel" phone="name" placeholder="Your phone number *"
                value="{{ old('number') }}" required />
        </div>

        <div class="mb-5">
            <x-inputs.input id="reference" type="text" name="reference" placeholder="Your enquiry reference *"
                value="{{ old('reference') }}" required />
        </div>

        <div class="mb-5">
            <x-inputs.textarea id="enqiury" type="textarea" name="enqiury" placeholder="Your enquiry / message to us *"
                rows="8" value="{{ old('enqiury') }}" required />
        </div>

        <x-inputs.checkbox-terms class="my-10"></x-inputs.accept-terms>

        <x-inputs.recaptcha />

        <div class="mt-6">
            <x-inputs.button type="submit" class="btn-primary">
                <span class="uppercase">{{ $buttonText }}</span>
            </x-inputs.button>
        </div>

    </form>
</div>
