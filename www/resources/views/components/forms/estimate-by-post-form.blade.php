@props([])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6" />

    <form method="POST" action="{{ route('estimate-by-post') }}">
        @csrf

        <div class="mb-5">
            <p class="text-black text-right text-sm">Please complete all fields</p>
        </div>

        <div class="mb-5">
            <x-inputs.input required type="text" name="last_name" id="last_name" placeholder="Last Name" value="{{ old('last_name') }}" />
        </div>

        <div class="mb-5">
            <x-inputs.input required type="text" name="code" id="code" placeholder="SSC-XXXXX-XXXXX" value="{{ old('code') }}" />
        </div>

        <div class="mb-5">
            <x-inputs.input required type="email" name="email" id="email" placeholder="Email Address" value="{{ old('email') }}" />
        </div>

        <div class="mb-5">
            <x-inputs.input required type="password" name="password" id="password" placeholder="Password" value="{{ old('password') }}" />
        </div>

        <div class="mb-5">
            <x-inputs.input required type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" value="{{ old('confirm_password') }}" />
        </div>

        <div class="mb-5">
            <x-inputs.checkbox-terms />
        </div>

        <div class="mb-5">
            <label for="offers_acceptance" class="text-lg">
                <x-inputs.checkbox id="offers_acceptance" type="textarea" name="offers_acceptance" placeholder="Your enquiry / message to us *" rows="8" value="{{ old('offers_acceptance') }}" required />
                <span class="ml-2 text-sm">I would like to be the first to hear about our big savings and offers on new watches, Watch Care and straps.</span>
            </label>
        </div>

        <x-inputs.button class="w-full block btn-primary btn">
            {{ $buttonText ?? 'Activate' }}
        </x-inputs.button>


    </form>
</div>
