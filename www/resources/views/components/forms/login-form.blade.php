@props(['buttonText' => 'Login', 'mode' => null])

<div {{ $attributes }}>
    {{ $slot }}

    <x-validation-errors class="mb-6"/>

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div>
            <x-inputs.input mode="{{$mode}}" id="email" type="email" name="email" placeholder="Email address" value="{{ old('email') }}"
                     required/>
        </div>

        <div class="mt-4">
            <x-inputs.input mode="{{$mode}}" id="password" type="password" name="password" placeholder="Password" required
                     autocomplete="current-password"/>
        </div>

        <div class="flex flex-wrap items-center justify-between mt-6">
            <x-inputs.button class="w-full md:w-auto mr-3 mb-3">
                {{ $buttonText }}
            </x-inputs.button>

            @if (Route::has('password.request'))
                <a class="text-sm mt-4 md:mt-0 {{$mode == 'light' ? 'text-white' : ''}}" href="{{ route('password.request') }}">Forgotten password?</a>
            @endif
        </div>
    </form>
</div>
