@props(['last' => false])

<div
    {!! $attributes->merge(['class' => 'relative p-3']) !!}
    @click.away="dropdownOpen = false"
    x-data="{ dropdownOpen: false }"
>
    <button
        type="button"
        @click="dropdownOpen = !dropdownOpen"
        x-state:on="Item active"
        x-state:off="Item inactive"
        class="text-black font-semibold hover:text-dark-grey focus:outline-none tracking-widest"
    >
        {{ $button }}
    </button>
    <div
        x-cloak
        x-show="dropdownOpen"
        x-transition:enter="transition ease-out duration-200"
        x-transition:enter-start="opacity-0 translate-y-1"
        x-transition:enter-end="opacity-100 translate-y-0"
        x-transition:leave="transition ease-in duration-150"
        x-transition:leave-start="opacity-100 translate-y-0"
        x-transition:leave-end="opacity-0 translate-y-1"
        class="absolute z-20 transform mt-7 px-2 w-screen max-w-xs sm:px-0 {{ $last ? '-translate-x-last' : '' }}"
    >
        <div class="relative bg-primary">
            {{ $slot }}
        </div>
    </div>
</div>
