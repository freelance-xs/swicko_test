@props([
    'bgColor' => 'dark-grey',
    'quoteColor' => 'blue',
])

<div {{ $attributes->merge(['class' => "bg-${bgColor} px-8 pt-12 pb-8 font-bold"]) }}>
    <div class="mb-12">
        <img src="{{ asset_url('icon/quote_' . $quoteColor . '.png') }}" alt="...">
    </div>
    <div class="prose {{ $bgColor === 'dark-grey' ? 'text-white' : 'text-black' }}">
        {{ $slot }}
    </div>
</div>
