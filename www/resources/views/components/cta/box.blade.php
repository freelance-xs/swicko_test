@props([
    'background' => '',
    'bgColor' => 'bg-primary',
])

<div {!! $attributes->merge(['class' => 'w-full px-5']) !!}>
    @if ($background)
        <div>
            <img class="object-cover w-full" src="{{ $background }}">
        </div>
    @endif
    <div class="p-8 {{ $bgColor }}">
        <div class="flex flex-col justify-between">
            <div class="flex justify-end items-center mb-8">
                @if (isset($stats))
                    {{ $stats }}
                @endif
            </div>
            <div class="mb-5">
                @if (isset($icon))
                    {{ $icon }}
                @endif
            </div>

            <div class="mb-5">
                {{ $slot }}
            </div>

            <div>
                {{ $button ?? '' }}
            </div>
        </div>
    </div>
</div>
