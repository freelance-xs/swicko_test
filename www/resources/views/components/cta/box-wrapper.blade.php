<div {{ $attributes->merge(['class' => 'flex flex-wrap -mx-5']) }}>
    {{ $slot }}
</div>
