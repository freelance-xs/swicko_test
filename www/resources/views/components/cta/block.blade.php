@props([
    'bgColor',
    'image',
])

<div {{ $attributes->merge(['class' => 'py-24 '.$bgColor]) }}>
    <div class="container mx-auto px-6">
        <div class="flex flex-wrap">
            <div class="w-full md:w-1/2 bg-white py-8 px-12">
                {{ $slot }}
            </div>
            <div class="w-full md:w-1/2">
                <img src="{{ asset_url($image) }}" class="w-full h-full object-cover" alt="...">
            </div>
        </div>
    </div>
</div>
