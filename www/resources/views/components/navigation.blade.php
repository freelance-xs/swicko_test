<nav class="navbar bg-white bg-opacity-95 shadow-xl w-full fixed z-20">

    @unless(Auth::check())
        <div class="navbar bg-primary py-3">
            <div class="container mx-auto px-4">
                <div class="w-full text-white">
                    <a class="flex items-center text-white text-sm hover:text-white hover:underline" href="{{ route('estimate-by-post') }}">
                        <div>
                            <img class="object-fill w-auto" src="{{ asset_url('icon/email.png') }}" alt="...">
                        </div>
                        @if (site_name() === 'swico')
                            <span class="pl-2"><span class="hidden md:inline">HAVE YOU RECEIVED AN </span>ESTIMATE BY POST? <span class="text-black font-bold md:text-white md:font-normal">CLICK HERE</span></span>

                        @elseif( site_name() === 'swissmade' )
                            <span class="pl-2"><span class="hidden md:inline">HAVE YOU RECEIVED AN ONLINE ACTIVATION CODE BY POST? <span class="text-black font-bold md:text-white md:font-normal">CLICK HERE</span></span>

                        @endif
                    </a>
                </div>
            </div>
        </div>
        @endif

        <div class="container mx-auto px-4 hidden relative md:block">
            <div class="flex">
                <div class="py-4 hidden md:block">
                    <div class="py-2">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset_url('/logo/logo.png') }}" alt="...">
                        </a>
                    </div>
                </div>

                <div class="w-full flex items-center justify-end">
                    <ul class="flex items-center text-xs font-semibold">
                        <x-dropdown class="border-b-2 {{ Request::is('watch-brand*') ? 'border-primary' : 'border-transparent' }}">
                            <x-slot name="button">
                                <span class="hidden lg:inline-block pr-1">Watch </span>Brands
                            </x-slot>
                            <img class="hidden absolute md:block" src="{{ asset_url('/icon/navbar/triangle.png') }}" style="top: -13px; left: 20px">
                            <div class="grid py-4">
                                <livewire:watch.watch-brand-drop-down/>
                            </div>
                        </x-dropdown>

                        @if (Auth::check())
                            <li class="p-3 border-b-2 {{ Request::is('') ? 'border-primary' : 'border-transparent' }}">
                                <a class="text-black hover:text-dark-grey" href="">
                                    <span class="hidden lg:inline-block pr-1">Send Your </span>Watch
                                </a>
                            </li>
                        @else
                            <x-dropdown class="border-b-2 {{ Request::is('warranty-claim', 'service-repair') ? 'border-primary' : 'border-transparent' }}">
                                <x-slot name="button">
                                    <span class="hidden lg:inline-block pr-1">Send Your </span>Watch
                                </x-slot>
                                <img class="hidden absolute md:block" src="{{ asset_url('/icon/navbar/triangle.png') }}" style="top: -13px; left: 20px">
                                <div class="grid py-4">
                                    <x-navigation.dropdown-item :route="route('warranty-claim')">Start Warranty Claim
                                    </x-navigation.dropdown-item>
                                    <x-navigation.dropdown-item :route="route('service-repair')">Start Service & Repair
                                    </x-navigation.dropdown-item>
                                    <x-navigation.dropdown-item :route="route('repairs-by-post')">How it works
                                    </x-navigation.dropdown-item>
                                </div>
                            </x-dropdown>
                        @endif

                        <li class="p-3 border-b-2 {{ Request::is('watch-care') ? 'border-primary' : 'border-transparent' }}">
                            <a class="text-black hover:text-dark-grey" href="{{ route('watch-care') }}">
                                <span class="lg:hidden">Care</span>
                                <span class="hidden lg:block xl:hidden">Watchcare</span>
                                <span class="hidden xl:block">{{ strtoupper(site_name()) }} WatchCare</span>
                            </a>
                        </li>

                        <x-dropdown class="border-b-2 {{ Request::is('about', 'repairs-by-post', 'services-explained', 'warranty-information', 'maintenance-care', 'testimonials', 'estimate-by-post', 'faqs') ? 'border-primary' : 'border-transparent' }}">
                            <x-slot name="button">
                                <span class="xl:hidden pr-1">Info</span>
                                <span class="hidden xl:block pr-1">Customer Information</span>
                            </x-slot>
                            <img class="hidden absolute md:block" src="{{ asset_url('/icon/navbar/triangle.png') }}" style="top: -13px; left: 20px">
                            <div class="grid py-4">
                                <x-navigation.dropdown-item :route="route('about')">
                                    About {{ strtoupper(site_name()) }}</x-navigation.dropdown-item>
                                <x-navigation.dropdown-item :route="route('repairs-by-post')">
                                    {{ site_name() === 'swico' ? 'Repairs By Post' : 'Online Watch Repairs' }}
                                </x-navigation.dropdown-item>
                                <x-navigation.dropdown-item :route="route('services-explained')">Service Explained
                                </x-navigation.dropdown-item>
                                <x-navigation.dropdown-item :route="route('warranty-information')">Warranty Information
                                </x-navigation.dropdown-item>
                                <x-navigation.dropdown-item :route="route('maintenance-care')">Maintenance & Care
                                </x-navigation.dropdown-item>
                                <x-navigation.dropdown-item :route="route('testimonials')">Testimonials
                                </x-navigation.dropdown-item>
                                <x-navigation.dropdown-item :route="route('estimate-by-post')">
                                    {{ site_name() === 'swico' ? 'Received Estimate By Post?' : 'Received Activation Code By Post?' }}
                                </x-navigation.dropdown-item>
                                <x-navigation.dropdown-item :route="route('faqs')">FAQs</x-navigation.dropdown-item>
                            </div>
                        </x-dropdown>

                        <li class="p-3 border-b-2 {{ Request::is('contact') ? 'border-primary' : 'border-transparent' }}">
                            <a class="text-black hover:text-dark-grey p-3" href="{{ route('contact') }}">Contact</a>
                        </li>

                        <li class="p-3">
                            <button class="btn btn-black py-4 px-5 text-xs font-semibold" @click="open('price-calculator')" style="margin-right: 10px">Quick Quote
                            </button>
                        </li>

                        @if (Auth::check())
                            <li class="p-3">
                                <a class="text-black" href="{{ route('cart') }}">
                                    <div class="flex">
                                        <img class="inline" src="{{ asset_url('icon/bag.png') }}" alt="bag">
                                        <span class="relative flex justify-center items-center h-5 w-5 rounded-full bg-primary font-bold" style="top: -5px;">0</span>
                                    </div>
                                </a>
                            </li>
                        @endif

                        <x-dropdown :last="true">
                            <x-slot name="button">
                                <div class="flex flex-col items-center">
                                    @if (Auth::check())
                                        <div>
                                            <img src="{{ asset_url('/icon/loggedin.png') }}" alt="...">
                                        </div>
                                    @else
                                        <div>
                                            <img src="{{ asset_url('/icon/login.png') }}" alt="...">
                                        </div>
                                        <span class="text-dark-grey text-xs"><b>LOGIN</b></span>
                                    @endif
                                </div>
                            </x-slot>
                            <img class="hidden absolute md:block" src="{{ asset_url('/icon/navbar/triangle.png') }}" style="top: -13px; right: 20px">
                            @if (Auth::check())
                                <div class="grid gap-4 px-4 py-4 text-sm font-normal">
                                    <p class="text-white text-base font-normal">{{ Auth::user()->fullName }}</p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block object-contain" src="{{ asset_url('icon/navbar/screen@2x.png') }}" alt="...">
                                        <a class="text-black py-2 hover:underline" href="{{ route('account-overview') }}">Account
                                            Overview</a>
                                    </p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block object-contain" src="{{ asset_url('icon/navbar/option_bar_settings@2x.png') }}" alt="...">
                                        Account Settings
                                    </p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block" src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                        <a class="text-white py-2 hover:underline" href="{{ route('account-details') }}">My Details</a>
                                    </p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block" src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                        <a class="text-white py-2 hover:underline" href="{{ route('account-address') }}">My
                                            Addresses</a>
                                    </p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block" src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                        <a class="text-white py-2 hover:underline" href="{{ route('account-reset-password') }}">Change
                                            Password</a>
                                    </p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block" src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                        <a class="text-white py-2 hover:underline" href="{{ route('account-contact-preferences') }}">Contact
                                            Preferences</a>
                                    </p>
                                </div>
                                <div class="bg-grey-500 grid gap-4 px-4 py-4 text-sm font-normal">
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block object-contain" src="{{ asset_url('icon/navbar/watch@2x.png') }}" alt="...">
                                        Registered Watches
                                    </p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block" src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                        <a class="text-white py-2 hover:underline" href="{{ route('account-my-watches') }}">My Watches</a>
                                    </p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block" src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                        <a class="text-white py-2 hover:underline" href="{{ route('account-register-your-watch') }}">Register your
                                            watch</a>
                                    </p>
                                </div>
                                <div class="bg-dark-grey grid gap-4 px-4 py-4 text-sm font-normal">
                                    <p class="text-white">
                                        <img class="h-5 w-5 mr-3 inline-block object-contain" src="{{ asset_url('icon/navbar/settings@2x.png') }}" alt="...">
                                        My Services & Repairs
                                    </p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block" src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                        <a class="text-grey-200 py-2 hover:underline" href="{{ route('account-my-repairs') }}">View my
                                            repairs</a>
                                    </p>
                                    <p>
                                        <img class="h-5 w-5 mr-3 inline-block" src="{{ asset_url('icon/navbar/blank.png') }}" alt="...">
                                        <a class="text-grey-200 py-2 hover:underline" href="{{ route('account-send-my-watch') }}">Send my
                                            watch</a>
                                    </p>
                                </div>
                                <div class="bg-white grid gap-4 px-4 py-4 text-sm font-normal">
                                    <form class="inline-block" method="post" action="{{ route('logout') }}">
                                        @csrf
                                        <button class="text-black py-2 hover:underline focus:outline-none">
                                            <img class="h-5 w-5 mr-3 inline-block object-contain" src="{{ asset_url('icon/navbar/padlock@2x.png') }}" alt="...">Logout
                                        </button>
                                    </form>
                                </div>
                            @else
                                <div class="grid gap-4 px-4 py-4">
                                    <p class="text-center uppercase text-lg font-bold text-black">Welcome
                                        to {{ strtoupper(site_name()) }}</p>
                                    <p class="text-center text-white text-base">Create an account to register, service or
                                        repair your watch.</p>
                                    <a href="{{ route('login') }}" class="btn btn-black w-full text-center text-sm font-normal">LOGIN TO {{ strtoupper(site_name()) }}</a>
                                    <a href="{{ route('watch-care') }}" class="btn btn-primary w-full text-center underline text-sm font-normal">CREATE
                                        MY ACCOUNT</a>
                                </div>
                            @endif
                        </x-dropdown>
                    </ul>
                </div>
            </div>
        </div>


        <div id="container-navbar-mobile" class="bg-white md:hidden">
            <div class="container mx-auto px-4">
                <div class="flex py-2">
                    <div class="w-1/3 flex items-center">
                        @if (Auth::check())
                            <div class="flex">
                                <div class="pr-4" @click="open('navbar-mobile-logged-in')">
                                    <img src="{{ asset_url('icon/loggedin.png') }}" alt="...">
                                </div>
                                <div>
                                    <a class="text-black" href="">
                                        <div class="flex">
                                            <img class="inline" src="{{ asset_url('icon/bag.png') }}" alt="bag">
                                            <span class="relative flex justify-center items-center h-5 w-5 rounded-full bg-primary font-bold" style="top: -5px;">0</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @else
                            <div class="flex flex-col items-center" @click="open('navbar-mobile-logged-out')">
                                <div>
                                    <img src="{{ asset_url('icon/login.png') }}" alt="...">
                                </div>
                                <span class="text-dark-grey text-xs"><b>LOGIN</b></span>
                            </div>
                        @endif
                    </div>
                    <div class="w-1/3 flex justify-center items-center">
                        <a href="/">
                            @if (site_name() === 'swico')
                                <img src="{{ asset_url('/logo/SWICO-ICON-RGB_small.png') }}" alt="..." style="height: 45px;">
                            @else
                                <img src="{{ asset_url('logo/swissmade_logo_mobile.png') }}" alt="...">
                            @endif
                        </a>
                    </div>
                    <div class="w-1/3 flex justify-end items-center">
                        <button @click="open('navbar-mobile')" style="color: white">
                            <img class="pull-right" src="{{ asset_url('/icon/navbar/nav_mobile.png') }}">
                        </button>
                    </div>
                </div>
            </div>
        </div>

        @push('modals')
            <x-modals.mobile-menu />

            @if (Auth::check())
                <x-modals.mobile-logged-in />
            @else
                <x-modals.mobile-logged-out />
            @endif
        @endpush



        {{-- <div class="modal fade" id="navbar-mobile-modal-account" role="dialog"> --}}
        {{-- <div class="modal-dialog modal-lg" style="width: 100%; height:100%; opacity: 1; padding: 0; margin: 0;"> --}}
        {{-- <!-- Modal content--> --}}
        {{-- <div class="modal-content background-blue-light" style="border: solid 0px #33C0BA; height: auto; min-height: 100%; border-radius: 0;"> --}}

        {{-- <div class="container"> --}}
        {{-- <div class="container banner-image-logo-overlay" style="background-position: 100% 100%; padding-right: 0; padding-left: 0"> --}}
        {{-- <div class="container container-small"> --}}

        {{-- <div class="modal-header price-calculator-pad-top" style="border-bottom: 0px solid #33C0BA;"> --}}
        {{-- <a href="#" class="close" data-dismiss="modal" style="float: left !important; opacity: 1;"> --}}
        {{-- <img src="/themes/swicoV2/images/icon/navbar/close-blank.png"> --}}
        {{-- </a> --}}

        {{-- <h3 class="modal-title font-white text-center"> --}}
        {{-- <img src="/themes/swicoV2/images/logo/logo_swico_white_small.png" style="position: relative; top: -10px; left: -15px; height: 44px; width: 44px;"> --}}
        {{-- </h3> --}}
        {{-- </div> --}}
        {{-- <div class="modal-body font-white" style="padding: 0; padding-top: 20px !important"> --}}

        {{-- <div class="bottommargin-sm"> --}}
        {{-- <p class="font-white" style="font-size: 18px"> --}}
        {{-- <img class="navbar-account-image" src="/themes/swicoV2/images/icon/login.png" alt="..."><b></b> --}}
        {{-- </p> --}}
        {{-- </div> --}}

        {{-- <hr style="margin-top: 0 !important; border-color: #fff; position: relative; left: -30px; border-style: solid;"> --}}

        {{-- <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true" style="background-color: transparent !important"> --}}

        {{-- <div class="bottommargin-sm"> --}}
        {{-- <p style="font-weight: normal; padding-top: 5px"> --}}
        {{-- <img class="navbar-account-image" src="/themes/swicoV2/images/icon/navbar/screen@2x.png" alt="..."><a class="navbar-mobile-link-header-dark-grey" href="/Account/AccountOverview">Account Overview</a> --}}
        {{-- </p> --}}
        {{-- </div> --}}

        {{-- <div class="panel box-shadow-none background-blue-light bottommargin"> --}}
        {{-- <div class="panel-margin-bottom-0" role="tab" id="heading10a"> --}}
        {{-- <h4 class="panel-title panel-title-faq" style="vertical-align: middle"> --}}
        {{-- <a class="navbar-mobile-link-header-dark-grey" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse10a" aria-expanded="false" aria-controls="collapse10a"> --}}
        {{-- <img class="navbar-account-image" src="/themes/swicoV2/images/icon/navbar/option_bar_settings@2x.png" alt="...">Account Settings&nbsp;&nbsp;<img src="/themes/swicoV2/images/icon/navbar/plus.png" style="position: relative; top: -2px;"> --}}
        {{-- </a> --}}
        {{-- </h4> --}}
        {{-- </div> --}}
        {{-- <div id="collapse10a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10a"> --}}
        {{-- <div class="panel-body" style="padding-bottom: 0; padding-left: 35px"> --}}
        {{-- <p><a class="navbar-mobile-link-sub-white" href="/Account/AccountDetails">My Details</a></p> --}}
        {{-- <p><a class="navbar-mobile-link-sub-white" href="/Account/AccountDetailsAddress">My Addresses</a></p> --}}
        {{-- <p><a class="navbar-mobile-link-sub-white" href="/Account/AccountDetailsPassword">Change Password</a></p> --}}
        {{-- <p><a class="navbar-mobile-link-sub-white" href="/Account/AccountDetailsPreferences">Contact Preferences</a></p> --}}
        {{-- </div> --}}
        {{-- </div> --}}
        {{-- </div> --}}

        {{-- <div class="panel box-shadow-none background-blue-light bottommargin"> --}}
        {{-- <div class="panel-margin-bottom-0" role="tab" id="heading11a"> --}}
        {{-- <h4 class="panel-title panel-title-faq" style="vertical-align: middle"> --}}
        {{-- <a class="navbar-mobile-link-header-dark-grey" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse11a" aria-expanded="false" aria-controls="collapse11a"> --}}
        {{-- <img class="navbar-account-image" src="/themes/swicoV2/images/icon/navbar/watch@2x.png" alt="...">Registered Watches&nbsp;&nbsp;<img src="/themes/swicoV2/images/icon/navbar/plus.png" style="position: relative; top: -2px;"> --}}
        {{-- </a> --}}
        {{-- </h4> --}}
        {{-- </div> --}}
        {{-- <div id="collapse11a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11a"> --}}
        {{-- <div class="panel-body" style="padding-bottom: 0; padding-left: 35px"> --}}
        {{-- <p><a class="navbar-mobile-link-sub-white" href="/Account/AccountWatches">My Watches</a></p> --}}
        {{-- <p><a class="navbar-mobile-link-sub-white" href="/Account/AccountWatchNew">Register your watch</a></p> --}}
        {{-- </div> --}}
        {{-- </div> --}}
        {{-- </div> --}}

        {{-- <div class="panel box-shadow-none background-blue-light bottommargin"> --}}
        {{-- <div class="panel-margin-bottom-0" role="tab" id="heading12a"> --}}
        {{-- <h4 class="panel-title panel-title-faq" style="vertical-align: middle"> --}}
        {{-- <a class="navbar-mobile-link-header-dark-grey" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse12a" aria-expanded="false" aria-controls="collapse12a"> --}}
        {{-- <img class="navbar-account-image" src="/themes/swicoV2/images/icon/navbar/settings@2x_white.png" alt="...">My Services &amp; Repairs&nbsp;&nbsp;<img src="/themes/swicoV2/images/icon/navbar/plus.png" style="position: relative; top: -2px;"> --}}
        {{-- </a> --}}
        {{-- </h4> --}}
        {{-- </div> --}}
        {{-- <div id="collapse12a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12a"> --}}
        {{-- <div class="panel-body" style="padding-bottom: 0; padding-left: 35px"> --}}
        {{-- <p><a class="navbar-mobile-link-sub-white" href="/Account/AccountServiceRequests">View my repairs</a></p> --}}
        {{-- <p><a class="navbar-mobile-link-sub-white" href="/Account/AccountServiceRequestNew">Send my watch</a></p> --}}
        {{-- </div> --}}
        {{-- </div> --}}
        {{-- </div> --}}

        {{-- <div class="bottommargin-sm"> --}}
        {{-- <p style="font-weight: normal;"> --}}
        {{-- <img style="height: 20px; width: 14px; margin-right: 22px;position: relative;left: 3px;top: -3px;" src="/themes/swicoV2/images/icon/navbar/padlock_white@2x.png" alt="..."><a class="navbar-mobile-link-header-dark-grey" href="/Account/SignOut">Log Out</a> --}}
        {{-- </p> --}}
        {{-- </div> --}}


        {{-- </div> --}}

        {{-- </div> --}}
        {{-- </div> --}}

        {{-- </div> --}}
        {{-- </div> --}}
        {{-- </div> --}}

        {{-- </div> --}}
        {{-- </div> --}}




        {{-- <div class="modal fade" id="navbar-mobile-modal-welcome" role="dialog"> --}}
        {{-- <div class="modal-dialog modal-lg" style="width: 100%; height:100%; opacity: 1; padding: 0; margin: 0;"> --}}
        {{-- <!-- Modal content--> --}}
        {{-- <div class="modal-content background-blue-light" style="border: solid 0px #33C0BA; height: auto; min-height: 100%; border-radius: 0; "> --}}

        {{-- <div class="container"> --}}
        {{-- <div class="container banner-image-logo-overlay" style="background-position: 100% 100%; padding-right: 0; padding-left: 0"> --}}
        {{-- <div class="container container-small"> --}}

        {{-- <div class="modal-header price-calculator-pad-top" style="border-bottom: 0px solid #33C0BA;"> --}}

        {{-- <a href="#" class="close" data-dismiss="modal" style="float: left !important; opacity: 1;"> --}}
        {{-- <img src="/themes/swicoV2/images/icon/navbar/close-blank.png"> --}}
        {{-- </a> --}}

        {{-- <h3 class="modal-title font-white text-center"> --}}
        {{-- <img src="/themes/swicoV2/images/logo/logo_swico_white_small.png" style="position: relative; top: -10px; left: -15px; height: 44px; width: 44px;"> --}}
        {{-- </h3> --}}

        {{-- </div> --}}

        {{-- <div class="modal-body font-white" style="padding: 15px;"> --}}
        {{-- <p class="text-uppercase swico-heading-small" style="color: #000; font-size: 24px; margin-top: 50px">Welcome to Swico</p> --}}
        {{-- <p class="" style="color: #fff; font-weight: normal !important;">Create an account to register, service or repair your watch.</p> --}}
        {{-- <p class="topmargin-lg"> --}}
        {{-- <a href="/Account/Signin" class="btn btn-swico-grey-dark btn-block"> --}}
        {{-- LOGIN TO SWICO --}}
        {{-- </a> --}}
        {{-- </p> --}}
        {{-- <p class="topmargin-sm"> --}}
        {{-- <a href="/Account/Watchcare" class="btn btn-swico-blue-light btn-lg" style="text-decoration: underline; font-size: 14px; padding: 0"> --}}
        {{-- CREATE MY ACCOUNT --}}
        {{-- </a> --}}
        {{-- </p> --}}

        {{-- </div> --}}

        {{-- </div> --}}

        {{-- </div> --}}
        {{-- </div> --}}
        {{-- </div> --}}

        {{-- </div> --}}
        {{-- </div> --}}


        <div id="container-price-calculator-mobile" class="bg-transparent md:hidden">
            <div class="container mx-auto px-4 bg-black bg-opacity-90">
                <div class="py-2">
                    <div class="w-full text-white">
                        <button class="flex w-full items-center justify-between text-white" @click="open('price-calculator')">
                            <div class="flex items-center">
                                <img src="{{ asset_url('/icon/navbar/pound.png') }}" width="25" height="25">
                                <span class="pl-2">QUICK QUOTE</span>
                            </div>
                            <div>
                                <img src="{{ asset_url('/icon/navbar/plus.png') }}" width="20" height="20">
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </nav>
