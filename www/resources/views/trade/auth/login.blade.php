<x-trade-layout>
    <div class="bg-dark-grey bg-no-repeat bg-cover"
         style="background-image: url('{{ asset_url('banners/login.jpg') }}');">
        <div class="hidden bg-no-repeat md:block"
             style="background-image: url('{{ asset_url('logo/logo_white.png') }}'); background-position: 100% 80%">
        <div class="container mx-auto px-6 py-6 relative md:py-24">
            <div class="flex justify-center">
                <x-forms.login-form class="w-full bg-white p-4 md:w-3/5 md:p-8 lg:w-2/5"
                                    :button-text="'Login to '.site_name()">
                    <div class="mb-6">
                        <div class="flex items-center justify-between mb-4">
                            <h3 class="text-2xl font-bold uppercase">Account Login</h3>
                            <div class="hidden md:block">
                                <img src="{{ asset_url('/icon/padlock.png') }}" alt="padlock">
                            </div>
                        </div>

                        <p>Enter your email address and password to log into your account</p>
                    </div>
                </x-forms.login-form>
            </div>
        </div>
        </div>
    </div>
</x-trade-layout>
