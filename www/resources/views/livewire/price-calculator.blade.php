<div>
    <div class="py-12 space-y-8">
        <div class="flex space-x-8">
            <div class="w-full md:w-1/2">
                <select wire:model="selectedBrand"
                        id="price-calculator-brand-select"
                        class="w-full">
                    @foreach($this->watchBrands() as $key => $brand)
                        <option value="{{ $key }}">{{ $brand }}</option>
                    @endforeach
                </select>
            </div>

            <div class="w-full md:w-1/2">
                @if ($this->selectedBrand)
                    <select wire:model="selectedService" class="w-full">
                        @foreach($this->brandServices() as $key => $service)
                            <option value="{{ $key }}">{{ $service }}</option>
                        @endforeach
                    </select>
                @endif
            </div>
        </div>

        @if ($this->selectedBrand && isset($this->servicesDetails()[$this->selectedService]['sub_services']))
            <div class="flex space-x-8">
                <div class="w-full w-1/2"></div>
                <div class="w-full w-1/2">
                    <select wire:model="selectedSubService" class="w-full">
                        @foreach($this->servicesDetails()[$this->selectedService]['sub_services'] as $key => $service)
                            <option value="{{ $key }}">{{ $service }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endif

        @if (($this->price && $this->time) || $this->other)
            <div class="flex space-x-8">
                @if ($this->price && $this->time)
                    <div class="w-full w-1/2">
                        <div class="flex items-center space-x-4">
                            <div
                                class="text-black bg-white py-2 px-6 font-bold text-2xl">{{ $this->price }}</div>
                            <div>
                                <img src="{{ asset_url('icon/calendar.png') }}" alt="...">
                            </div>
                            <span
                                class="text-xl text-white">{!! $this->time !!}</span>
                        </div>
                    </div>

                    <div class="w-full w-1/2 flex justify-end">
                        <a href="{{ route('service-repair') }}" class="btn btn-primary">
                            START REPAIR
                        </a>
                    </div>
                @endif

                @if ($this->other)
                        <div class="w-full w-1/2">
                            <div class="flex items-center space-x-4">
                                <div class="text-black bg-white py-2 px-6 font-bold text-2xl">
                                    {{ $this->other }}
                                </div>
                                <span class="text-xl text-white">{!! $this->time !!}</span>
                            </div>
                        </div>
                @endif
            </div>
        @endif
    </div>
</div>
