<div class="md:flex md:flex-shrink-0 relative md:z-10">
    <div x-show="sidebarOpen" class="md:hidden"
         x-description="Off-canvas menu for mobile, show/hide based on off-canvas menu state.">
        <div class="fixed inset-0 flex z-40">
            <div @click="sidebarOpen = false" x-show="sidebarOpen"
                 x-description="Off-canvas menu overlay, show/hide based on off-canvas menu state."
                 x-transition:enter="transition-opacity ease-linear duration-300" x-transition:enter-start="opacity-0"
                 x-transition:enter-end="opacity-100" x-transition:leave="transition-opacity ease-linear duration-300"
                 x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" class="fixed inset-0"
                 aria-hidden="true" style="display: none;">
                <div class="absolute inset-0 bg-grey-400 opacity-75"></div>
            </div>
            <div x-show="sidebarOpen" x-description="Off-canvas menu, show/hide based on off-canvas menu state."
                 x-transition:enter="transition ease-in-out duration-300 transform"
                 x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0"
                 x-transition:leave="transition ease-in-out duration-300 transform"
                 x-transition:leave-start="translate-x-0" x-transition:leave-end="-translate-x-full"
                 class="relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-dark-grey" style="display: none;">
                <div class="absolute top-0 right-0 -mr-12 pt-2">
                    <button x-show="sidebarOpen" @click="sidebarOpen = false"
                            class="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                            style="display: none;">
                        <span class="sr-only">Close sidebar</span>
                        <svg class="h-6 w-6 text-white" x-description="Heroicon name: x"
                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                             aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                  d="M6 18L18 6M6 6l12 12"></path>
                        </svg>
                    </button>
                </div>
                <div class="flex-shrink-0 flex items-center px-6">
                    <img class="w-auto" src="{{ asset_url('swico-logo.png') }}" alt="Icon">
                </div>
                <div class="mt-5 flex-1 h-0 overflow-y-auto">
                    <nav class="px-2 space-y-1">
                        <x-menu/>
                    </nav>
                </div>
            </div>
            <div class="flex-shrink-0 w-14" aria-hidden="true">
                <!-- Dummy element to force sidebar to shrink to fit close icon -->
            </div>
        </div>
    </div>

    <div class="hidden md:flex md:flex-shrink-0">
        <div class="flex flex-col {{ $this->minimised ? 'w-24' : 'w-64' }}">
            <!-- Sidebar component, swap this element with another sidebar if you like -->
            <div class="flex flex-col h-0 flex-1">
                <div class="flex items-center h-32 flex-shrink-0 px-6 bg-dark-grey">
                    @if ($this->minimised)
                        <img class="w-auto" src="{{ asset_url('swico-icon.png') }}" alt="Icon">
                    @else
                        <img class="w-auto" src="{{ asset_url('swico-logo.png') }}" alt="Logo">
                    @endif
                </div>
                <div class="flex-1 flex flex-col">
                    <nav class="flex-1 px-6 py-4 bg-dark-grey space-y-1 relative">
                        <x-menu :minimised="$this->minimised"/>

                        <button
                            class="absolute right-0 transform translate-x-2/4 translate-y-12 text-white bg-dark-grey border-2 border-white rounded-full h-6 w-6 flex justify-center items-center focus:outline-none"
                            wire:click="toggleMinimised">
                            @php $class = $this->minimised ? 'transform rotate-180' : ''; @endphp
                            {{ svg('icon-arrow', "h-2 w-2 $class") }}
                        </button>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
