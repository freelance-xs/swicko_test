<x-guest-layout>

    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}" background-overlay="{{ asset_url('icon/banner-shield-overlay.png') }}">
        <x-slot name="preTitle">Watch Repairs By Post<br>A Free Secure Postal Service</x-slot>
        <x-slot name="mainTitle">
            START YOUR... <br> WARRANTY CLAIM
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
        <x-slot name="rightColumn">
            <div class="flex justify-end items-end"><img src="{{ asset_url('logo/logo_swico_white_small.png') }}"></div>
        </x-slot>
    </x-hero>

    <div class="container relative mx-auto py-10" style="min-height: 400px;">
        <img src="{{ asset_url('icon/arrow_down_white.png') }}" class="absolute -bottom-12 left-20" alt="...">
        <div class="md:-mx-10 relative">
            <div class="md:w-1/2 float-left md:px-10">
                <img class="mb-5" src="{{ asset_url('icon/tick_shield_blue.png') }}" alt="...">
                <h2 class="font-bold mb-5 uppercase">create your account</h2>
                <h6 class="font-bold mb-5 text-grey-200">Welcome to SWICO Watch Service Centre.</h2>
                    <p class="">
                        In order to send your watch you will need to create an account. Please fill in the form below to create your account.
                    </p>
                    <a href="{{ route('estimate-by-post') }}">
                        REPAIRS BY POST - How does it work?
                    </a>
            </div>
            <div class="md:w-1/2 md:px-10 float-right">
                <div>
                    <img src="{{ site_name() === 'swico' ? asset_url('image/watch_three.png') : asset_url('image/watch_three_swissmade.png') }}">
                </div>
            </div>
        </div>
    </div>


    <div class="bg-grey-100 py-10 lg:py-24 static">
        <div class="clear-both table"></div>
        <div class="container mx-auto">
            <div class="lg:flex lg:-mx-3">
                <div class="lg:w-4/6 lg:px-3 mb-6">
                    <div class="bg-white p-5">
                        <x-forms.register-form warranty />
                    </div>
                </div>
                <div class="lg:w-2/6 lg:px-3">
                    <div class="bg-grey-300 p-8 mb-6">
                        <img src="{{ asset_url('icon/profile_white.png') }}" class="mb-5" alt="...">
                        <h2 class="uppercase font-bold leading-8 mb-5">Already registered with {{site_name()}}?</h2>
                        <x-forms.login-form mode="light" />
                    </div>
                    <div class="bg-dark-grey p-8 mb-6">
                        <img src="{{ asset_url('icon/file_text_data.png') }}" class="mb-5" alt="...">
                        <h2 class="uppercase font-bold text-white leading-8 mb-5">HAVE YOU RECEIVED AN ESTIMATE BY POST?</h2>
                        <p class="text-white text-lg mb-8">Have you received an estimate and registration code in the post? Please complete the online account set up to manage your repair online.</p>
                        <a href="{{ 'estimate-by-post' }}" class="btn btn-primary uppercase">activate</a>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <x-split-section image="{{site_name() === 'swico' ? 'image/shutterstock_796998178.jpg' : 'image/dial.jpg'}}" :bg_color="'white'">
        <div>
            <div class="mb-6">
                <img src="{{ asset_url('icon/tick_shield_blue.png') }}" alt="...">
            </div>
            <h2 class="text-3xl uppercase font-bold tracking-tight mb-3">warranty claim</h2>
            <p class="mb-6">
                If your watch is covered by a valid warranty and you have an aftersales enquiry, we're here to help.
            </p>
            <div class="flex">
                <a class="btn btn-primary uppercase w-full text-center md:w-auto" href="{{ route('warranty-information') }}">
                    Read More
                </a>
            </div>
        </div>
    </x-split-section>

    @switch(site_name())
        @case('swissmade')
        <x-watch-brands mode="dark" :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
        @break
        @case('swico')
        <x-watch-brands mode="dark" :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
        @break
        @default
    @endswitch
</x-guest-layout>
