<x-guest-layout>
    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}">
        <x-slot name="preTitle">
            customer <br> information
        </x-slot>
        <x-slot name="mainTitle">
            about <br> <span class="font-bold"> {{ site_name() }}</span>
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
    </x-hero>

    <div class="container mx-auto py-24 relative">
        <div class="md:flex md:-mx-10">
            <div class="md:w-1/2 md:px-10">
                <h2 class="uppercase font-bold mb-5">OUR COMPANY</h2>
                @if (site_name() === 'swico')
                    <h4 class="text-grey-300 mb-5">SWICO has enjoyed long and successful working partnerships with watch manufacturers since 1976, handling in excess of 30,000 watch repairs annually on their behalf. We are the Officially Appointed UK Service & Repair Centre for our manufacturers and are specialists in the after sales support, repair and maintenance of our brands' watches for retail shops and consumers direct.</h4>
                    <p class="mb-5">
                        In 2013 we launched our online service. This has provided customers with a fast, convenient online service to manage their watch care requests directly with us from anywhere in the UK using our free secure postal service.
                    </p>
                    <img src="{{ asset_url('logo/SWICO-ICON-GREY-RGB-SMALL.png') }}" alt="...">
                @elseif( site_name() === 'swissmade' )
                    <h4 class="text-grey-300 mb-5">
                        Based in Hatton Garden in the heart of London’s jewellery quarter, Swissmade specialise in the repair and maintenance of quality Swiss-made watches.
                    </h4>
                    <p>
                        Swissmade are the Official UK Service Centre for Ebel and Baume & Mercier, an Authorised Level 3 Service Centres for Omega and Longines, and has an Official Rolex Accredited Watchmaker. Our expertise enables us to service and refurbish these and other famous Swiss watch brands. All watch repairs are carried out with manufacturer supplied original parts and some obsolete parts are even hand crafted at our premises - so potentially no watch is beyond repair. The appearance
                        of a
                        watch is always a major consideration for our customers and where necessary cases and watch dials are sympathetically restored and polished.
                    </p>

                    <p>
                        Since becoming part of the SWICO Group in 2015, Swissmade has been extended and a complete refurbishment of its workshops has taken place, together with a system’s update; streamlining services through an automated technology platform. Customers can book and view the status of their watch repairs online themselves and a dedicated customer service team are available to advise and talk customers through the processes involved in the restoration, repair or maintenance of
                        their
                        time pieces.
                    </p>
                @endif
            </div>
            <div class="md:w-1/2 md:px-10">
                <img class="w-full" src="{{ site_name() === 'swico' ? asset_url('logo/SWICO-LOGO-HORIZONTAL-RGB.jpg') : asset_url('image/SM-workshop.jpg') }}" alt="...">
            </div>
        </div>
    </div>

    <div class="bg-grey-100 md:py-24 py-10">
        <div class="container mx-auto">
            <h2 class="uppercase font-bold mb-5">WHY CHOOSE US?</h2>
            <h4 class="text-grey-500 mb-10">
                @if (site_name() === 'swico')
                    Our team and specialist watchmakers are equipped and trained to look after your watch throughout its lifetime. The experienced customer service team oversee a watch's journey from arrival at our centre to its safe return. Customers receive progress updates online and by email. Dedicated to providing a fast, first class service, we have created a convenient online portal to access our Service Centre.
                @elseif( site_name() === 'swissmade' )
                    Our team and specialist Swiss trained watchmakers are equipped and trained directly by the watch manufacturers to look after your watch through its whole lifetime to the highest standard. The experienced Customer Care team oversee the watch’s journey from arrival at our centre to its safe return, customers receive progress updates online and by email. Dedicated to providing a fast, first class service, we have created a convenient online portal to access our Service Centre
                    from anywhere in the UK using our FREE secure postal service.
                @endif
            </h4>
            <h2 class="uppercase font-bold mb-5">WHAT WE DO</h2>
            <div class="mb-10">
                <ul class="list-disc pl-5">
                    <li>Watch Batteries &amp; Reseal.</li>
                    <li>Movement Repairs &amp; Servicing.</li>
                    <li>Refurbishment - Valeting &amp; Polishing.</li>
                    <li>Dial Restoration. </li>
                    <li>Case Repairs.</li>
                    <li>Glass Replacement.</li>
                    <li>Straps &amp; Bracelets - Adjustments, Repairs or Replacements.</li>
                    <li>Pressure testing and resealing.</li>
                    <li>Ultrasonic Cleaning.</li>
                    <li>Maintenance Service and Care.</li>
                </ul>
            </div>
            <div class="md:flex py-10 md:-mx-10">
                <div class="md:w-1/2 md:px-10">
                    <p>
                        <span class="font-bold uppercase">MANUFACTURERS WARRANTY CLAIM AGENT {{ site_name() }}</span>
                        @if (site_name() === 'swico')
                            are the Official UK Service Centre authorised by our watch brand manufacturers to repair their products under warranty. Your watch manufacturer provides a 2 year international warranty from date of purchase. This 2 year warranty is valid providing the watch was purchased from an authorised dealer and is supplied with a stamped and dated warranty card or proof of purchase. The manufacturer’s warranty
                            does not cover accidental damage and
                            is invalidated by a repair or intervention by an unauthorised party.

                        @elseif( site_name() === 'swissmade' )
                            are the Official UK Service Centre for Ebel and Baume & Mercier, authorised by the manufacturer to repair any defects under the manufacturers warranty. Your watch manufacturer provides a 2 year manufactures warranty from date of purchase. The limited warranty is valid providing it was purchased from an authorised dealer, as evidence by a valid warranty card or proof of purchase. Manufacturers warranty doesn’t cover accidental damage or becomes void in the event that
                            any person not duly authorised by the manufacturer attempts to repair the claimed defect.
                        @endif
                    </p>
                    <p>
                        <span class="font-bold">FREE NO OBLIGATION ESTIMATES</span> We provide free, no obligation, estimates for all repair and maintenance queries for out-of-warranty watches using our free postal service. Once we receive your watch, we will prepare an accurate estimate for your approval. If you would like to proceed you can make a payment online or you will have the option to reject the estimate and your watch will be returned at no cost to you.
                    </p>
                    <p>
                        <span class="font-bold">WORKSHOP & EQUIPMENT</span> Operating from our purpose-built secure service centre in Surrey, our workshop uses only the latest tools and manufacturer approved equipment to ensure the highest quality of work is carried out. This is vital to ensure your watch is cared for correctly. We have a range of machinery including; Timing Machines; Cleaning Machines; Ultrasonic Cleaners; Demagnetisers; Analysers; Pressure Testing Machines; Case-back opening
                        tools; Polishing Machine;
                        Watch-winder; all in accordance to the watch brands’ procedures and regulations.
                    </p>
                    <p>
                        <span class="font-bold">GENUINE MANUFACTERES PARTS USED</span> We only ever use the manufacturers’ genuine and approved spare parts with the specialist tools and equipment. We order our parts direct from the manufacturer and carry an extensive range.
                    </p>
                </div>
                <div class="md:w-1/2 md:px-10">
                    <p>
                        <span class="font-bold">FREE BUFFING AND CLEANING</span> All watches we receive are Ultra Sonic Cleaned before being returned. The watch is separated out into its component parts, cleaned and re-assembled according to brand techniques. Ultrasonic cleaning works by vibrating a pool of water with an object in it. The vibrations create tiny bubbles that attach themselves to small particulates of dirt and debris and pull them off.
                    </p>

                    <p>
                        <span class="font-bold">QUALITY CONTROL</span> After each watch is cleaned it passes through Quality Control. We check not only the aesthetics and that your instructions have been completed but also verify the technical characteristics of your watch by testing the timekeeping, water resistance and power reserve. If these are within the manufacturer’s tolerance parameters then the watch is passed, if not the watch is returned to the watchmaker for further fine tuning.
                        Quality control can take between 1
                        and
                        5 days depending on the requirements and type of watch being tested.
                    </p>

                    <p>
                        <span class="font-bold">QUICK TURNAROUND</span> We endeavour to return your watch to you as quickly as possible. Following an inspection of your watch and job approval we will send you an approximate completion date. We estimate a full service to be returned within 20 working days, for minor work including battery replacements & reseals or strap replacements within 5 working days. All timescales are subject to availability of parts. We reserve the right to re-estimate if
                        we find more work is required.
                    </p>

                    <p>
                        <span class="font-bold">SERVICE GUARANTEE PROMISE</span> Our watch movement services are returned to you with a 12-month SWICO guarantee.
                    </p>
                    <p>
                        <span class="font-bold">TESTIMONIALS</span> Please see our independent reviews on our testimonial page.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <x-swico.services-grid />

    @if (site_name() === 'swico')
        <x-swico.swissmade />
    @elseif( site_name() === 'swissmade' )

    @endif
</x-guest-layout>
