<x-app-auth-layout>
    <x-slot name="backgroundOverlay">
        {{ asset_url('icon/account/banner_watch.png') }}
    </x-slot>
    <x-slot name="bannerTitle">
        REGISTER MY WATCH <br>
        WITH SWISSMADE WATCHCARE
    </x-slot>
    <div class="flex items-center mb-10">
        <div class="mr-5">
            <img src="{{ asset_url('icon/account/account_watch_blue.png') }}" alt="...">
        </div>
        <h2 class="uppercase font-bold">TELL US ABOUT YOUR WATCH</h2>
    </div>

    <div class="bg-grey-100 p-3">
        <x-forms.account.account-register-your-watch-form />
    </div>

    <x-slot name="footerContent">
        @switch(site_name())
            @case('swissmade')
            <x-watch-brands mode="dark" :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
            @break
            @case('swico')
            <x-watch-brands mode="dark" :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
            @break
            @default
        @endswitch
    </x-slot>
</x-app-auth-layout>
