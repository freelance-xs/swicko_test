<x-app-auth-layout>
    <x-slot name="backgroundOverlay">
        {{ asset_url('icon/account/banner_settings.png') }}
    </x-slot>
    <x-slot name="bannerTitle">
        SEND MY WATCH
    </x-slot>
    <div class="flex items-center mb-10">
        <div class="mr-5">
            <img src="{{ asset_url('icon/account/account_watch_blue.png') }}" alt="...">
        </div>
        <h2 class="uppercase font-bold"> CREATE A NEW SERVICE / REPAIR REQUEST</h2>
    </div>

    <x-forms.account.account-send-my-watch-form />

    <x-slot name="footerContent">
        <x-layout.page-section contain>
            <x-cta.box-wrapper class="md:flex">
                <x-cta.box class="flex-1 my-5" bg-color="bg-grey-500">
                    <x-slot name="icon">
                        <img src="{{ asset_url('icon/account/account_settings_black.png') }}" alt="...">
                    </x-slot>
                    <x-slot name="stats">
                        <div class="bg-white rounded-full p-2 font-bold">10</div>
                    </x-slot>
                    <h3 class="uppercase text-white mb-10">view my services <br> & repairs</h3>
                    <x-slot name="button">
                        <a href="{{ route('account-my-repairs') }}" class="btn btn-primary inline-block uppercase ">view my repairs</a>
                    </x-slot>
                </x-cta.box>
                <x-cta.box class="flex-1 my-5" bg-color="bg-dark-grey">
                    <x-slot name="icon">
                        <img src="{{ asset_url('icon/account/account_watch_blue.png') }}" alt="...">
                    </x-slot>
                    <h3 class="uppercase text-white mb-10">register my watch <br> for {{ site_name() }} <br> watchcare</h3>
                    <x-slot name="button">
                        <a href="{{ route('account-register-your-watch') }}" class="btn btn-primary inline-block uppercase ">view my repairs</a>
                    </x-slot>
                </x-cta.box>
            </x-cta.box-wrapper>
        </x-layout.page-section>

        @switch(site_name())
            @case('swissmade')
            <x-watch-brands mode="dark" :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
            @break
            @case('swico')
            <x-watch-brands mode="dark" :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
            @break
            @default
        @endswitch

    </x-slot>
</x-app-auth-layout>
