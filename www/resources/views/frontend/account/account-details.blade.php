<x-app-auth-layout testingProp="foobar">
    <x-slot name="backgroundOverlay">
        {{ asset_url('icon/account/banner_my_account.png') }}
    </x-slot>
    <x-slot name="bannerTitle">
        my account <br> settings
    </x-slot>

    <div class="flex items-center mb-10">
        <div class="mr-5">
            <img src="{{ asset_url('icon/option_bar_settings.png') }}" alt="...">
        </div>
        <h2 class="uppercase font-bold">my profile</h2>
    </div>
    <div x-data="{ tab: window.location.hash ? window.location.hash.substring(1) : 'details' }" id="tab_wrapper">
        <!-- The tabs navigation -->
        <div class="flex flex-wrap items-center">
            <a class="mr-3 mt-3" @click.prevent="tab = 'details'; window.location.hash = 'details'" href="#">
                <div :class="{ 'bg-dark-grey ': tab === 'details', 'bg-grey-300': tab !== 'details'  }" class="p-4 text-lg text-white capitalize">my details</div>
            </a>
            <a class="mr-3 mt-3" @click.prevent="tab = 'address'; window.location.hash = 'address'" href="#">
                <div :class="{ 'bg-dark-grey ': tab === 'address', 'bg-grey-300': tab !== 'address'  }" class="p-4 text-lg text-white capitalize">my address</div>
            </a>
            <a class="mr-3 mt-3" @click.prevent="tab = 'reset-password'; window.location.hash = 'reset-password'" href="#">
                <div :class="{ 'bg-dark-grey ': tab === 'reset-password', 'bg-grey-300': tab !== 'reset-password'  }" class="p-4 text-lg text-white capitalize">reset password</div>
            </a>
            <a class="mr-3 mt-3" @click.prevent="tab = 'contact-preferences'; window.location.hash = 'contact-preferences'" href="#">
                <div :class="{ 'bg-dark-grey ': tab === 'contact-preferences', 'bg-grey-300': tab !== 'contact-preferences'  }" class="p-4 text-lg text-white capitalize">contact preferences</div>
            </a>
        </div>

        <div class="bg-grey-100 p-5">

            <!-- The tabs content -->
            <div x-show="tab === 'details'">
                <x-forms.account.account-details-form />
            </div>
            <div x-show="tab === 'address'">
                <x-forms.account.account-address-form />
            </div>
            <div x-show="tab === 'reset-password'">
                <x-forms.account.account-reset-password-form />
            </div>
            <div x-show="tab === 'contact-preferences'">
                <x-forms.account.account-contact-preferences-form />
            </div>
        </div>
    </div>
    <x-slot name="footerContent">
        <x-layout.page-section contain>

            <x-cta.box-wrapper class="md:flex">
                <x-cta.box class="flex-1 my-5" bg-color="bg-grey-500">
                    <x-slot name="icon">
                        <img src="{{ asset_url('icon/account/account_settings_black.png') }}" alt="...">
                    </x-slot>
                    <x-slot name="stats">
                        <div class="bg-white rounded-full p-2 font-bold">10</div>
                    </x-slot>
                    <h3 class="uppercase text-white mb-10">view my services <br> & repairs</h3>
                    <x-slot name="button">
                        <a href="{{ route('account-my-repairs') }}" class="btn btn-primary inline-block uppercase ">view my repairs</a>
                    </x-slot>
                </x-cta.box>
                <x-cta.box class="flex-1 my-5">
                    <x-slot name="icon">
                        <img src="{{ asset_url('icon/account/account_watch_black.png') }}" alt="...">
                    </x-slot>
                    <h3 class="uppercase text-white mb-10">my watches</h3>
                    <x-slot name="button">
                        <a href="{{ route('account-my-watches') }}" class="btn btn-black inline-block uppercase ">view watches</a>
                    </x-slot>
                </x-cta.box>
            </x-cta.box-wrapper>

        </x-layout.page-section>
        @switch(site_name())
            @case('swissmade')
            <x-watch-brands mode="dark" :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
            @break
            @case('swico')
            <x-watch-brands mode="dark" :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
            @break
            @default
        @endswitch
    </x-slot>
</x-app-auth-layout>
