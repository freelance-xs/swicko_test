<x-guest-layout>

    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}" background-overlay="{{ asset_url('logo/logo_white.png') }}">
        <x-slot name="preTitle">customer <br> information</x-slot>
        <x-slot name="mainTitle">
            faqs
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
        <x-slot name="rightColumn">
            <div class="flex justify-end items-end"><img src="{{ asset_url('logo/logo_swico_white_small.png') }}"></div>
        </x-slot>
    </x-hero>

    <div class="container mx-auto md:py-24 py-10">
        <div class="md:flex md:-mx-10">
            <div class="md:w-1/2 md:px-10">
                <h2 class="uppercase font-bold">
                    @if (site_name() === 'swico')
                        WATCH REPAIR AND SERVICING FREQUENTLY ASKED QUESTIONS
                    @elseif( site_name() === 'swissmade' )
                        WATCH REPAIR AND SERVICING FREQUENTLY ASKED QUESTIONS
                    @endif
                </h2>
                <h4 class="text-grey-300 my-5">
                    Below is a comprehensive list of watch repair and servicing questions that we get asked about most often.
                </h4>
                <p class="my-5">
                    If however you can’t find what you need, please feel free to get in touch and a member of our experienced customer services team will be happy to help.
                </p>

                <div class="my-10">
                    <a href="{{ route('contact') }}" class="btn btn-primary my-5 uppercase">contact {{ site_name() }}</a>
                </div>

            </div>
            <div class="md:w-1/2 md:px-10">
                <img src="{{ asset_url('image/faqs.jpg') }}" alt="...">
            </div>

        </div>
    </div>

    <x-layout.page-section contain class="bg-grey-100">
        <h2 class="font-bold mb-10">Repair Questions</h2>

        <x-accordion.accordion>

            <x-accordion.accordion-item class="bg-white" index="1" id="x1">
                <x-slot name="trigger">
                    <span class="font-bold">How do I send my watch to you?</span>
                </x-slot>
                <div>
                    Create an account, then submit a ‘new repair request’ in my services/repairs. We will then send you prepaid packaging so you can post your watch back to us securely through your local Post Office.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                <x-slot name="trigger">
                    <span class="font-bold">
                        How will I know if you have received my watch?
                    </span>
                </x-slot>
                <div>
                    You will be sent an email within a couple of days of us receiving your watch with our assessment of work to be carried out. You can also track the progress of your service/repair any time by logging into your account.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-white" index="3" id="x3">
                <x-slot name="trigger">
                    <span class="font-bold">
                        Will you send me an estimate for the repair?
                    </span>
                </x-slot>
                <div>
                    Once your watch has been assessed by one of our watch technicians and the required work to be carried out is established, we shall email you a detailed estimate for the repair of your watch.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-white" index="4" id="x4">
                <x-slot name="trigger">
                    <span class="font-bold">
                        When will you start the repair?
                    </span>
                </x-slot>
                <div>
                    When you have agreed to the estimate and paid for your watch’s repair through your account.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-white" index="5" id="x5">
                <x-slot name="trigger">
                    <span class="font-bold">
                        How do I track my repair?
                    </span>
                </x-slot>
                <div>
                    You will receive email notifications as your watch moves through our service centre and is despatched. You can also track the progress of your repair any time throughout the process, simply log into your account.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-white" index="6" id="x6">
                <x-slot name="trigger">
                    <span class="font-bold">
                        How long will I have to wait?
                    </span>
                </x-slot>
                <div>
                    Upon receiving payment or warranty claims approved you will receive an email providing you with an estimated completion date. Generally service/repairs take 4 to 6 weeks to complete. However, occasionally it will be necessary to source parts from the manufacturer which would delay the process. In this event you will be advised by email.
                    <br>
                    We hold spare parts for the vast majority of standard repair processes at our service centre. However, occasionally it will be necessary to source the parts from one of our manufacturers which may delay the process. In this event you will be advised by email.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-white" index="7" id="x7">
                <x-slot name="trigger">
                    <span class="font-bold">
                        When will you return my watch to me?
                    </span>
                </x-slot>
                <div>
                    Once your watch has been serviced/repaired it will be returned to you by special delivery post.
                    <br>
                    We shall send you an email on the day of dispatch with the courier’s parcel tracking number and contact details.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-white" index="8" id="x8">
                <x-slot name="trigger">
                    <span class="font-bold">
                        Can I bring my watch to you personally?
                    </span>
                </x-slot>
                <div>
                    <p>Yes of course, you can find us at: </p>
                    <div class="font-bold text-grey-300">
                        <p>
                            SWICO WATCH SERVICE CENTRE<br>
                            Meadway<br>
                            Haslemere<br>
                            Surrey<br>
                            GU27 1NN<br><br>
                        </p>

                        <p>
                            We are open from 9am until 4pm for repairs.
                        </p>
                    </div>
                </div>
            </x-accordion.accordion-item>
        </x-accordion.accordion>
    </x-layout.page-section>


    <x-layout.page-section contain>
        <h2 class="font-bold mb-10">Payment Questions</h2>

        <x-accordion.accordion>

            <x-accordion.accordion-item class="bg-grey-100" index="1" id="x1">
                <x-slot name="trigger">
                    <span class="font-bold">
                        Which payment options do you accept?
                    </span>
                </x-slot>
                <div>
                    We accept all major Credit/Debit cards with the exception of American Express.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-grey-100" index="2" id="x2">
                <x-slot name="trigger">
                    <span class="font-bold">
                        How do I pay for my service/repair?
                    </span>
                </x-slot>
                <div>
                    You can log into your account and pay online for your repair by credit or debit card. Repairs will not be undertaken prior to your payment.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-grey-100" index="3" id="x3">
                <x-slot name="trigger">
                    <span class="font-bold">
                        Is there another way I can pay instead of paying online?
                    </span>
                </x-slot>
                <div>
                    <p>
                        If you would prefer to pay over the telephone please contact our repair centre on +44 (0)1428 664704.
                    </p>
                    <p>We accept cheques made payable to SWICO Limited, however this will delay your speed of service. Please note that a payment made online with a credit/debit card will speed up the process.</p>

                    <x-note bgColor="white">No repair is undertaken without the payment clearing first.</x-note>
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-grey-100" index="4" id="x4">
                <x-slot name="trigger">
                    <span class="font-bold">
                        Are my credit or debit card details saved in my account?
                    </span>
                </x-slot>
                <div>
                    No, we store no financal information on our servers.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-grey-100" index="5" id="x5">
                <x-slot name="trigger">
                    <span class="font-bold">
                        How do I contact the SWICO accounts department?
                    </span>
                </x-slot>
                <div class="text-grey-300 font-bold text-lg">
                    Tel:<a href="tel:+441428656822"> 01428 656822</a> <br>
                    Email: <a href="mailto:accounts@swico.co.uk">accounts@swico.co.uk</a>
                </div>
            </x-accordion.accordion-item>

        </x-accordion.accordion>
    </x-layout.page-section>

    <x-layout.page-section contain class="bg-grey-100">
        <h2 class="font-bold mb-10">Repair Change / Cancellation Questions</h2>

        <x-accordion.accordion>

            <x-accordion.accordion-item class="bg-white" index="1" id="x1">
                <x-slot name="trigger">
                    <span class="font-bold">Can I cancel a repair online?</span>
                </x-slot>
                <div>
                    Providing the repair is at estimate stage, of course.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                <x-slot name="trigger">
                    <span class="font-bold">Can I update or change my account details?</span>
                </x-slot>
                <div>
                    Of course. Simply sign in to your account to update your personal details.
                </div>
            </x-accordion.accordion-item>
        </x-accordion.accordion>
    </x-layout.page-section>

    <x-layout.page-section contain>
        <h2 class="font-bold mb-10">Warranty &#38; Guarantee Questions</h2>

        <x-accordion.accordion>

            <x-accordion.accordion-item class="bg-grey-100" index="1" id="x1">
                <x-slot name="trigger">
                    <span class="font-bold">
                        What are the terms of my guarantee?
                    </span>
                </x-slot>
                <div>
                    An International Guarantee applies only to watches purchased at an approved dealer and where the GUARANTEE CARD has been filled in completely and correctly by the dealer. It is valid for the stated period on the guarantee card from the date of purchase.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-grey-100" index="2" id="x2">
                <x-slot name="trigger">
                    <span class="font-bold">
                        I have no guarantee card but have a purchase receipt. Is this OK?
                    </span>
                </x-slot>
                <div>
                    Yes, providing the watch has been purchased at an approved dealer.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-grey-100" index="3" id="x3">
                <x-slot name="trigger">
                    <span class="font-bold">
                        Does my guarantee cover accidental damage?
                    </span>
                </x-slot>
                <div>
                    Accidental damaged is not covered by any guarantee card.
                </div>
            </x-accordion.accordion-item>

        </x-accordion.accordion>
    </x-layout.page-section>


    <x-layout.page-section contain class="bg-grey-100">
        <h2 class="font-bold my-10">Cases, Bracelets &#38; Straps</h2>
        <div class="bg-dark-grey items-center p-5 text-white text-sm flex">
            <div class="flex">
                <div style="width: 20px;">

                    <svg class="svg-inline--fa fa-exclamation-triangle fa-w-18 fa-lg" aria-hidden="true" data-fa-processed="" data-prefix="fa" data-icon="exclamation-triangle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                        <path fill="currentColor"
                              d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z">
                        </path>
                    </svg>
                </div>

                &nbsp;&nbsp;Watch straps are not covered by our International Guarantee because, as with all leather goods, their durability depends entirely on the life style of the individual owner.
            </div>
        </div>

        <x-accordion.accordion>

            <x-accordion.accordion-item class="bg-white" index="1" id="x1">
                <x-slot name="trigger">
                    <span class="font-bold">Can I purchase a new case and or bracelet for my watch?</span>
                </x-slot>
                <div>
                    Stock permitting, we endeavour to source and supply cases and bracelets for all of our models.<br>
                    NB. In the case of bracelets this only applies to models originally fitted with a metal bracelet.
                </div>
            </x-accordion.accordion-item>

            <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                <x-slot name="trigger">
                    <span class="font-bold">Can I have my watch case and bracelet refurbished?</span>
                </x-slot>
                <div>
                    Depending on the condition of your watch, we may be able to refurbish steel, steel and gold or solid gold models. Gold plated watches are not suitable for refurbishment. However, stock permitting, we may be able to source and supply new cases and bracelets for current and certain older models.
                </div>
            </x-accordion.accordion-item>
            <x-accordion.accordion-item class="bg-white" index="3" id="x3">
                <x-slot name="trigger">
                    <span class="font-bold">Can I purchase a replacement strap for my watch?</span>
                </x-slot>
                <div>
                    Stock permitting, we should be able to source and supply new straps for old. <br>

                    Please contact our strap department by email or phone with your request. Any time between 9am and 5.30pm weekdays. <br>

                    Tel: +44(0)1428 656822 / +44 (0)1428 664706 / +44 (0)1428 664704<br>
                    Email: <a href="mailto:straps@swico.co.uk">straps@swico.co.uk</a>
                </div>
            </x-accordion.accordion-item>
        </x-accordion.accordion>
    </x-layout.page-section>


    <x-swico.services-grid />

    @switch(site_name())
        @case('swissmade')
        <x-watch-brands mode="light" :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
        @break
        @case('swico')
        <x-watch-brands mode="light" :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
        @break
        @default
    @endswitch

</x-guest-layout>
