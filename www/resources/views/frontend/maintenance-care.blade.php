<x-guest-layout>

    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}">
        <x-slot name="preTitle">customer <br>information</x-slot>
        <x-slot name="mainTitle">
            MAINTENANCE <br>
            & CARE
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
    </x-hero>

    <div class="container mx-auto py-10 md:py-24">
        <x-split-section image="image/maintenance_care.jpg">
            <div>
                <h2 class="mb-5 font-bold">
                    MAXIMISE PERFORMANCE
                </h2>

                <h4 class="text-grey-300 mb-5">
                    Like any finely crafted mechanism, your watch requires periodic maintenance to ensure optimum performance.
                </h4>
                <p>
                    Maintenance includes disassembly, cleaning, replacement of parts as needed, reassembly and lubrication of the movement. A maintenance interval of 3 years is recommended, in addition to any required battery replacement.
                </p>
                <p class="mb-10">
                    If your watch needs repairs or adjustments it must be returned to the Authorised Service Centre agent. <span class="font-bold">Never open the watch yourself.</span>
                </p>

                <img src="{{ asset_url('icon/sc_icon_grey_small.png') }}" alt="...">
            </div>
        </x-split-section>
    </div>

    <div class="bg-grey-100 py-10 md:py-24">
        <div class="container mx-auto">
            <h2 class="font-bold mb-5">PRECAUTIONS</h2>
            <p class="text-primary text-lg">What should I do to ensure that my watch continues to provide excellent service for many years</p>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">magnetism</span>
                    </x-slot>
                    <div>
                        Avoid placing your watch on speakers or refrigerators, since these have strong magnetic fields. The exposure of a timepiece to strong magnetic fields will affect its time-keeping, and may cause it to stop. A quartz watch should resume working with its original accuracy when removed from the magnetic field; a mechanical timepiece may require demagnetization before it will operate again.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">dust</span>
                    </x-slot>
                    <div>
                        Dust can enter a watch if the case, crystal or crown is not intact. A small particle is enough to stop the movement. Have your watch repaired promptly if the crystal becomes loose, chipped or cracked, or if there is damage to the case or crown.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">SCREW-IN-CROWN</span>
                    </x-slot>
                    <div>
                        If your watch is equipped with a screw-down crown or pushers, be sure to screw it/them back down into the case after every operation, this ensures no water enters the mechanism.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">CHRONOGRAPH PUSH-PIECES</span>
                    </x-slot>
                    <div>
                        Do not operate chronograph pushers under water in order to prevent water entering the mechanism.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">CHEMICAL PRODUCTS</span>
                    </x-slot>
                    <div>
                        Avoid direct contact with solvents, detergents, perfumes, cosmetic products etc., as these may damage seals, straps, case/bracelet.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">shock</span>
                    </x-slot>
                    <div>
                        Do not drop your watch or bang it against a hard surface. Water resistance must be rechecked after every violent shock.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">CLEANING</span>
                    </x-slot>
                    <div>
                        Clean your watch regularly by wiping it with a soft cloth dampened with fresh water, especially after exposure to seawater.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">BATHING IN THE SEA</span>
                    </x-slot>
                    <div>
                        Always rinse your watch with warm water afterwards.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">TEMPERATURES</span>
                    </x-slot>
                    <div>
                        Avoid extreme temperatures above 60°C or 140°F below 0°C or 32°F or extreme temperature changes.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">GALVANIC GOLD-PLATED, ION-PLATED & PVD-FINISHED WATCHES</span>
                    </x-slot>
                    <div>
                        All scratches and knocks will eventually damage plating. Once damaged, day to day wear, perspiration and perfume will erode the plating.
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>

            <x-accordion.accordion>
                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        <span class="font-bold uppercase">WATER RESISTANCE</span>
                    </x-slot>
                    <div>
                        <p class="font-bold text-base">
                            Most watches are water resistant to 30 meters (3 bar/3ATM/99 feet); certain models are water resistant to even greater pressures/depths, as marked on the dial or case-back.
                        </p>
                        <div class="flex md:-mx-3 my-10">
                            <x-note class="flex-1 md:px-3" label="caution">
                                If the water resistance limit is not marked on your watch, do not subject it to a pressure/depth greater than 3 bar (3 ATM/30 meters/99 feet), or your watch could be damaged and your warranty will be voided.
                            </x-note>
                            <x-note class="flex-1 md:px-3" label="important">
                                Do not operate the watch crown, push-buttons on a chronograph model, or any other pushers/buttons when the timepiece is wet or under water.
                            </x-note>
                            <x-note class="flex-1 md:px-3">
                                The watch brand will not be responsible for any damage caused by or related to leakage originating from improper handling or damage to the case, crown, gaskets or crystal.
                            </x-note>
                        </div>
                        <p>
                            To maintain water resistance, yearly maintenance is required. The gaskets and crown which seal the case are subject to wear and deterioration under normal use. These parts should be inspected and replaced as needed each time the watch is opened for service, including battery changes.
                        </p>
                        <p>
                            The authorised service centre will automatically retest your watch for water resistance as a routine step in all service orders.
                        </p>
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>



        </div>
    </div>

    <div class="bg-white container mx-auto py-10 md:py-24 bg-right-top bg-no-repeat" style="background-image:url('{{ asset_url('logo/logo_swico_light_grey.jpg') }}')">
        <h2 class="uppercse mb-5 font-bold">STRAP & BRACELET CARE</h2>
        <h4 class="text-grey-300 w-2/3">While the life span of a watch strap depends on your care and wearing habits, as well as the climate in which you live, proper care will extend its life.</h4>

        <div class="md:flex md:-mx-3 py-10">
            <div class="flex-1 md:px-3">
                To preserve the condition of your leather strap as long as possible we recommend:
                <ul class="pl-5 list-disc">
                    <li>Leather straps should be kept dry.</li>
                    <li>Exposure to water may cause a leather watch strap to become brittle and eventually break.</li>
                    <li>Following exposure to chlorinated or salt water, gently damp wipe your leather strap with fresh water, and air dry.</li>
                    <li>To preserve the finish of the leather, contact with perfumes should be avoided.</li>
                    <li>Avoid contact with water and dampness to prevent discoloration and deformation.</li>
                    <li>Avoid prolonged exposure to sunlight to prevent the colour from fading.</li>
                    <li>Do not forget that leather is permeable! Therefore avoid contact with greasy substances and cosmetic products.</li>
                    <li>The life expectancy of leather (as an organic material) is limited, sweat and general wear and tear will reduce the life of any strap.</li>
                </ul>
                <p class="my-5">
                    Use warm water and a neutral pH soap applied to a soft cloth to gently clean a rubber strap; then wipe with a second clean cloth dipped in warm water, or rinse carefully under warm running water. Contact with acetone and ethyl alcohol should be avoided.
                </p>

            </div>
            <div class="flex-1 md:px-3">
                <p>
                    A natural gum rubber or “rubber cement pick-up” (found in art supply stores) may be used to remove most surface stains from a white leather strap or white silicone bracelet. Simply rub the natural gum gently across the soiled area until the strap/bracelet is clean. You can also use white cleansing milk for everyday maintenance.
                </p>
                <x-note>
                    <div class="font-bold">Your watch bracelets are not covered by the brands watch warranty.</div>
                </x-note>
                <p>
                    Bracelets should be regularly cleaned in warm soapy water to keep them in good condition, avoiding the build-up of grime which clogs and restricts the links. Once damaged, day to day wear, perspiration and perfume will erode the plating.
                </p>
                <x-note>
                    <div class="font-bold">Your watch strap/bracelet is not covered by the brands watch warranty.</div>
                </x-note>
            </div>
            <div class="flex-1 md:px-3">
                <img src="{{ asset_url('image/maintenance_watch.png') }}" alt="...">
            </div>
        </div>
    </div>

    <x-split-section image="image/maintenance_watch_two.jpg" :left="true" bgColor="grey-100">
        <div>
            <h2 class="mb-5 font-bold">
                BATTERY INFORMATION
            </h2>
            <h4 class="text-grey-300 mb-5">
                A quartz timepiece has a 1.55 Volt silver-oxide battery that is specially designed for watches. It will last approximately 12 – 18 months under normal use.
            </h4>
            <p class="font-bold">End-of-Life Feature:</p>
            <p>
                The seconds hand on many models will start to move haltingly in 4-second increments when the battery is almost exhausted, signalling the need to have it replaced. Battery replacement: It is important to use only the replacement battery recommended by SWICO. Take or send your watch brands Authorized Service Centre to have battery changed.
            </p>
            <x-note bgColor="white" class="my-5">
                <p class="font-bold">
                    Frequent use of special functions, like the stopwatch timer on chronograph models, may shorten battery life.
                </p>
            </x-note>
            <x-note bgColor="grey-300" class="my-5">
                <div>
                    <p class="text-white font-bold">Your old battery will be disposed of/recycled properly.</p>
                    <p class="font-bold">
                        DO NOT attempt to change the battery yourself.
                    </p>
                </div>
            </x-note>
        </div>
    </x-split-section>


    <x-swico.services-grid />
</x-guest-layout>
