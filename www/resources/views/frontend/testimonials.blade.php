<x-guest-layout>

    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}">
        <x-slot name="preTitle">customer <br> information</x-slot>
        <x-slot name="mainTitle">
            CUSTOMER <br> TESTIMONIALS
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
    </x-hero>

    <div class="container relative mx-auto py-10" style="min-height: 500px;">
        <img src="{{ asset_url('icon/arrow_down_white.png') }}" class="absolute -bottom-12 left-20" alt="...">
        <div class="md:-mx-10 relative">
            <div class="md:w-1/2 float-left md:px-10">
                <img class="mb-5" src="{{ asset_url('icon/tick_shield_blue.png') }}" alt="...">

                <h2 class="font-bold mb-5 uppercase">
                    FIND OUT WHAT OUR CUSTOMERS HAVE TO SAY ABOUT US
                </h2>
                <h3 class="mb-5 text-grey-200">
                    We have repaired and serviced thousands of watches over the years and are incredibly proud of the quality of work we produce and the level of customer service we provide.
                </h3>
                <p class="">
                    Below are just a few of the comments and reviews that have been left by our satisfied customers over the years.
                </p>
                <a href="{{ route('estimate-by-post') }}">
                    REPAIRS BY POST - How does it work?
                </a>
            </div>
            <div class="md:w-1/2 md:px-10 float-right">
                <div>
                    <img src="{{ asset_url('image/testimonials.jpg') }}" class="object-cover w-full" style="height: 600px;">
                </div>
            </div>
        </div>
        <div class="lg:hidden table clear-both"></div>
    </div>

    <div class="bg-grey-100 md:py-24 py-10">
        <div class="container mx-auto clear-both">
            <div class="md:flex md:-mx-6">
                <div class="flex-1 md:px-3">
                    <x-testimonial class="my-5">
                        <div>
                            {{ formatDate('11/01/20') }}
                            <br>
                            Hi
                            <br>
                            I would just like to say thank you for my watch repair the watch is running perfectly in fact better than when it was new. Sorry it's taken so long to thank you but I had misplaced your details.
                            Once again thank you.
                            <br>
                            Regards<br>
                            J Clough
                        </div>
                    </x-testimonial>
                    <x-testimonial class="my-5" bgColor="primary" quoteColor="white">
                        <div class="text-black">
                            07/10/19<br>
                            Dear All,<br>
                            Thank you for repairing my Raymond Weil watch. It looks fantastic.<br>
                            With kind regards <br>
                            P Forrest
                        </div>
                    </x-testimonial>
                    <x-testimonial class="my-5" bgColor="grey-300" quoteColor="black">
                        <div class="text-black">
                            28/09/19<br>
                            Received watch today, thank you so much for a first class service. Very happy.<br>
                            G Wiseman
                        </div>
                    </x-testimonial>
                    <x-testimonial class="my-5" bgColor="white">
                        <div class="text-black">
                            10/09/19 <br>
                            Good afternoon<br>
                            I received my watch today and would like to thank you for your fast and efficient service.<br>
                            Best Regards<br>
                            J Hutchins
                        </div>
                    </x-testimonial>
                </div>
                <div class="flex-1 md:px-3">
                    <x-testimonial class="my-5" bgColor="grey-300" quoteColor="black">
                        <div class="text-black">
                            29/08/19 <br>
                            I just wanted to say how impressed I am by your level of customer service and such an efficient turnaround.<br>
                            Thanks again, <br>
                            L Scott
                        </div>
                    </x-testimonial>
                    <x-testimonial class="my-5">
                        <div>
                            05/07/19 <br>
                            Dear {{ site_name() }} <br>
                            Thank you very much for the recent battery change on my Othello watch. The watch arrived this morning and I am grateful for your "accelerated return delivery". <br>
                            B Welsh
                        </div>
                    </x-testimonial>
                    <x-testimonial class="my-5" bgColor="primary" quoteColor="white">
                        <div class="text-black">
                            25/06/19 <br>

                            Good Morning, <br>
                            First of all, can I say thank you for repairing the bracelet - I have now received the watch. Secondly, thank you for the repair as a gesture of goodwill - this is very much appreciated and shows excellent customer service. <br>
                            Thanks again. <br>

                            Kind Regards, <br>
                            S Sweeney
                        </div>
                    </x-testimonial>
                    <x-testimonial class="my-5">
                        <div>
                            18/05/19 <br>

                            Just wanted to say thanks for safe return of my watch.. so happy right now! Great service, watch is still old and battered but looking refreshed and as I love it so much, I’m just pleased to have it back on my arm! Been so easy to do business with you. <br>

                            V Gerrish
                        </div>
                    </x-testimonial>
                </div>
                <div class="flex-1 md:px-3">
                    <x-testimonial class="my-5">
                        <div>
                            03/05/19 <br>
                            Thank you for your quick response, your customer service is excellent! <br>
                            Kind regards <br>
                            J Loveitt
                        </div>
                    </x-testimonial>
                    <x-testimonial class="my-5" bgColor="white">
                        <div>
                            09/03/19 <br>
                            Thank you for the great customer service yesterday, really appreciate the help. <br>
                            Best Regards <br>
                            P Joel
                        </div>
                    </x-testimonial>
                    <x-testimonial class="my-5" bgColor="grey-300" quoteColor="black">
                        <div class="text-black">
                            08/03/19 <br>
                            Dear Repairs department, <br>
                            This is a big 'Thank you' for the return and fixing of my Boss lady's wristwatch. I am delighted to be re-united with it once more. I was quite bereft without it. <br>
                            Best regards <br>
                            J Belcher
                        </div>
                    </x-testimonial>
                    <x-testimonial class="my-5" bgColor="primary" quoteColor="white">
                        <div class="text-black">
                            04/02/19 <br>

                            I received my watch today after it had been repaired and I'm so happy with it. I'm really pleased you carried out the repair for me and I can wear it again. Excellent customer service. Thank you. <br>

                            L Hacking
                        </div>
                    </x-testimonial>
                </div>
            </div>
        </div>
    </div>

    <x-swico.services-grid />

    @switch(site_name())
        @case('swissmade')
        <x-watch-brands mode="dark" :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
        @break
        @case('swico')
        <x-watch-brands mode="dark" :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
        @break
        @default
    @endswitch
</x-guest-layout>
