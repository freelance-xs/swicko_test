<x-app-auth-layout>
    <x-slot name="bannerTitle">
        my bag
    </x-slot>
    <div class="flex flex-wrap items-center justify-between">
        <div class="flex items-center lg:w-1/2 lg:order-1 order-2">
            <div class="mr-5">
                <img src="{{ asset_url('icon/account/shopping_cart_blue.png') }}" alt="...">
            </div>
            <h2 class="uppercase font-bold"> ITEMS IN YOUR BAG</h2>
        </div>
        <div class="lg:w-1/2 w-full lg:order-2 order-1 mb-5 lg:mb-0">
            <img class="w-full" src="{{ asset_url('icon/account/mybag_step01.png') }}" alt="...">
        </div>
    </div>
    <div class="mt-10">
        <div class="bg-dark-grey p-5 ">
            <div class="flex">
                <div style="width: 22px;" class="mr-5">
                    <img class="" src="{{ asset_url('icon/file_text_data.png') }}" alt="...">
                </div>
                <h4 class="uppercase text-white">my bag</h4>
            </div>
    </div>
        <div class="bg-grey-100 p-5">
            @if ($items->isEmpty())
                You currently have no items in your bag.
            @else
            @endif
        </div>
    </div>
    <x-slot name="footerContent">
    </x-slot>
</x-app-auth-layout>
