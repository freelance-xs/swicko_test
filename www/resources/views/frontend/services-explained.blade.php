<x-guest-layout>

    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}">
        <x-slot name="preTitle">customer <br>information</x-slot>
        <x-slot name="mainTitle">
            service <br> explained
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
    </x-hero>

    <div class="container relative mx-auto py-10" style="min-height: 400px;">
        <div class="md:-mx-10 relative">
            <div class="md:w-1/2 float-left md:px-10">
                @if (site_name() === 'swico')
                    <h2 class="font-bold mb-5 uppercase">WHAT A BATTERY REPLACEMENT & SERVICE INVOLVES</h2>
                    <h4 class=" mb-5 text-grey-200">Watches are used every day to help us organize our time and daily activities. Over time batteries in quartz watch movements will become flat and need replacing. You may also notice that your watch functions don’t work as they are intended or the watches time keeping has become erratic. </h4>
                    <p>
                        To assess if your watch requires a Battery Replacement & Reseal or a Full Service please send your watch to us to be assessed by one of our experts.
                    </p>
                @elseif( site_name() === 'swissmade' )
                    <h2 class="font-bold mb-5 uppercase">
                        WHAT A BATTERY REPLACEMENT & SERVICE INVOLVES
                    </h2>
                    <h4 class=" mb-5 text-grey-200">
                        Watches are used every day to help us organize our time and daily activities. Over time batteries in quartz watch movements will become flat and need replacing. You may also notice that your watch functions don’t work as they are intended or the watches time keeping has become erratic.
                    </h4>
                    <p>
                        To assess if your watch requires a Battery Replacement & Reseal or a Full Service please send your watch to us to be assessed by one of our experts.
                    </p>
                @endif
            </div>
            <div class="md:w-1/2 md:px-10 float-right">
                <div>
                    <img class="mb-5 object-cover w-full" src="{{ asset_url('image/service_explained.jpg') }}" alt="..." style="height: 500px;">
                </div>
            </div>
        </div>
    </div>

    <div class="bg-grey-100 py-10 lg:py-24 ">
        <div class="container mx-auto clear-both">
            <h2 class="uppercase font-bold my-10">quartz watches</h2>

            <x-accordion.accordion>

                <x-accordion.accordion-item class="bg-white" index="0" id="x0">
                    <x-slot name="trigger">
                        <span class="font-bold">RECOMMENDATION:</span> BATTERY REPLACEMENT
                    </x-slot>
                    <div>
                        If possible batteries should be replaced every eighteen months or when the watch starts to lose time. To protect the moment, spent batteries must not be left as they may leak and damage the movement, necessitating a full service.
                    </div>
                </x-accordion.accordion-item>

                <x-accordion.accordion-item class="bg-white" index="2" id="x2">
                    <x-slot name="trigger">
                        BATTERY REPLACEMENT
                    </x-slot>
                    <div>
                        If possible batteries should be replaced every eighteen months or when the watch starts to lose time. To protect the moment, spent batteries must not be left as they may leak and damage the movement, necessitating a full service.
                    </div>
                </x-accordion.accordion-item>

                <x-accordion.accordion-item class="bg-white" index="3" id="x3">
                    <x-slot name="trigger">
                        <span class="font-bold">RECOMMENDATION:</span> QUARTZ WATCHES
                    </x-slot>
                    <div>
                        To ensure the longevity of your time piece we recommend that Quartz Watches are serviced every 2 ½ to 3 years. This period, of course, varies considerably from wearer to wearer.
                    </div>
                </x-accordion.accordion-item>

                <x-accordion.accordion-item class="bg-white" index="4" id="x4">
                    <x-slot name="trigger">
                        <span class="font-bold">FULL SERVICE</span>
                    </x-slot>
                    <div>
                        <p class="font-bold text-base mb-5">This is the procedure our watchmakers will perform when carrying out a service:</p>
                        <div class="grid grid-cols-2 md:grid-cols-4 gap-4">
                            <div>
                                <p><span class="text-primary font-bold text-lg">-</span> Remove the quartz movement from the case</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Remove the dial and hands from the movement and inspect for damage</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Strip the movement, separating electronic and mechanical parts</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Clean the mechanical parts in an ultrasonic machine</p>
                            </div>
                            <div>
                                <p><span class="text-primary font-bold text-lg">-</span> Check the mechanical parts for wear and replace where necessary</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Reassemble and lubricate all mechanical parts</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Check electronics and replace faulty parts where necessary</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Reassemble the complete movement and fit new cell/battery</p>
                            </div>
                            <div>
                                <p><span class="text-primary font-bold text-lg">-</span> Inspect reassembled movement</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Refit the dial and hands</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Clean case in the ultrasonic tank using a non-corrosive solution</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Air vacuum case to remove any residue</p>
                            </div>
                            <div>
                                <p><span class="text-primary font-bold text-lg">-</span> Refit movement, dial and hands into case - replace seals where necessary</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Timepiece checked in special apparatus for water resistance</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Test the movement on timing machine for accuracy</p>
                                <p><span class="text-primary font-bold text-lg">-</span> Finally inspect watch after 48 hours</p>
                            </div>
                        </div>
                        @if (site_name() === 'swico')
                            <x-info>Our Full-Service is guaranteed for 2 years</x-info>
                        @elseif( site_name() === 'swissmade' )
                            <x-info>Our Full-Service is guaranteed for 1 year</x-info>
                        @endif
                    </div>
                </x-accordion.accordion-item>


            </x-accordion.accordion>
        </div>
    </div>

    <div class="bg-white py-10 lg:py-24 ">
        <div class="container mx-auto clear-both">
            <h2 class="uppercase font-bold mb-10">Mechanical/Automatic Watches</h2>

            <x-accordion.accordion>

                <x-accordion.accordion-item class="bg-grey-100" index="0" id="x0">
                    <x-slot name="trigger">
                        <span class="font-bold">RECOMMENDATION:</span> AUTOMATIC WATCHES
                    </x-slot>
                    <div class="text-base">
                        <p class="font-bold">Automatic Chronograph/Chronometer</p>
                        <p>This watch movement is wound by the active movement of the wearer’s wrist.</p>
                        <p>An automatic watch should be worn ‘actively’ and continuously for at least 12 hours a day.</p>
                        <p>
                            If the wearer is inactive or takes the watch off regularly the reserve will start to run down and eventually the watch will start to lose time and stop.
                        </p>

                        <p class="font-bold">Automatic Chronograph/Chronometer</p>
                        <p>
                            What to do if an automatic watch starts to lose time?
                        </p>
                        <p>
                            Remove the watch from the wrist and wind it carefully between six and ten times depending on usage i.e. more winds for less wear.
                        </p>
                        <p>
                            To ensure the longevity of your time piece we recommend that mechanical/automatic watches are serviced every 2 years. This period, of course, varies considerably from wearer to wearer.
                            If possible batteries should be replaced every eighteen months or when the watch starts to lose time. To protect the moment, spent batteries must not be left as they may leak and damage the movement, necessitating a full service.
                        </p>
                    </div>
                </x-accordion.accordion-item>

                <x-accordion.accordion-item class="bg-grey-100" index="1" id="x1">
                    <x-slot name="trigger">
                        <div class="font-bold">FULL SERVICE</div>
                    </x-slot>
                    <div>
                        This is the procedure our watchmakers will perform when carrying out a service on a automatic/mechanical watch:

                        <ul class="list-disc pl-5">
                            <li> Remove the movement from case. </li>
                            <li> Remove the dial and hands from the movement and inspect for damage. </li>
                            <li> Disassemble the movement and check the mechanical parts.</li>
                            <li> Clean the parts in an ultrasonic machine.</li>
                            <li> Check all mechanical parts for wear and replace where necessary.</li>
                            <li> Reassemble the movement and lubricate mechanical parts.</li>
                            <li> Check the balance wheel and hair spring.</li>
                            <li> Check the accuracy of the movement on a timing machine.</li>
                            <li> Refit the dial and hands.</li>
                            <li> Clean the case or case/bracelet in an ultrasonic tank.</li>
                            <li> Fit a new glass when required or requested.</li>
                            <li> Air vacuum the case to remove any residue.</li>
                            <li> Refit the movement, dial and hands into the case.</li>
                            <li> Check seals and replace where necessary.</li>
                            <li> Timepiece checked in special apparatus for water resistance.*</li>
                            <li> Test watch once again on timing machine for accuracy.</li>
                            <li> Automatic watches put on 24 hour electronic winding machine.</li>
                            <li> Finally inspect after 48 hours.</li>
                            <li> * Applicable only to water resistant models</li>
                        </ul>
                        <p class="italic my-5">* Applicable only to water resistant models</p>
                        @if (site_name() === 'swico')
                            <x-info>Our Full-Service is guaranteed for 2 years</x-info>
                        @elseif( site_name() === 'swissmade' )
                            <x-info>Our Full-Service is guaranteed for 1 year</x-info>
                        @endif
                    </div>
                </x-accordion.accordion-item>
            </x-accordion.accordion>
        </div>
    </div>

    <x-swico.services-grid />

    @switch(site_name())
        @case('swissmade')
        <x-watch-brands mode="light" :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
        @break
        @case('swico')
        <x-watch-brands mode="light" :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
        @break
        @default
    @endswitch

</x-guest-layout>
