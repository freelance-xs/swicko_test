<x-guest-layout>
    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}" background-overlay="{{ asset_url('logo/logo_white.png') }}">
        <x-slot name="preTitle">
            customer <br> information
        </x-slot>
        <x-slot name="mainTitle">
            Privacy <br> Policy
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
        <x-slot name="rightColumn">
            <div class="flex justify-end items-end"><img src="{{ asset_url('logo/logo_swico_white_small.png') }}"></div>
        </x-slot>
    </x-hero>

    <x-layout.page-section contain>
        <h2 class="font-bold uppercase mb-5">PRIVACY POLICY</h2>

        <p>
            We operate a comprehensive policy for privacy and security. Our aim is to store the minimal amount of information possible to allow you to make purchases.
        </p>
        <p><b>PROTECTING YOUR PRIVACY</b></p>
        <p>
            At SWICO, we are 100% committed to protecting the privacy and security of our customers and site visitors. We totally appreciate and respect how important privacy is.
        </p>
        <hr>
        <p><b>How We Use Your Information</b></p>
        <p>
            We use your information in a number of different ways - what we do depends on the information. The tables below set this out in detail, showing what we do, and why we do it.
        </p>
        <br>
        <p><strong>Your name and contact details</strong></p>
        <table>
            <tr>
                <th>
                    How we use your name &amp; contact details
                </th>
                <th>
                    Why?
                </th>
            </tr>

            <tr>
                <td>Deliver your purchases to you</td>
                <td>We've got to do this to perform our contract with you</td>
            </tr>
            <tr>
                <td>
                    Send you service messages e-mail
                </td>
                <td>
                    We've got to do this to perform our contract with you, so you can tell when your order is coming
                </td>
            </tr>
            <tr>
                <td>
                    Sending you information by email about our new products and services
                </td>
                <td>
                    To keep you up to date. We only send this with your permission
                </td>
            </tr>
            <tr>
                <td>
                    Fraud prevention and detection
                </td>
                <td>
                    To prevent and detect fraud against either you or us - unfortunate, but absolutely essential
                </td>
            </tr>
            <tr>
                <td>
                    Showing you SWICO adverts as your browse the web
                </td>
                <td>
                    So you can see our latest products and deals
                </td>
            </tr>
            <tr>
                <td>
                    Finding out what you, and other customers, like
                </td>
                <td>
                    To ensure we are giving you what you want, and to stay ahead of the competition
                </td>
            </tr>

        </table>
        <br>
        <p><strong>Your payment information</strong></p>
        <p>
            This means your card's security/CVV code, don't worry, we don't keep them.
        </p>
        <table>

            <tr>
                <th>
                    How we use your payment information
                </th>
                <th>
                    Why?
                </th>
            </tr>


            <tr>
                <td>
                    Take payment, and give refunds
                </td>
                <td>
                    We've got to do this to perform our contract with you.
                </td>
            </tr>
            <tr>
                <td>
                    Fraud prevention and detection
                </td>
                <td>
                    To prevent and detect fraud against either you or us - unfortunate, but absolutely essential
                </td>
            </tr>

        </table>
        <br>
        <p><strong>Purchase history and saved items</strong></p>
        <p>
            What you've bought and what you've stored in your basket for another time.
        </p>
        <table>

            <tr>
                <th>
                    How we use your purchase history and saved items
                </th>
                <th>
                    Why?
                </th>
            </tr>


            <tr>
                <td>
                    Sell you things
                </td>
                <td>
                    We've got to do this to perform our contract with you - if you can't put stuff into a basket, you can't buy it!
                </td>
            </tr>
            <tr>
                <td>
                    Provide customer service and support, and handle returns
                </td>
                <td>
                    We've got to do this to perform our contract with you
                </td>
            </tr>
            <tr>
                <td>
                    Finding out what you, and other customers, like
                </td>
                <td>
                    To ensure we are giving you what you want, and to stay ahead of the competition
                </td>
            </tr>

        </table>
        <br>
        <p><strong>Information about your phone or laptop, and how you use our website</strong></p>
        <p>
            Information you give us when you browse our site, including your IP address and device type as well as how you use our website.
        </p>
        <table>

            <tr>
                <th>
                    How we use information about your phone or laptop, and how you use our website
                </th>
                <th>
                    Why?
                </th>
            </tr>


            <tr>
                <td>
                    Improve our website and set default options for you (such as language and currency)
                </td>
                <td>
                    To give you the best possible shopping experience
                </td>
            </tr>
            <tr>
                <td>
                    Protect our website
                </td>
                <td>
                    To prevent and detect fraud against either you or us - unfortunate, but absolutely essential - and to meet our legal obligations about looking after your data
                </td>
            </tr>

        </table>
        <hr>
        <p>Sharing Your Information</p>
        <p>
            We do not, and will not, sell any of your personal data to any third party - including your name, address, email address or credit card information. We want to earn and maintain your trust, and we believe this is absolutely essential in order do that.
        </p>
        <p>
            However, we share your data with the following categories of companies as an essential part of being able to provide our services to you, as set out in this statement:
        </p>
        <p>
        </p>
        <ul>
            <li>Companies that do things to get your purchases to you, such as payment service providers, warehouses, order packers, and delivery companies</li>
            <li>Professional service providers, such as marketing agencies, advertising partners and website hosts who, who help us run our business</li>
            <li>Credit reference agencies, law enforcement and fraud prevention agencies, so we can help tackle fraud</li>
            <li>Companies approved by you, such as social media sites (if you choose to link your accounts to us)</li>
        </ul>
        <p></p>
        <p>
            We may provide third parties with aggregated but anonymised information and analytics about our customers and, before we do so, we will make sure that it does not identify you.
        </p>
        <hr>
        <p>Marketing Messages</p>
        <p>
            If you have said we can, we'll send you marketing messages by email to keep you aware of what we're up to and to help you see and find our products.
        </p>
        <br>
        <p><strong>How to stop marketing messages from us</strong></p>
        <p>
            You can stop receiving marketing messages from us at any time.
        </p>
        <p>
            You can do this:
        </p>
        <p>
        </p>
        <ul>
            <li>Through your account settings</li>
            <li>By clicking on the 'unsubscribe' link in any email<br>Once you do this, we will update your profile to ensure that you don't receive further marketing messages.</li>
        </ul>
        Stopping marketing messages will not stop service communications (such as order updates).
        <p></p>
        <hr>
        <p>Seeing Adverts for SWICO Online</p>
        <p>
            We also engage in online advertising, also to keep you aware of what we're up to and to help you see and find our products.
        </p>
        <p>
            Like many companies, we target banners and ads to you when you are on other websites and apps. We do this using a variety of digital marketing networks and ad exchanges, and we use a range of advertising technologies like web beacons, pixels, ad tags, cookies, and mobile identifiers, as well as specific services offered by some sites and social networks, such as Facebook's Custom Audience service.
        </p>
        <p>
            The banners and ads you see will be based on information we hold about you, or your previous use of our website or on SWICO banners or ads you have previously clicked on.
        </p>
        <hr>
        <p><b>Keeping Your Information</b></p>
        <p>
            We'll hold on to your information for as long as you have your account, or as long as is needed to be able to provide the services to you, or (in the case of any contact you may have with our Customer Care team) for as long as is necessary to provide support-related reporting and trend analysis only.
        </p>
        <p>
            If reasonably necessary or required to meet legal or regulatory requirements, resolve disputes, prevent fraud and abuse, or enforce our terms and conditions, we may also keep hold of some of your information as required, even after you have closed your account or it is no longer needed to provide the services to you.
        </p>
        <hr>
        <p><b>Your Rights</b></p>
        <p>
            You have a lot of rights relating to your personal information:
        </p>
        <p>
        </p>
        <ul>
            <li>The right to be informed about how your personal information is being used</li>
            <li>The right to access the personal information we hold about you</li>
            <li>The right to request the correction of inaccurate personal information we hold about you (although you can do most of this through My Account)</li>
            <li>The right to request that we delete your data, or stop processing it or collecting it, in some circumstances</li>
            <li>The right to stop direct marketing messages, which you can do through My Account, and to withdraw consent for other consent-based processing at any time</li>
            <li>The right to complain to your data protection regulator - in the UK, the Information Commissioner's Office</li>
        </ul>
        <p></p>
        <p>
            If you want to exercise your rights, have a complaint, or just have questions, please contact us.
        </p>
        <hr>
        <p><b>Cookies</b></p>
        <p>
            We use cookies on our website. For more information on cookies, please see our <a href="{{route('cookies')}}">cookie notice</a>.
        </p>
        <hr>

    </x-layout.page-section>
</x-guest-layout>
