<x-guest-layout>
    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}" background-overlay="{{ asset_url('logo/logo_white.png') }}">
        <x-slot name="preTitle">
            customer <br> information
        </x-slot>
        <x-slot name="mainTitle">
            term & <br> conditions
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
        <x-slot name="rightColumn">
            <div class="flex justify-end items-end"><img src="{{ asset_url('logo/logo_swico_white_small.png') }}"></div>
        </x-slot>
    </x-hero>

    <x-layout.page-section contain>
        <h2 class="font-bold uppercase mb-5">terms & conditions</h2>

        <p><strong>Application of these Conditions</strong></p>
        <p>
            Please understand that if you refuse to accept these Conditions, you will not be able to process your Services/Repairs/Products from the Website.
        </p>
        <p>
            When using the website, you accept that communication with us will be mainly electronic. We will contact you by email or provide you with information by posting notices on the website. For contractual purposes, you agree to this electronic means of communication and you acknowledge that all contracts, notices, information and other communications that we provide to you electronically comply with any legal requirement that such communications be in writing. This condition does not
            affect your statutory rights.
        </p>
        <hr>
        <p><strong>Changes</strong></p>
        <p>
            We may change these conditions from time to time without notice to you. Changes will apply to any subsequent Orders received. It is your responsibility to check regularly to see if any changes have been made to these conditions. We reserve the right to modify or withdraw this website at any time without liability to You.
        </p>
        <hr>
        <p><strong>Queries</strong></p>
        <p>
            Please contact us if you have any questions relating to these conditions or ordering Services/Repairs/Products generally by using the <a href="/Home/Contact">contact us</a> page.
        </p>
        <hr>
        <p><strong>Your Account</strong></p>
        <p>
            You are responsible for maintaining the confidentiality of the password chosen by you when you register on the website and for restricting access to your computer and/or email address to prevent unauthorised access to your account.
        </p>
        <hr>
        <p><strong>Description and Availability</strong></p>
        <p>
            We take all reasonable measures to ensure that details, measurements, and descriptions of Services/Repairs/Products given on this website are as accurate and complete as possible but there may be variations between these and the Services/Repairs/Products themselves. All Services/Repairs/Products are subject to part availability; We undertake stock takes to ensure the greatest accuracy in our stock levels, however if any Services/Repairs/Products on your order is unavailable we will
            notify you as soon as possible and refund your card.
        </p>
        <hr>
        <p><strong>Price</strong></p>
        <p>
            Prices appearing on this website are inclusive of sales tax (where applicable) at the current rates. The cost of your order will be the price of the Services/Repairs/Products ordered.
        </p>
        <p>
            Please note that all prices on this website are for online purchases only.
        </p>
        <p>
            Due to circumstances beyond our control occasionally prices may have to be altered. However, a customer is always charged with current price at any said time of placing their order and a price reduction (if happen) does not automatically entitle the customer for a refund for such price difference.
        </p>
        <p>
            Due to a technical error (or any other error beyond SWICO's control) if (and only if) a customer is charged with wrong price, either party will have the right to cancel the order if needed.
        </p>
        <hr>
        <p><strong>Manufacturer's Warranty Claims</strong></p>
        <p>
            If you have adhered to the manufacturing terms and the watch has been purchased from an authorised dealer and is covered by a valid warranty, we will repair manufacturing defects <u>free of charge</u>. The watch must be accompanied with its original warranty card or proof of purchase. The warranty becomes void in the event that any other persons attempts to repair the claimed defect. Batteries are covered under the warranty for the first year only from the date of purchase.
        </p>
        <hr>
        <p><strong>Placing an Order</strong></p>
        <p>
            You must not order Services/Repairs/Products via this website if you are younger than 16 years of age. All orders are subject to acceptance by us and are subject to availability. After placing an order, you will receive an email from us acknowledging that we have received your order. Please note that this does not mean that your order has been accepted. Your order constitutes an offer to us to buy a Service/Repair/Product. All orders are subject to acceptance by us and we will
            confirm such acceptance by sending you an email that confirms your request.
        </p>
        <p>
            Please ensure if you have a sale code, that this is entered and redeemed before completing payment. Please also ensure you have double checked the items in your basket before completing your order. We are unable to apply codes or amend orders retrospectively.
        </p>
        <p>
            Please do not refresh your page during payment, as this can lead to a second payment being taken. If you are worried this may have happened, you can check this in "My Service/Repairs." Please contact us straight away and we will try our best to cancel the duplicate order.
        </p>
        <hr>
        <p><strong>Returning a watch unrepaired</strong></p>
        <p>
            Items that are requested to be returned not repaired, either by way of mutual agreement, or at your request will not be chargeable.
        </p>
        <p>
            We will return your watch in the condition as received. This watch condition assessment is detailed on our assessment online.
        </p>
        <hr>
        <p><strong>Creating an Account/Registering a watch</strong></p>
        <p>
            By creating an online account with us and part of our contract to fulfil our service to you, you give us consent to join our watch care programme and use your personal information to communicate with you about your order and send you future service reminders.
        </p>
        <hr>
        <p><strong>Downtime</strong></p>
        <p>
            You acknowledge and accepts that computer and telecommunications systems are not fault-free and may from time to tome require periods of downtime (being periods during which the website is not available to you) for the purpose of repair, maintenance and upgrading or otherwise.
        </p>
        <p>
            We do not guarantee uninterrupted availability of the website but shall make reasonable efforts to minimise any periods during which the website is not available. You accept that you will have no claim for breach of contract or otherwise in respect of any such period of unavailability.
        </p>
        <hr>
        <p><strong>Intellectual Property</strong></p>
        <p>
            You acknowledge and agree that all copyright, trademarks and all other intellectual property rights in all material or content appearing on this website shall remain at all times vested in us or our licensors. SWICO watch service centre is a registered trademark owned by us.
        </p>
        <hr>
        <p><strong>Payment Methods</strong></p>
        <p>
            There are multiple ways for you to pay for your order.
        </p>
        <p>
            We accept: Visa credit cards; Visa; Mastercard; Maestro; American Express.
        </p>
        <p>
            To pay, simply complete the details when prompted to do so in the checkout section of this Website. You confirm that the card being used is yours. We may undertake searches with credit reference agencies for the purpose of verifying your identity and the personal information you submit as part of an order. To do so the agencies may check your personal Information against any particulars on any database (public or otherwise) to which they have access. They may also use your details in
            the future to assist other companies for verification purposes. By submitting your order you agree to this.
        </p>
        <hr>
        <p><strong>Refunds</strong></p>
        <p>
            Any refunds processed will be made via the same transaction. Please do not send card/bank details to us via email. We do not require this to process your refund.
        </p>
        <hr>

        <p>Delivery Services</p>
        <p><strong>Sending your watch using our free pre-paid postage service</strong></p>
        <p>
            Royal Mail is our chosen delivery service; the packaging and pre-paid label we send you is FREE of charge. Royal Mail or swicowatchservicecentre.co.uk cannot be held responsible if the client has not securely packaged the watch. The usage of this service is at your discretion, if you deem this not to be suitable for your item, please use an alternative postal/courier service.
        </p>
        <hr>
        <p><strong>Delivery</strong></p>
        <p>
            We will notify you when your watch has been despatched. When you receive this notification, we aim to deliver all mainland UK standard delivery within 1 to 4 working days. You can find your tracking details on the despatched notification.
        </p>
        <p>
            Please allow an extra working day for orders sent by Standard Delivery during UK Bank Holidays.
        </p>
        <p>
            Deliveries to The Scottish Highlands, Scottish Islands, the Isle of Wight, Isle of Man, Jersey, and Northern Ireland may take up to two working days longer. UK deliveries are with the exception of those addresses listed under "Delivery Locations".
        </p>
        <p>
            Whilst we will endeavour to deliver to you as soon as possible, we shall not be liable for any failure to make delivery within time estimates stated above.
        </p>
        <hr>
        <p><strong>Lost Parcels</strong></p>
        <p>
            We will begin our missing parcels procedure if your Watch/Product fails to arrive within 28 working of dispatch and you notify us in writing. The above time frames are in accordance with Royal Mails lost parcel policy.
        </p>
        <hr>

        <p>Returns</p>
        <p><b>Incorrect or faulty items</b></p>
        <p>
            We do of course do our very best to ensure that every order we dispatch is correct &amp; of highest quality. However, mistakes unfortunately do happen. Should you receive an incorrect or faulty item, please contact us immediately so that we can authorise your return.
        </p>
        <p>
            For questions regarding returns please email: <a href="mailto:repairs@swico.co.uk">repairs@swico.co.uk</a>
        </p>
        <p>
            Returns Address: SWICO, Meadway, Haslemere, Surrey, GU27 1NN.
        </p>
        <hr>

        <p>Secure Online Shopping</p>
        <p><b>Secure Certificate</b></p>
        <p>
            We believe buying online at swicowatchservicecentre.co.uk is secure. This is because: All payment transactions are carried out over an SSL 128-Bit Encrypted secure connection. We do this using industry standard technology. The padlock symbol, at the bottom of your browser window, confirms this level of encryption. If you click on this, you will see our VeriSign secure certificate, meaning that your data is visible only to Us.
        </p>
        <hr>
        <p><b>Use</b></p>
        <p>
            We only use your personal Information for the following purposes:
        </p>
        <ul>
            <li>Processing your Orders and Service Notifications.</li>
            <li>For statistical purposes to improve this website and its services to you.</li>
            <li>To administer this Website.</li>
            <li>Other use by us to which you agree when asked on this Website.</li>
        </ul>
        <p></p>
        <p>
            When you register on this website you are given the option to receive marketing communications from us by email which we feel may be of interest to you. If You indicate in your agreement to being contacted by us by these means you may subsequently unsubscribe from our contact list at any time by updating the "unsubscribe" directions given in any email received. You can also amend your account profile at any time by updating your Marketing Contact Preference by signing-in and going to
            "My Account".
        </p>
        <p>
            Your Personal Information may be disclosed to other businesses within the group of which we are a part and to reputable third-party contractors engaged by SWICO Group Ltd to perform a variety of functions such as processing your Orders, Service Notifications and assisting with promotions or providing technical services for our websites. We require all such third parties to treat your personal Information as fully confidential and to fully comply with all applicable UK Data Protection
            and consumer legislation. You should be aware that if we are requested by the police or any other regulatory or government authority investigating suspected illegal activities to provide your personal Information and/or user Information, we are entitled to do so.
        </p>
        <hr>
        <p><b>Links to other Sites</b></p>
        <p>
            We may offer links to sites that are not operated by us. If You visit one of these linked sites you should review its privacy and other policies. We are not responsible for the policies and practices of other companies. Questions and concerns regarding this policy and your personal Information should be directed to the following address: SWICO Group Limited, Meadway, Haslemere, Surrey, GU27 1NN.
        </p>
        <hr>

        <p>Privacy And Cookies</p>
        <p>
            Our privacy &amp; Cookies policy, which sets out how we will use your information, can be found <a href="/Static?resource=%2Fabout%2Fprivacy-policy.cshtml&amp;pageTitle=Privacy%20Policy">here</a>. By using this website, you consent to the processing described therein and you warrant that all data provided by you is accurate and up to date. You are responsible for maintaining and promptly updating account information for accuracy and completeness.
        </p>
        <hr>
        <p><b>Satisfaction Guarantee</b></p>
        <p>
            It is important to us that you are delighted with all our service. If You are not completely satisfied with any service you order from us, please contact us within 14 working days of receipt.
        </p>
        <hr>
        <p><b>Disclaimer</b></p>
        <p>
            The information contained in this website is for general information purposes only. The information is provided by swicowatchservicecentre.co.uk and while we endeavor to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any
            purpose. Any reliance you place on such information is therefore strictly at your own risk.
        </p>
        <p>
            In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website. Through this website, you are able to link to other websites which are not under the control of swicowatchservicecentre.co.uk. We have no control over the nature, content and availability of those sites. The inclusion of any
            links does not necessarily imply a recommendation or endorse the views expressed within them.
        </p>
        <p>
            Every effort is made to keep the website up and running smoothly. However, swicowatchservicecentre.co.uk takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control. However, owing to our policy of continuous improvement we reserve the right to change policies without prior notice.
        </p>
        <hr>


    </x-layout.page-section>
</x-guest-layout>
