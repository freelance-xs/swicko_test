<x-guest-layout>
    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}" background-overlay="{{ asset_url('logo/logo_white.png') }}">
        <x-slot name="preTitle">
            customer <br> information
        </x-slot>
        <x-slot name="mainTitle">
            cookies
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
        <x-slot name="rightColumn">
            <div class="flex justify-end items-end"><img src="{{ asset_url('logo/logo_swico_white_small.png') }}"></div>
        </x-slot>
    </x-hero>

    <x-layout.page-section contain>
        <h2 class="font-bold uppercase mb-5">USE OF COOKIES</h2>
        <p>
            We use cookies when you visit our site. There are three main types of cookies - here's how and why we use them.
        </p>
        <br>
        <p>
        </p>
        <ol class="list-decimal list-inside pl-10">
            <li>&nbsp;Site functionality cookies - these cookies allow you to navigate the site and use our features, such as "Add to basket" and "My Wishlist".<br><br></li>
            <li>&nbsp;Site analytics cookies - these cookies allow us to measure and analyse how our customers use the site, to improve both its functionality and your shopping experience.<br><br></li>
            <li>&nbsp;Targeting or advertising cookies - these cookies are used to deliver ads relevant to you. They also limit the number of times that you see an ad and help us measure the effectiveness of our marketing campaigns.<br><br></li>
        </ol>
        <p></p>
        <p>
            By using our site, you agree to us placing these sorts of cookies on your device and accessing them when you visit the site in the future. If you want to delete any cookies that are already on your computer, the "help" section in your browser should provide instructions on how to locate the file or directory that stores cookies. Further information about cookies can be found at https://ico.org.uk/for-organisations/guide-to-pecr/cookies-and-similar-technologies/ . Please note that by
            deleting or disabling future cookies, your user experience may be affected and you might not be able to take advantage of certain functions of our site.
        </p>
        <hr>


    </x-layout.page-section>
</x-guest-layout>
