<x-guest-layout>
    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}" background-overlay="{{ asset_url('icon/account/banner_watch.png') }}">
        <x-slot name="mainTitle">
            <span class="font-bold">{{ site_name() }}</span> </br> watchcare
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
    </x-hero>

    <div class="container mx-auto py-24 relative">
        <div class="md:flex md:-mx-10">
            <div class="md:w-1/2 md:px-10">
                <img src="{{ asset_url('icon/watch_blue.png') }}" class="mb-5" alt="...">
                <h2 class="uppercase font-bold mb-5">REGISTER YOUR WATCH WITH {{ site_name() }} WATCHCARE</h2>
                @if (site_name() === 'swico')
                    <h3 class="text-grey-200 mb-5">Who better to look after your watch than the Watch Manufacturer's Officially Appointed UK Service & Repair Centre.</h3>
                    <p class="mb-10">Get the best possible performance and wear from your watch with {{ site_name() }} WatchCare. Benefit from Lifetime Support, Tailored Service Reminders, Helpful Tips, and Exclusive Offers and Discounts when you register.</p>

                @else
                    <h3 class="text-grey-200 mb-5">
                        Our mission is for you to get the best possible performance and wear from your watch.
                    </h3>
                    <p class="mb-10">
                        Register your watch now for Swissmade WatchCare.
                    </p>
                @endif
            </div>
            <div class="md:w-1/2 md:px-10">
                <img src="{{ site_name() === 'swico' ? asset_url('image/watchcare_info.jpg') : asset_url('image/index_watchcare.jpg') }}" alt="...">
            </div>
        </div>
    </div>

    <div class="bg-grey-100 py-10 md:py-24 relative">
        <img src="{{ asset_url('icon/arrow_down_grey.png') }}" class="absolute -bottom-12 left-20" alt="...">
        <div class="container mx-auto">
            <div class="mb-10">
                @if (site_name() === 'swico')
                    <h2 class="uppercase font-bold mb-5">why become a member ?</h2>
                    <h4 class="text-grey-300 mb-5">We are here to help you get the most from your timepiece through its lifetime.</h4>
                @else
                    <h2 class="uppercase font-bold mb-5">WHY REGISTER MY WATCH FOR WATCHCARE?</h2>
                    <h4 class="text-grey-300 mb-5">Watches are built to last, and its not difficult to properly take care of your watch with SWISSMADE WATCHCARE.</h4>
                @endif
            </div>
            <div class="md:flex md:-mx-10">
                @if (site_name() === 'swico')
                    <div class="md:w-1/2 md:px-10">
                        <p>
                            <span class="font-bold">WELCOME GIFT 10% off your first purchase</span> redeemable against any WatchCare request e.g. Battery Replacement & Reseal, Replacement Strap, Accidental Damage Repair or a Maintenance Service.
                            T&C’s:- one time use code; never expires; not valid with any other discount codes or previous purchases.
                        </p>
                        <p>
                            <span class="font-bold">SUPPORT</span> Get the very best performance from your watch with up-to date information and maintenance tips to help you keep your watch in tip top condition.
                        </p>
                    </div>
                    <div class="md:w-1/2 md:px-10">
                        <p>
                            <span class="font-bold"> WATCHCARE SERVICE REMINDERS </span> Regularly servicing your watch ensures it stays in great condition. We will prompt you when your watch will require a maintenance service to ensure optimum performance.
                        </p>
                        <p>
                            <span class="font-bold">EXCLUSIVE OFFERS AND DISCOUNTS</span> As exclusive members opt into marketing to receive our latest promotions - Watch strap deals, discounts and much more.
                        </p>
                    </div>
                @else
                    <div class="md:w-1/2 md:px-10">
                        <p>
                            <span class="font-bold">SUPPORT</span> 
                            Get up to date information and maintenance tips on your watch to help you get the very best from your watch through its whole lifetime.
                        </p>
                        <p>
                            <span class="font-bold"> WATCHCARE SERVICE REMINDERS </span> Regularly servicing your watch ensures it stays in great condition. We will prompt you when your watch will require a maintenance service to ensure optimum performance.
                        </p>
                    </div>
                    <div class="md:w-1/2 md:px-10">

                        <p>
                            <span class="font-bold">EXCLUSIVE OFFERS AND DISCOUNTS</span> As exclusive members opt into marketing to receive our latest promotions - Watch strap deals, discounts and much more.
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="bg-white py-20 lg:py-24 static">
        <div class="clear-both table"></div>
        <div class="container mx-auto">
            <div class="lg:flex lg:-mx-3">
                <div class="lg:w-4/6 lg:px-3 mb-6">
                    <div class="bg-grey-100 p-5">
                        <x-forms.register-form />
                    </div>
                </div>
                <div class="lg:w-2/6 lg:px-3">
                    <div class="bg-grey-500 p-8 mb-6">
                        <img src="{{ asset_url('icon/profile_white.png') }}" class="mb-5" alt="...">
                        <h2 class="uppercase font-bold leading-8 mb-5">Already registered with {{ site_name() }}?</h2>
                        <x-forms.login-form mode="light" />
                    </div>
                    <div class="bg-dark-grey p-8 mb-6">
                        <img src="{{ asset_url('icon/file_text_data.png') }}" class="mb-5" alt="...">
                        <h2 class="uppercase font-bold text-white leading-8 mb-5">HAVE YOU RECEIVED AN ESTIMATE BY POST?</h2>
                        <p class="text-white text-lg mb-8">Have you received an estimate and registration code in the post? Please complete the online account set up to manage your repair online.</p>
                        <a href="{{ 'estimate-by-post' }}" class="btn btn-primary uppercase">activate</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @switch(site_name())
        @case('swissmade')
        <x-watch-brands mode="dark" :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
        @break
        @case('swico')
        <x-watch-brands mode="dark" :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
        @break
        @default
    @endswitch
</x-guest-layout>
