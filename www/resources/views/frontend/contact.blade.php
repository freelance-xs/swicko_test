<x-guest-layout>

    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}">
        <x-slot name="preTitle">Customer<br>Information</x-slot>
        <x-slot name="mainTitle">
            contact <span class="font-bold">{{ site_name() }}<span class="text-primary ml-2 font-bold">.</span></span>
        </x-slot>
    </x-hero>

    <div class="container mx-auto">
        <div class="mt-5 md:mt-10 flex flex-wrap transform md:translate-y-16 md:-mx-10">
            <div class="w-full md:w-1/2 px-3 md:px-10">
                <h2 class="font-bold text-2xl"><span class="uppercase">{{ site_name() }}</span> Watch Service Centre</h2>
                @if (site_name() === 'swico')
                    <p class="text-2xl mb-10">
                        Meadway, Haslemere, Surrey GU27 1NN
                    </p>
                    <p class="text-primary text-2xl font-bold">+44 (0)1428 656822</p>
                    <p class="">Monday - Friday 9am - 5:30pm</p>
                    <a class="text-primary text-2xl font-bold block mb-10" href="mailto:watchrepairs@swicogroup.com" target="_blank">watchrepairs@swicogroup.com</a>
                    <p><span class="font-bold">Our team of experts are here to help.</span> Please complete our contact form
                        and submit and our customer service team will get back to you.</p>
                @elseif( site_name() === 'swissmade' )
                    <p class="text-2xl mb-10">
                        Unit 21 - 22, Arundel House, 43 Kirby Street, London EC1N 8TE
                    </p>
                    <p class="text-primary text-2xl font-bold">+44 (0)20 7405 8504</p>
                    <a class="text-primary text-2xl font-bold block mb-10" href="mailto:info@swissmade.co.uk" target="_blank">info@swissmade.co.uk</a>

                @endif
                <p>
                    <span class="font-bold">Our team of experts are here to help.</span>
                    Please complete our contact form and submit and our customer service team will get back to you.
                </p>
                <img class="mt-10" src="{{ asset_url('icon/sc_icon_grey_small.png') }}" alt="...">
            </div>
            <div class="mt-10 md:mt-0 w-full md:w-1/2 md:px-10 ">
                <div class="bg-grey-100 py-10 px-3">
                    <x-forms.contact-form button-text="submit"></x-forms.contact-form>
                </div>
            </div>
        </div>
    </div>
    <div>
        @if (site_name() === 'swico')
            <x-gmap width="100%" height="500" />

        @elseif( site_name() === 'swissmade' )
            <x-gmap width="100%" height="500" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2482.640235237016!2d-0.107511!3d51.519816!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761b4dde712067%3A0x2b91056ddf267d73!2sArundal%20House%2C%20Holborn%2C%20London%2C%20UK!5e0!3m2!1sen!2sus!4v1615769092783!5m2!1sen!2sus" />

        @endif
    </div>

    <x-swico.services-grid />

    @if (site_name() === 'swico')
        <x-swico.swissmade />
    @elseif( site_name() === 'swissmade' )

    @endif

</x-guest-layout>
