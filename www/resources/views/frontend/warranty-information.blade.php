<x-guest-layout>
    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}" background-overlay="{{ asset_url('logo/logo_white.png') }}">
        <x-slot name="preTitle">customer <br>information</x-slot>
        <x-slot name="mainTitle">
            warranty <br> information
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
        <x-slot name="rightColumn">
            <div class="flex justify-end items-end"><img src="{{ asset_url('logo/logo_swico_white_small.png') }}"></div>
        </x-slot>
    </x-hero>

    <div class="container relative mx-auto py-10" style="min-height: 400px;">
        <div class="md:-mx-10 relative">
            <div class="md:w-1/2 float-left md:px-10">
                <h2 class="font-bold mb-5 uppercase">MANUFACTURERS WARRANTY</h2>
                <h4 class=" mb-5 text-grey-200">Your watch manufacturer provides a 2 year manufacturer’s warranty from date of purchase.</h2>
                    <p class="mb-10">
                        The limited warranty is valid providing it was purchased from an Authorised Dealer, as evidenced by a valid warranty card or proof of purchase. To be valid, the warranty card must be duly completed, dated and stamped by the Authorised Dealer.
                    </p>
                    <img src="{{ asset_url('image/warranty_watch.png') }}" alt="...">
            </div>
            <div class="md:w-1/2 md:px-10 float-right">
                <div>
                    <img class="mb-5 object-cover w-full" src="{{ asset_url('image/warranty_main.jpg') }}" alt="..." style="height: 500px;">
                </div>
            </div>
        </div>
    </div>

    <div class="bg-grey-100 py-10 lg:py-24 ">
        <div class="container mx-auto pt-20">
            <div class="clear-both"></div>
            <div class="md:flex md:-mx-10">
                <div class="md:w-1/2 md:px-10">
                    <p>
                        This limited warranty covers the defects in material or resulting from the manufacturing of the watch. The brands authorised service centre will repair or replace the movement of your watch free of charge if such movement proves to be defective in material or workmanship under normal use. Excluded from this limited warranty of the manufacturer:
                    </p>
                    <p><span class="text-primary font-bold text-lg">-</span> Batteries, strap, cases or crystals.</p>

                    <p><span class="text-primary font-bold text-lg">-</span> Damage resulting from an accident, normal wear, negligence, misuse, abuse or from the intervention of any person authorised by the manufacturer.</p>

                    <p><span class="text-primary font-bold text-lg">-</span> Loss and theft.</p>
                    <p><span class="text-primary font-bold text-lg">-</span> Incidental or consequential damage arising from the failure or malfunction of the watch as well as from any inappropriate usage, lack of precision, malfunction or lack of conformity of the watch.</p>
                </div>
                <div class="md:w-1/2 md:px-10 flex justify-between">
                    <div class="w-4/6">
                        <p>
                            The warranty becomes void in the event that any person not duly authorised by the manufacturer attempts to repair the claimed defect.
                        </p>
                        <p>
                            Should you require work under this warranty, please send your watch by insured mail and in a carefully wrapped package supplied by SWICO free of charge. This can be requested from the ‘send your watch’ section on our website. The watch must be accompanied by the duly completed warranty card.
                        </p>
                    </div>
                    <div class="w-1/6">
                        <img src="{{ asset_url('icon/sc_icon_grey_small.png') }}" class="w-full" alt="...">
                    </div>
                </div>
            </div>
        </div>
    </div>



    <x-split-section image="image/warranty_booklet.jpg">
        <div>
            <h2 class="mb-5 uppercase font-bold">{{site_name()}} SERVICE WARRANTY</h2>

            <h4 class="text-grey-300 mb-5">
                The {{site_name()}} service guarantee is valid for 12 or 24 months as specified on the service guarantee that was returned with the watch.

            </h4>

            <p>
                It covers any defect arising from the work undertaken in our workshops, relevant to the service. It does not cover batteries, strap, cases or crystals, nor does it cover damage arising from normal wear, scratches, accidents or misuse, or from any alteration. If any fault develops which is covered by this guarantee, please return the watch with the SWICO service guarantee, by insured mail and in carefully wrapped package supplied by SWICO free of charge. This can be requested
                from
                the
                ‘send your watch’ section on our website.
            </p>
        </div>
    </x-split-section>

    <x-swico.services-grid />

    @switch(site_name())
        @case('swissmade')
        <x-watch-brands :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
        @break
        @case('swico')
        <x-watch-brands :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
        @break
        @default
    @endswitch
</x-guest-layout>
