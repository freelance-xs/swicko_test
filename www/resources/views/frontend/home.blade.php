<x-guest-layout>


    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}" background-overlay="{{ asset_url('logo/logo_white.png') }}">
        <x-slot name="preTitle">
            @switch(site_name())
                @case('swissmade')
                ONLINE WATCH REPAIRS <br> A FREE SECURE POSTAL SERVICE
                @break
                @case('swico')
                Watch Repairs By Post<br>A Free Secure Postal Service
                @break
                @default
            @endswitch
        </x-slot>
        <x-slot name="mainTitle">
            <span class="font-bold">SWICO.</span>The Official <br>UK Service Center <br>For Your Watch
            @if (site_name() === 'swissmade')
                <div>
                    <div class="mt-10 ">
                        <p class="text-base text-primary flex">
                            <span class="mr-3">
                                <img class="img-responsive" src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                            </span>
                            Online portal to manage request
                        </p>
                        <p class="text-base text-primary flex">
                            <span class="mr-3">
                                <img class="img-responsive" src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                            </span>
                            Free secure postal return service
                        </p>
                        <p class="text-base text-primary flex">
                            <span class="mr-3">
                                <img class="img-responsive" src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                            </span>
                            Manufacturers Warranty Claims for Ebel and Baume &amp; Mercier
                        </p>
                        <p class="text-base text-primary flex">
                            <span class="mr-3">
                                <img class="img-responsive" src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                            </span>
                            Quick Turnaround
                        </p>
                    </div>
                </div>
            @endif
        </x-slot>
        <x-slot name="extraContent">
            <x-cta.box-wrapper class="hidden md:flex items-end">
                <x-cta.box class="md:w-1/3">
                    <x-slot name="icon">
                        <img src="{{ asset_url('icon/tick_shield.png') }}" alt="shield">
                    </x-slot>
                    <h3 class="text-white text-2xl mt-5 mb-3 uppercase">Warranty Claims</h3>
                    <p class="mb-16">
                        @switch(site_name())
                            @case('swissmade')
                            If you have a Baume & Mercier or Ebel watch covered by a valid manufacturers warranty and you have an aftersales enquiry, we're here to help.
                            @break
                            @case('swico')
                            If your watch is covered by a valid manufacturers warranty and you have an aftersales enquiry, we’re here to help.
                            @break
                            @default
                        @endswitch
                    </p>

                    <x-slot name="button">
                        <a class="btn btn-black uppercase w-full text-center" href="{{ route('warranty-claim') }}">Start
                            Your Claim</a>
                    </x-slot>
                </x-cta.box>
                <x-cta.box class="md:w-1/3" bg-color="bg-white">
                    <x-slot name="icon">
                        <img src="{{ asset_url('icon/settings.png') }}" alt="settings">
                    </x-slot>
                    <h3 class="text-black text-2xl mt-5 mb-3 uppercase">SERVICES, REPAIRS, STRAPS</h3>
                    <p class="mb-16">Is your watch out of warranty and need a service, strap or a new lease of life?
                        Take advantage of our free estimates and postage.</p>
                    <x-slot name="button">
                        <a class="btn btn-grey uppercase w-full text-center" href="{{ route('service-repair') }}">Start
                            Your Repair</a>
                    </x-slot>
                </x-cta.box>
                <x-cta.box class="md:w-1/3" bg-color="bg-grey-500">
                    <x-slot name="icon">
                        <img src="{{ asset_url('icon/watch.png') }}" alt="watch">
                    </x-slot>
                    <h3 class="text-white text-2xl mt-5 mb-3 uppercase"><span class="font-bold">SWICO</span> WATCHCARE
                    </h3>
                    <p class="mb-16">Register your watch today to get free service reminders and exclusive watch care
                        offers and discounts.</p>
                    <x-slot name="button">
                        <a class="btn btn-primary uppercase w-full text-center" href="{{ route('warranty-claim') }}">Register
                            A Watch</a>
                    </x-slot>
                </x-cta.box>
            </x-cta.box-wrapper>
        </x-slot>
    </x-hero>

    <div class="container mx-auto px-4">
        <div class="py-6 md:py-20 flex flex-wrap -mx-4">
            <div class="w-full md:w-1/2 px-4">
                <h2 class="text-3xl uppercase font-bold tracking-tight mb-3">Welcome to <span class="font-extrabold">{{ strtoupper(site_name()) }}</span>
                    <banner-bluedot class="text-primary text-base">●</banner-bluedot>
                </h2>
                <h3 class="text-grey-500 text-xl mb-3">
                    @switch(site_name())
                        @case('swissmade')
                        Specialists in high quality repair, maintenance and restoration of fine Swiss watches from the heart of London’s Hatton Garden since 1985.
                        @break
                        @case('swico')
                        SWICO the Official UK Service & Repair Centre for our WATCH BRANDS' timepieces, providing warranty or non-warranty repair and maintenance for watches to the
                        highest standards.
                        @break
                        @default
                    @endswitch
                </h3>
                <p>Dedicated to providing a fast, first class service, we have created a convenient online portal to
                    access our Service Centre from anywhere in the UK using our FREE secure postal service.</p>
                <div class="flex flex-wrap space-y-4 md:space-y-0 my-8">
                    @switch(site_name())
                        @case('swissmade')
                        <a class="btn btn-black uppercase md:mr-4 w-full text-center md:w-auto" href="{{ route('about') }}">About swissmade</a>
                        <a class="btn btn-primary uppercase w-full text-center md:w-auto" href="{{ route('testimonials') }}">Testimonials</a>
                        @break
                        @case('swico')
                        <a class="btn btn-primary uppercase md:mr-4 w-full text-center md:w-auto" href="{{ route('about') }}">Why choose us?</a>
                        <a class="btn btn-grey uppercase w-full text-center md:w-auto" href="{{ route('testimonials') }}">Testimonials</a>
                        @break
                        @default
                    @endswitch
                </div>
            </div>
            <div class="w-full md:w-1/2 px-4">
                @switch(site_name())
                    @case('swissmade')
                    <img src="{{ asset_url('image/hompage_about.jpeg') }}" alt="...">

                    @break
                    @case('swico')
                    <img src="{{ asset_url('image/1502463_181007_HugoBoss_D3_S02_closeups_033_F3.jpg') }}" alt="...">

                    @break
                    @default
                @endswitch

            </div>
        </div>
    </div>

    @switch(site_name())
        @case('swissmade')
        <x-watch-brands :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
        @break
        @case('swico')
        <x-watch-brands :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
        @break
        @default
    @endswitch

    <x-split-section image="{{ site_name() === 'swissmade' ? 'image/jose-carrasco-437682-unsplash.jpg' : 'image/index_repairs_by_post.jpg' }}">
        <div>
            <div class="mb-6">
                <img src="{{ asset_url('icon/mail.png') }}" alt="...">
            </div>
            <h2 class="text-3xl uppercase font-bold tracking-tight mb-3">Watch Repairs by Post</h2>
            <p class="text-grey-500 mb-6">Sending your watch to us is easy and safe thanks to our free secure postal
                service. Our convenient online portal keeps you updated at each stage.</p>
            <div class="flex">
                <a class="btn btn-primary uppercase w-full text-center md:w-auto" href="{{ route('repairs-by-post') }}">How
                    does it work?</a>
            </div>
        </div>
    </x-split-section>

    <x-split-section image="{{ 'image/index_watchcare.jpg' }}" :left="true" :bg_color="'grey-100'">
        <div>
            <div class="mb-6">
                <img src="{{ asset_url('icon/watch_blue.png') }}" alt="...">
            </div>
            <h2 class="text-3xl uppercase font-bold tracking-tight mb-3">{{ site_name() }}</h2>
            <p class="text-grey-500 mb-6">Register your watch today to get free service reminders and exclusive watch
                care offers and discounts.</p>
            <div class="flex">
                <a class="btn btn-primary uppercase w-full text-center md:w-auto" href="{{ route('watch-care') }}">Register
                    a Watch</a>
            </div>
        </div>
    </x-split-section>

    <x-split-section image="{{ site_name() === 'swissmade' ? 'image/dial.jpg' : 'image/index_warranty.jpg' }}">
        <div>
            <div class="mb-6">
                <img src="{{ asset_url('icon/tick_shield_blue.png') }}" alt="...">
            </div>
            <h2 class="text-3xl uppercase font-bold tracking-tight mb-3">WARRANTY CLAIMS</h2>
            <p class="text-grey-500 mb-6">If your watch is covered by a valid warranty and you have an aftersales
                enquiry, we're here to help.</p>
            <div class="flex">
                <a class="btn btn-primary uppercase w-full text-center md:w-auto" href="{{ route('warranty-claim') }}">Start
                    Warranty Claim</a>
            </div>
        </div>
    </x-split-section>

    <x-split-section image="{{ site_name() === 'swissmade' ? 'image/Ebelmovement.jpeg' : 'image/index_service_repair.jpg' }}" :left="true" :bg_color="'grey-100'">
        <div>
            <div class="mb-6">
                <img src="{{ asset_url('icon/settings.png') }}" alt="...">
            </div>
            <h2 class="text-3xl uppercase font-bold tracking-tight mb-3">SERVICES, REPAIRS, STRAPS</h2>
            <p class="text-grey-500 mb-6">Is your watch out of warranty and needs a service, strap or a new lease of
                life? Take advantage of our free estimates and postage.</p>
            <div class="flex">
                <a class="btn btn-primary uppercase w-full text-center md:w-auto" href="{{ route('service-repair') }}">Start
                    Service & Repair</a>
            </div>
        </div>
    </x-split-section>

    <x-split-section image="{{ site_name() === 'swissmade' ? 'image/lawrence-suzara-1133350-unsplash.jpg' : 'image/index_estimate_by_post.jpg' }}">
        <div>
            <div class="mb-6">
                <img src="{{ asset_url('icon/file_text_data.png') }}" alt="...">
            </div>
            <h2 class="text-3xl uppercase font-bold tracking-tight mb-3">HAVE YOU RECEIVED AN ESTIMATE BY POST?</h2>
            <p class="text-grey-500 mb-6">Have you received an estimate and registration code in the post? Please
                complete the online account set up to manage your repair online.</p>
            <div class="flex">
                <a class="btn btn-primary uppercase w-full text-center md:w-auto" href="{{ route('estimate-by-post') }}">View Estimate</a>
            </div>
        </div>
    </x-split-section>

    <x-cta.block bg-color="bg-grey-500" image="image/index_estimate_by_post.jpg">
        <h2 class="text-3xl uppercase font-bold tracking-light mb-3">WHY CHOOSE {{ site_name() }} <br>
            WATCH SERVICE CENTRE</h2>
        <p class="text-grey-500 text-xl mb-6">
            @switch(site_name())
                @case('swissmade')
                Specialists in the repair and maintenance of fine Swiss watches from the heart of London’s Hatton Garden since 1985.
                @break
                @case('swico')
                SWICO the Official UK Service & Repair Centre providing warranty or
                non-warranty repair and maintenance for our WATCH BRANDS timepieces.
                @break
                @default
            @endswitch
        </p>
        <ul class="font-bold space-y-2">
            <li class="flex items-start">
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                Online portalto manage request
            </li>
            <li class="flex items-start">
             
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                Free secure
                postal return service
            </li>
            <li class="flex items-start">
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                @if (site_name() === 'swissmade')
                    Manufacturers Warranty Claims for Ebel and Baume & Mercier
                @else
                    Manufacturers Warranty Claims
                @endif
            </li>
            <li class="flex items-start">
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                @if (site_name() === 'swissmade')
                    Free No Obligation Estimates for non-warranty enquiries
                @else
                    Free No Obligation Estimates
                @endif
            </li>
            <li class="flex items-start">
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                Quick Turnaround
            </li>
        </ul>
        <div class="flex mt-6">
            <a class="uppercase btn btn-primary w-full md:w-auto" href="{{ route('watch-care') }}">
                Register with {{site_name()}}
            </a>
        </div>
    </x-cta.block>

    @if (site_name() === 'swico')
        
    <x-swico.swissmade :mode="'dark'" />
    @endif

</x-guest-layout>
