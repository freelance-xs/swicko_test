<x-guest-layout>

    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}">
        <x-slot name="preTitle">
            customer <br>information
        </x-slot>
        <x-slot name="mainTitle">
            @if (site_name() === 'swico')
                watch repairs <br> by post
            @else
                online watch <br> repairs
            @endif
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
    </x-hero>

    <div class="container mx-auto py-24">
        <div class="md:flex md:-mx-10">
            <div class="md:w-1/2 md:px-10">
                <h2 class="uppercase font-bold mb-5">how does it work?</h2>
                <h3 class="text-grey-200 mb-5">Send your watch for FREE for a Service or Repair.</h3>
                <p class="mb-10">To ensure the best customer service you can now register a request online. Simply create your account, make your request, and send your watch using our free, secure postal service. We will keep in touch with you about your request via the SWICO online service centre.</p>
                <a class="btn btn-primary uppercase" href="{{url('repairs-by-post')}}">step by step guide</a>               
            </div>
            <div class="md:w-1/2 md:px-10">
                <img src="{{ asset_url('image/how_it_works.jpg') }}" alt="how it works">
            </div>
        </div>
    </div>

    {{-- steps --}}
    {{-- 1 --}}
    <x-split-section image="image/steps/how_it_works_01.jpg" :left="true" bgColor="grey-100">
        <div class="bg-contain bg-no-repeat bg-right-top" style="background-image: url('{{ asset_url('image/steps/1.png') }}')">
            <h1 class="text-9xl mb-5 text-primary uppercase">01</h1>
            <p class="uppercase text-xl mb-5 text-grey-300">step 01</p>
            <h2 class="font-bold mb-5">Create an online account</h2>
            <p>In order to send your watch you will need to create an account.</p>
        </div>
    </x-split-section>

    {{-- 2 --}}
    <x-split-section image="image/steps/how_it_works_02.jpg" :left="false" bgColor="grey-100">
        <div class="bg-contain bg-no-repeat bg-right-top" style="background-image: url('{{ asset_url('image/steps/2.png') }}')">
            <h1 class="text-9xl mb-5 text-primary uppercase">02</h1>
            <p class="uppercase text-xl mb-5 text-grey-300">step 02</p>
            <h2 class="font-bold mb-5">Tell us about your repair</h2>
            <p>When registered tell us about your watch and create your request.</p>
        </div>
    </x-split-section>

    {{-- 3 --}}
    <x-split-section image="image/steps/how_it_works_03.jpg" :left="true" bgColor="grey-100">
        <div class="bg-contain bg-no-repeat bg-right-top" style="background-image: url('{{ asset_url('image/steps/3.png') }}')">
            <h1 class="text-9xl mb-5 text-primary uppercase">03</h1>
            <p class="uppercase text-xl mb-5 text-grey-300">step 03</p>
            <h2 class="font-bold mb-5">Send us your watch</h2>
            <p>
                When we receive your request we will send you our FREE postage label & instructions to send your watch to us safely by a Secure Royal Mail Return Service. You can track its delivery to us with the unique parcel reference you will be provided.
            </p>
        </div>
    </x-split-section>

    {{-- 4 --}}
    <x-split-section image="image/steps/how_it_works_04.jpg" :left="false" bgColor="grey-100">
        <div class="bg-contain bg-no-repeat bg-right-top" style="background-image: url('{{ asset_url('image/steps/4.png') }}')">
            <h1 class="text-9xl mb-5 text-primary uppercase">04</h1>
            <p class="uppercase text-xl mb-5 text-grey-300">step 04</p>
            <h2 class="font-bold mb-5">Watch received and assessed</h2>
            <p>
                We provide peace of mind by acknowledging receipt of your watch. It will then be carefully examined by our expert watchmakers to confirm the work required.
            </p>
        </div>
    </x-split-section>

    {{-- 5 --}}
    <x-split-section image="image/steps/how_it_works_05.jpg" :left="true" bgColor="grey-100">
        <div class="bg-contain bg-no-repeat bg-right-top" style="background-image: url('{{ asset_url('image/steps/5.png') }}')">
            <h1 class="text-9xl mb-5 text-primary uppercase">05</h1>
            <p class="uppercase text-xl mb-5 text-grey-300">step 05</p>
            <h2 class="font-bold mb-5">Track. Approve. Pay</h2>
            <p>
                If your watch is covered by a valid warranty, we will email you an expected completion date. If your watch is a out of warranty, we will email you a free no obligation estimate for your approval and you can make a payment online. If you choose not to go ahead you will have the option to reject the estimate and your watch will be returned at no cost to you.
            </p>
        </div>
    </x-split-section>

    {{-- 6 --}}
    <x-split-section image="image/steps/how_it_works_06.jpg" :left="false" bgColor="grey-100">
        <div class="bg-contain bg-no-repeat bg-right-top" style="background-image: url('{{ asset_url('image/steps/6.png') }}')">
            <h1 class="text-9xl mb-5 text-primary uppercase">06</h1>
            <p class="uppercase text-xl mb-5 text-grey-300">step 06</p>
            <h2 class="font-bold mb-5">We repair</h2>
            <p>
                Once your watch has been repaired, it is cleaned and inspected to ensure its meets the watch brands and SWICO high quality standards.
            </p>
        </div>
    </x-split-section>

    {{-- 7 --}}
    <x-split-section image="image/steps/how_it_works_07.jpg" :left="true" bgColor="grey-100">
        <div class="bg-contain bg-no-repeat bg-right-top" style="background-image: url('{{ asset_url('image/steps/7.png') }}')">
            <h1 class="text-9xl mb-5 text-primary uppercase">07</h1>
            <p class="uppercase text-xl mb-5 text-grey-300">step 07</p>
            <h2 class="font-bold mb-5">Receive your repaired watch</h2>
            <p>
                Your watch will be shipped back to you by a Secure Royal Mail service. You will be emailed the unique parcel reference to track its delivery when its on its way.
            </p>
        </div>
    </x-split-section>

    <x-swico.services-grid />

</x-guest-layout>
