<x-guest-layout>
    <x-hero background-image="{{ asset_url('banners/home-hero.jpg') }}">
        <x-slot name="preTitle">customer <br> information</x-slot>
        <x-slot name="mainTitle">
            @if (site_name() === 'swico')
                estimates<br> by post

            @elseif( site_name() === 'swissmade' )
                activation code <br> by post
            @endif
            <banner-bluedot class="text-primary text-base">●</banner-bluedot>
        </x-slot>
    </x-hero>

    <div class="container relative mx-auto py-10" style="min-height: 700px;">
        <div class="md:-mx-10 relative">
            <div class="md:w-1/2 float-left md:px-10">
                <img class="mb-5" src="{{ asset_url('icon/file_text_data.png') }}" alt="...">

                <h2 class="font-bold mb-5 uppercase">
                    @if (site_name() === 'swico')
                        HAVE YOU RECEIVED AN ESTIMATE BY POST?
                    @elseif( site_name() === 'swissmade' )
                        HAVE YOU RECEIVED AN ONLINE ACTIVATION CODE BY POST?
                    @endif
                </h2>

                <p class="text-lg">
                    @if (site_name() === 'swico')
                        If you have received an estimate with an online activation code from us, you will need to enter the activation code in the area indicated SSC-XXXXX-XXXXX, along with your last name, email and a password to view your watch repair online.
                    @elseif( site_name() === 'swissmade' )
                        If you have received an estimate with an online activation code from us, you will need to enter the activation code in the area indicated SSC-XXXXX-XXXXX, along with your last name, email and a password to view your watch repair online.
                    @endif
                </p>
                <p class="font-bold">
                    <span class="text-primary">IMPORTANT:</span> You do not need to create a new online account. <br>
                </p>
                @if (site_name() === 'swico')
                    <p>
                        This has been partly done for you by a member of our customer care team.
                    </p>
                @elseif( site_name() === 'swissmade' )

                @endif

                <img class="my-10" src="{{ asset_url('image/warranty_watch.png') }}" alt="...">
            </div>
            <div class="md:w-1/2 md:px-10 float-right">
                <div class="bg-grey-100 p-5">
                    <x-forms.estimate-by-post-form />
                </div>
            </div>
        </div>
        <div class="table clear-both"></div>
    </div>

    <x-cta.block bg-color="bg-grey-500" image="image/index_estimate_by_post.jpg">
        <h2 class="text-3xl uppercase font-bold tracking-light mb-3">WHY CHOOSE {{ site_name() }} <br>
            WATCH SERVICE CENTRE</h2>
        <p class="text-grey-500 text-xl mb-6">
            @switch(site_name())
                @case('swissmade')
                Specialists in the repair and maintenance of fine Swiss watches from the heart of London’s Hatton Garden since 1985.
                @break
                @case('swico')
                SWICO the Official UK Service & Repair Centre providing warranty or
                non-warranty repair and maintenance for our WATCH BRANDS timepieces.
                @break
                @default
            @endswitch
        </p>
        <ul class="font-bold space-y-2">
            <li class="flex items-start">
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                Online portalto manage request
            </li>
            <li class="flex items-start">
             
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                Free secure
                postal return service
            </li>
            <li class="flex items-start">
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                @if (site_name() === 'swissmade')
                    Manufacturers Warranty Claims for Ebel and Baume & Mercier
                @else
                    Manufacturers Warranty Claims
                @endif
            </li>
            <li class="flex items-start">
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                @if (site_name() === 'swissmade')
                    Free No Obligation Estimates for non-warranty enquiries
                @else
                    Free No Obligation Estimates
                @endif
            </li>
            <li class="flex items-start">
                <div class="mr-3" style="min-width: 17px;">
                    <img src="{{ asset_url('icon/tick_shield_blue_small.png') }}" alt="...">
                </div>
                Quick Turnaround
            </li>
        </ul>
        <div class="flex mt-6">
            <a class="uppercase btn btn-primary w-full md:w-auto" href="{{ route('watch-care') }}">
                Register with {{site_name()}}
            </a>
        </div>
    </x-cta.block>

    @switch(site_name())
        @case('swissmade')
        <x-watch-brands mode="dark" :watches="['ebel', 'baume_and_mercier', 'longines', 'omega', 'rolex']" />
        @break
        @case('swico')
        <x-watch-brands mode="dark" :watches="['boss', '88_rue-du_rhone', 'coach', 'movado', 'ferrari', 'tommy_hilfiger', 'boss_orange', 'lacoste', 'olivia_burton', 'tw_steel', 'raymond_weil']" />
        @break
        @default
    @endswitch
</x-guest-layout>
