const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
        require('tailwindcss'),
    ])

mix
    .js('resources/js/frontend/swico.js', 'public/js/frontend')
    .postCss('resources/css/frontend/swico.css', 'public/css/frontend', [
        require('postcss-import'),
        require('tailwindcss')
    ])

mix
    .js('resources/js/frontend/swissmade.js', 'public/js/frontend')
    .postCss('resources/css/frontend/swissmade.css', 'public/css/frontend', [
        require('postcss-import'),
        require('tailwindcss')
    ])

mix
    .js('resources/js/frontend/trade.js', 'public/js/frontend')
    .postCss('resources/css/frontend/trade.css', 'public/css/frontend', [
        require('postcss-import'),
        require('tailwindcss')
    ])

if (mix.inProduction()) {
    mix.version();
}
